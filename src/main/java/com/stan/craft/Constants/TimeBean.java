package com.stan.craft.Constants;

public class TimeBean {

    public TimeBean() {
    }

    public int getGameTime() {
        return StaticVar.GameTime;
    }

    public int getDays() {
        return StaticVar.Days;
    }

    public int getFights() {
        return StaticVar.Fights;
    }
}
