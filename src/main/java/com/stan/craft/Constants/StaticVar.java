package com.stan.craft.Constants;

import com.stan.craft.Models.Creep;
import com.stan.craft.Models.Item;

import java.util.List;

public class StaticVar {
    public static int GameTime = 1;
    public static int Days = 0;
    public static int Fights = 0;
    public static int loggedUsers = 0;
    public static List<Creep> AllCreeps;
    public static List<Integer> AllLevels;
    public static List<Item> AllItems;

    public static int IndividualId;
}
