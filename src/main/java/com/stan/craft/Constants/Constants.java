package com.stan.craft.Constants;

public class Constants {
    public static final boolean ENABLE_FIGHTS = true;
    public static final int MILLISECONDS_BETWEEN_FIGHTS = 100; //The timer! 1000 is 1 sec, 167 is fight on 30 sec
    public static final int ROUND_LENGTH_SECONDS = 180;

    public static final int MAX_CHARACTER_LEVEL = 100;
    public static final int NUMBER_OF_ITEM_LEVELS = 10;
    public static final double GENERAL_DROP_ITEM_PER_CENT = 0.15;
    public static final int MAX_ID_OF_DROP_CHANCES = 6;
    public static final int MATRIX_SIZE = 15;
    public static final int LOGS_PER_PAGE = 30;
    public static final int FIGHT_LOGS_PER_PAGE = 30;
    public static final int SIZE_FIGHT_LOG_LINES = 30;
    //for 30 lines we need the in DB the fight_log, the log to be 4000 symbols long - will be changed
    public static final int FIRST_LINES_FIGHT_LOG = 5;
    public static final String LOG_SEPARATOR = "===";
    public static final int DAYS_MAIN_LOG = 10;
    public static final int DAYS_FIGHT_LOG = 10;
    public static final long MAX_MAIN_LOG_COUNT = 2000L;
    public static final long MAX_FIGHT_LOG_COUNT = 2000L;
    public static final int SECONDS_PER_DAY = 86400;
    public static final int INVENTORY_ITEM_ROW_SIZE = 10;
    public static final int BAG_ITEM_ROW_SIZE = 2;
    public static final int AVAILABLE_SKILLS_ROW_SIZE = 10;
    public static final int CHARACTER_SKILLS_ROW_SIZE = 10;

    public static final String REGEX_INPUT_PATTERN = "^[A-Za-z]{3,20}$";

    //Crystals & Rings
    public static final String CRYSTAL_AMBER = "Amber increases equipped speed by 20%";
    public static final String CRYSTAL_AMETHYST = "Amethyst increases armor with 20%";
    public static final String CRYSTAL_DIAMOND = "Diamond increases magic damage with 20%";
    public static final String CRYSTAL_EMERALD = "Emerald increases block chance with 0.1";
    public static final String CRYSTAL_GOLD = "Gold increases drop chances with 1";
    public static final String CRYSTAL_SAPPHIRE = "Sapphire increases damage with 20%";
    public static final String CRYSTAL_RUBY = "Ruby increases life with 20%";
    public static final String EMPTY_RING = "Empty ring needs a crystal";
    public static final String CRYSTAL_RING = "Crystal ring with ";

    //Passive Skills
    public static final int SKILL_MAX_LEVEL = 20;
    public static final String SKILL_POWER = "Power is a Passive skill. Increases self and equipped damage up to 20%.";
    public static final String SKILL_POWER_AVAILABILITY = "Knight,Titan,Shaman,Slayer";
    public static final double SKILL_POWER_INCREASE_DMG_PER_LVL = 0.01;
    public static final String SKILL_SWIFT = "Swift is a Passive skill. Increases self and equipped speed up to 20%.";
    public static final String SKILL_SWIFT_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_SWIFT_INCREASE_SPEED_PER_LVL = 0.01;
    public static final String SKILL_DEFENCE = "Defence is a Passive skill. Increases self and equipped armor up to 20%.";
    public static final String SKILL_DEFENCE_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_DEFENCE_INCREASE_ARMOR_PER_LVL = 0.01;
    public static final String SKILL_TOUGHNESS = "Toughness is a Passive skill. Increases self and equipped life up to 20%.";
    public static final String SKILL_TOUGHNESS_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_TOUGHNESS_INCREASE_LIFE_PER_LVL = 0.01;
    public static final String SKILL_GOD_SHIELD = "God shield is a Passive skill. Increases self and equipped block up to 20%.";
    public static final String SKILL_GOD_SHIELD_AVAILABILITY = "Knight,Titan,Slayer";
    public static final double SKILL_GOD_SHIELD_INCREASE_BLOCK_PER_LVL = 0.004;
    public static final String SKILL_SPIRIT = "Spirit is a Passive skill. Gives spirit dmg up to 7% of your avr Final dmg. For Wizard: up to 20% magic dmg increase";
    public static final String SKILL_SPIRIT_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_SPIRIT_WIZARD_INCREASE_MAGIC_DMG_PER_LVL = 0.01;
    public static final double SKILL_SPIRIT_OTHERS_INCREASE_MAGIC_DMG_PER_LVL = 0.0035;

    //Curses
    public static final String SKILL_RETARD = "Retard is a Curse. Decreases final speed of the enemy up to 20%.";
    public static final String SKILL_RETARD_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_RETARD_DECREASE_SPEED_PER_LVL = 0.01;
    public static final String SKILL_WEAK = "Weak is a Curse. Decreases final damage of the enemy up to 20%.";
    public static final String SKILL_WEAK_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_WEAK_DECREASE_DMG_PER_LVL = 0.01;
    public static final String SKILL_VULNERABLE = "Vulnerable is a Curse. Decreases final armor of the enemy up to 20%.";
    public static final String SKILL_VULNERABLE_AVAILABILITY = "Knight,Titan,Shaman,Slayer";
    public static final double SKILL_VULNERABLE_DECREASE_ARMOR_PER_LVL = 0.01;
    public static final String SKILL_INVALID = "Invalid is a Curse. Decreases final life of the enemy up to 20%.";
    public static final String SKILL_INVALID_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_INVALID_DECREASES_LIFE_PER_LVL = 0.01;
    public static final String SKILL_ARMLESS = "Armless is a Curse. Decreases final block chance of the enemy up to 20%.";
    public static final String SKILL_ARMLESS_AVAILABILITY = "Knight,Titan,Shaman,Slayer";
    public static final double SKILL_ARMLESS_DECREASES_BLOCK_PER_LVL = 0.01;

    //Active Skills
    public static final String SKILL_SHOCK = "Shock is an Active Skill, you can use only 1 active. 20% chance to shock your opponent (he cannot hit) up to 1.8 sec.";
    public static final String SKILL_SHOCK_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_SHOCK_CHANCE = 0.20;
    public static final String SKILL_CRITICAL = "Critical is an Active Skill, you can use only 1 active. 15% chance to hit once with damage up to x2.";
    public static final String SKILL_CRITICAL_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_CRITICAL_CHANCE = 0.15;
    public static final String SKILL_MULTIHIT = "MultiHit is an Active Skill, you can use only 1 active. 10% - 40% to hit twice";
    public static final String SKILL_MULTIHIT_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final String SKILL_DYNAMITE = "Dynamite is an Active Skill, you can use only 1 active. 5% chance to start hitting for the fight with damage x1 - x1.5.";
    public static final String SKILL_DYNAMITE_AVAILABILITY = "Knight,Titan,Shaman,Slayer,Wizard";
    public static final double SKILL_DYNAMITE_CHANCE = 0.05;

    public static final String KNIGHT_SKILLS_AVAILABILITY = "Power,Swift,Defence,Toughness,GodShield,Spirit,Retard,Weak" +
            ",Vulnerable,Invalid,Armless,Shock,Critical,MultiHit,Dynamite";
    public static final String TITAN_SKILLS_AVAILABILITY = "Power,Swift,Defence,Toughness,GodShield,Spirit,Retard,Weak" +
            ",Vulnerable,Invalid,Armless,Shock,Critical,MultiHit,Dynamite";
    public static final String SHAMAN_SKILLS_AVAILABILITY = "Power,Swift,Defence,Toughness,Spirit,Retard,Weak" +
            ",Vulnerable,Invalid,Armless,Shock,Critical,MultiHit,Dynamite";
    public static final String SLAYER_SKILLS_AVAILABILITY = "Power,Swift,Defence,Toughness,GodShield,Spirit,Retard,Weak" +
            ",Vulnerable,Invalid,Armless,Shock,Critical,MultiHit,Dynamite";
    public static final String WIZARD_SKILLS_AVAILABILITY = "Swift,Defence,Toughness,Spirit,Retard,Weak" +
            ",Invalid,Shock,Critical,MultiHit,Dynamite";


    //86400000L milliseconds = 1 day
    //259200000L milliseconds = 3 day
    //345600000L milliseconds = 4 day
    //864000000L milliseconds = 10 day
}
