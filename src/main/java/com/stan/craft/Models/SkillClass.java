package com.stan.craft.Models;

public enum SkillClass {
    //Passive skills
    Swift,
    Power,
    Defence,
    Toughness,
    GodShield,
    Spirit,
    Wealth,
    SpiritArmor,


    //Curses
    Retard,
    Weak,
    Vulnerable,
    Invalid,
    Armless,

    //Active skills
    Shock,
    Critical,
    MultiHit,
    Dynamite,
    FireStike

}
