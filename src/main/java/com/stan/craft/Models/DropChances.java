package com.stan.craft.Models;

import javax.persistence.*;

@Entity
@Table(name = "drop_chances")
public class DropChances {

    @Id
    @Column(name = "drop_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int dropId;

    @Column(name = "chance_1")
    int chanceLevel1;

    @Column(name = "chance_10")
    int chanceLevel10;

    @Column(name = "chance_20")
    int chanceLevel20;

    @Column(name = "chance_30")
    int chanceLevel30;

    @Column(name = "chance_40")
    int chanceLevel40;

    @Column(name = "chance_50")
    int chanceLevel50;

    @Column(name = "chance_60")
    int chanceLevel60;

    @Column(name = "chance_70")
    int chanceLevel70;

    @Column(name = "chance_80")
    int chanceLevel80;

    @Column(name = "chance_crystal")
    int chanceCrystal;

    public DropChances() {
    }

    public int getChanceCrystal() {
        return chanceCrystal;
    }

    public void setChanceCrystal(int chanceCrystal) {
        this.chanceCrystal = chanceCrystal;
    }

    public int getDropId() {
        return dropId;
    }

    public void setDropId(int dropId) {
        this.dropId = dropId;
    }

    public int getChanceLevel1() {
        return chanceLevel1;
    }

    public void setChanceLevel1(int chanceLevel1) {
        this.chanceLevel1 = chanceLevel1;
    }

    public int getChanceLevel10() {
        return chanceLevel10;
    }

    public void setChanceLevel10(int chanceLevel10) {
        this.chanceLevel10 = chanceLevel10;
    }

    public int getChanceLevel20() {
        return chanceLevel20;
    }

    public void setChanceLevel20(int chanceLevel20) {
        this.chanceLevel20 = chanceLevel20;
    }

    public int getChanceLevel30() {
        return chanceLevel30;
    }

    public void setChanceLevel30(int chanceLevel30) {
        this.chanceLevel30 = chanceLevel30;
    }

    public int getChanceLevel40() {
        return chanceLevel40;
    }

    public void setChanceLevel40(int chanceLevel40) {
        this.chanceLevel40 = chanceLevel40;
    }

    public int getChanceLevel50() {
        return chanceLevel50;
    }

    public void setChanceLevel50(int chanceLevel50) {
        this.chanceLevel50 = chanceLevel50;
    }

    public int getChanceLevel60() {
        return chanceLevel60;
    }

    public void setChanceLevel60(int chanceLevel60) {
        this.chanceLevel60 = chanceLevel60;
    }

    public int getChanceLevel70() {
        return chanceLevel70;
    }

    public void setChanceLevel70(int chanceLevel70) {
        this.chanceLevel70 = chanceLevel70;
    }

    public int getChanceLevel80() {
        return chanceLevel80;
    }

    public void setChanceLevel80(int chanceLevel80) {
        this.chanceLevel80 = chanceLevel80;
    }
}
