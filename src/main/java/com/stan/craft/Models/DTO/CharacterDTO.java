package com.stan.craft.Models.DTO;

import com.stan.craft.Models.CharacterClass;

public class CharacterDTO {
    private CharacterClass characterClass;
    private String name;

    public CharacterDTO() {
    }

    public CharacterDTO(CharacterClass characterClass, String name) {
        this.characterClass = characterClass;
        this.name = name;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
