package com.stan.craft.Models.DTO;

import com.stan.craft.Models.CreepClass;
import com.stan.craft.Models.IndividualType;

public class CreepDTO {
    private int creepId;


    private CreepClass creepClass;

    private IndividualType type;

    private int isBoss; //0 = No; 1 = Yes

    private String name;

    private int level;

    private long experience;

    private long gold;

    private int damageMin;

    private int damageMax;

    private int magicDamage;

    private int armor;

    private int life;

    private double blockChance;

    private double speed;

    private int dropChancesId;

    private String picture;

    public CreepDTO() {
    }

    public IndividualType getType() {
        return type;
    }

    public void setType(IndividualType type) {
        this.type = type;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getMagicDamage() {
        return magicDamage;
    }

    public void setMagicDamage(int magicDamage) {
        this.magicDamage = magicDamage;
    }

    public double getBlockChance() {
        return blockChance;
    }

    public void setBlockChance(double blockChance) {
        this.blockChance = blockChance;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getCreepId() {
        return creepId;
    }

    public void setCreepId(int creepId) {
        this.creepId = creepId;
    }

    public CreepClass getCreepClass() {
        return creepClass;
    }

    public void setCreepClass(CreepClass creepClass) {
        this.creepClass = creepClass;
    }

    public int getIsBoss() {
        return isBoss;
    }

    public void setIsBoss(int isBoss) {
        this.isBoss = isBoss;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getExperience() {
        return experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public long getGold() {
        return gold;
    }

    public void setGold(long gold) {
        this.gold = gold;
    }

    public int getDamageMin() {
        return damageMin;
    }

    public void setDamageMin(int damageMin) {
        this.damageMin = damageMin;
    }

    public int getDamageMax() {
        return damageMax;
    }

    public void setDamageMax(int damageMax) {
        this.damageMax = damageMax;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getDropChancesId() {
        return dropChancesId;
    }

    public void setDropChancesId(int dropChancesId) {
        this.dropChancesId = dropChancesId;
    }
}
