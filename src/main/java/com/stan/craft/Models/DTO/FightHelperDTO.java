package com.stan.craft.Models.DTO;

import com.stan.craft.Models.Skill;

import java.util.List;

public class FightHelperDTO {
    private int life;
    private double timeToHit;
    private List<Skill> listActiveSkills;
    private Skill activeSkill;
    private boolean dynamiteActivated;

    public FightHelperDTO() {
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public double getTimeToHit() {
        return timeToHit;
    }

    public void setTimeToHit(double timeToHit) {
        this.timeToHit = timeToHit;
    }

    public List<Skill> getListActiveSkills() {
        return listActiveSkills;
    }

    public void setListActiveSkills(List<Skill> listActiveSkills) {
        this.listActiveSkills = listActiveSkills;
    }

    public Skill getActiveSkill() {
        return activeSkill;
    }

    public void setActiveSkill(Skill activeSkill) {
        this.activeSkill = activeSkill;
    }

    public boolean isDynamiteActivated() {
        return dynamiteActivated;
    }

    public void setDynamiteActivated(boolean dynamiteActivated) {
        this.dynamiteActivated = dynamiteActivated;
    }
}
