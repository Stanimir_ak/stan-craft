package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class Armless extends Skill {

    public Armless() {
        setValue("-" + Constants.SKILL_ARMLESS_DECREASES_BLOCK_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue("-" + Constants.SKILL_ARMLESS_DECREASES_BLOCK_PER_LVL * getLevel() * 100 + "%");
        individual.setFinalBlockChance(Utils.round(individual.getFinalBlockChance() *
                (1 - (Constants.SKILL_ARMLESS_DECREASES_BLOCK_PER_LVL * getLevel())), 2));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Curse;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Armless;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_ARMLESS;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_ARMLESS_AVAILABILITY;
    }
}
