package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class Critical extends Skill {
    public Critical() {
        setValue(String.valueOf(Utils.round(1.05, 1)));
    }

    @Override
    public Individual applySkill(Individual individual) {
//        setValue(String.valueOf(Utils.round(2 + ((getLevel()-1) * 0.107),1))); // from 2 to 4
        setValue(String.valueOf(Utils.round(1.05 + ((getLevel() - 1) * 0.05), 1))); // from 1.05 to 2.0
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Active;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Critical;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_CRITICAL;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_CRITICAL_AVAILABILITY;
    }
}
