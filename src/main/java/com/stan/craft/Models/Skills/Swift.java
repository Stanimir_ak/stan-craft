package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class Swift extends Skill {
    public Swift() {
        setValue(Utils.round(Constants.SKILL_SWIFT_INCREASE_SPEED_PER_LVL * getLevel() * 100, 2) + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue(Utils.round(Constants.SKILL_SWIFT_INCREASE_SPEED_PER_LVL * getLevel() * 100, 2) + "%");
        individual.setFinalSpeed(Utils.round(individual.getEquipSpeed() * (1 - (Constants.SKILL_SWIFT_INCREASE_SPEED_PER_LVL * getLevel())), 1));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Passive;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Swift;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_SWIFT;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_SWIFT_AVAILABILITY;
    }
}
