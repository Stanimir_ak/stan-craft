package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;

import javax.persistence.Entity;

@Entity
public class Defence extends Skill {
    public Defence() {
        setValue(Constants.SKILL_DEFENCE_INCREASE_ARMOR_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue(Constants.SKILL_DEFENCE_INCREASE_ARMOR_PER_LVL * getLevel() * 100 + "%");
        individual.setFinalArmor((int) (individual.getEquipArmor() *
                (1 + (Constants.SKILL_DEFENCE_INCREASE_ARMOR_PER_LVL * getLevel()))));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Passive;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Defence;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_DEFENCE;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_DEFENCE_AVAILABILITY;
    }
}
