package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class Retard extends Skill {
    public Retard() {
        setValue("-" + Constants.SKILL_RETARD_DECREASE_SPEED_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue("-" + Constants.SKILL_RETARD_DECREASE_SPEED_PER_LVL * getLevel() * 100 + "%");
        individual.setFinalSpeed(Utils.round(individual.getFinalSpeed() *
                (1 + (Constants.SKILL_RETARD_DECREASE_SPEED_PER_LVL * getLevel())), 1));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Curse;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Retard;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_RETARD;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_RETARD_AVAILABILITY;
    }
}
