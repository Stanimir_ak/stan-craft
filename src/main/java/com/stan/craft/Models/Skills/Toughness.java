package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;

import javax.persistence.Entity;

@Entity
public class Toughness extends Skill {

    public Toughness() {
        setValue(Constants.SKILL_TOUGHNESS_INCREASE_LIFE_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue(Constants.SKILL_TOUGHNESS_INCREASE_LIFE_PER_LVL * getLevel() * 100 + "%");
        individual.setFinalLife((int) (individual.getEquipLife() * (1 + (Constants.SKILL_TOUGHNESS_INCREASE_LIFE_PER_LVL * getLevel()))));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Passive;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Toughness;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_TOUGHNESS;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_TOUGHNESS_AVAILABILITY;
    }
}
