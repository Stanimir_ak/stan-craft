package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;

import javax.persistence.Entity;

@Entity
public class Invalid extends Skill {
    public Invalid() {
        setValue("-" + Constants.SKILL_INVALID_DECREASES_LIFE_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue("-" + Constants.SKILL_INVALID_DECREASES_LIFE_PER_LVL * getLevel() * 100 + "%");
        individual.setFinalLife((int) (individual.getFinalLife() *
                (1 - (Constants.SKILL_INVALID_DECREASES_LIFE_PER_LVL * getLevel()))));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Curse;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Invalid;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_INVALID;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_INVALID_AVAILABILITY;
    }
}
