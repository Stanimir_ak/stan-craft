package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;

import javax.persistence.Entity;

@Entity
public class Weak extends Skill {
    public Weak() {
        setValue("-" + Constants.SKILL_WEAK_DECREASE_DMG_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue("-" + Constants.SKILL_WEAK_DECREASE_DMG_PER_LVL * getLevel() * 100 + "%");
        individual.setFinalDamageMin((int) (individual.getFinalDamageMin() * (1 - (Constants.SKILL_WEAK_DECREASE_DMG_PER_LVL * getLevel()))));
        individual.setFinalDamageMax((int) (individual.getFinalDamageMax() * (1 - (Constants.SKILL_WEAK_DECREASE_DMG_PER_LVL * getLevel()))));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Curse;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Weak;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_WEAK;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_WEAK_AVAILABILITY;
    }
}
