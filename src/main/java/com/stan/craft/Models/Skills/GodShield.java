package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class GodShield extends Skill {
    public GodShield() {
        setValue(Constants.SKILL_GOD_SHIELD_INCREASE_BLOCK_PER_LVL * getLevel() * 100 + " points");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue(Constants.SKILL_GOD_SHIELD_INCREASE_BLOCK_PER_LVL * getLevel() * 100 + " points");
        individual.setFinalBlockChance(Utils.round(individual.getEquipBlockChance() +
                ((Constants.SKILL_GOD_SHIELD_INCREASE_BLOCK_PER_LVL * getLevel())), 2));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Passive;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.GodShield;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_GOD_SHIELD;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_GOD_SHIELD_AVAILABILITY;
    }
}
