package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class Shock extends Skill {
    public Shock() {
        setValue(String.valueOf(Utils.round(1, 1)));
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue(String.valueOf(Utils.round(1 + ((getLevel() - 1) * 0.0422), 1))); // from 1.0 to 1.8 sec
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Active;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Shock;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_SHOCK;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_SHOCK_AVAILABILITY;
    }
}
