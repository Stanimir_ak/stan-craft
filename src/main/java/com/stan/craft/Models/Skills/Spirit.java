package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.*;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class Spirit extends Skill {
    public Spirit() {
        setValue(Utils.round(Constants.SKILL_SPIRIT_OTHERS_INCREASE_MAGIC_DMG_PER_LVL * getLevel(), 2) * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        //if Wizard
        if (individual.getType() == IndividualType.Character && individual.getMagicDamageBase() > 0) {
            setValue(Constants.SKILL_SPIRIT_WIZARD_INCREASE_MAGIC_DMG_PER_LVL * getLevel() * 100 + "%");
            individual.setFinalMagicDamage((int) (individual.getEquipMagicDamage() *
                    (1 + (Constants.SKILL_SPIRIT_WIZARD_INCREASE_MAGIC_DMG_PER_LVL * getLevel()))));
            return individual;
        } else {
            setValue(Utils.round(Constants.SKILL_SPIRIT_OTHERS_INCREASE_MAGIC_DMG_PER_LVL * getLevel(), 2) * 100 + "%");
            individual.setFinalMagicDamage((int) (((individual.getFinalDamageMax() + individual.getFinalDamageMin()) / 2)
                    * (Constants.SKILL_SPIRIT_OTHERS_INCREASE_MAGIC_DMG_PER_LVL * getLevel())));
            return individual;
        }
    }

    @Override
    public SkillType getType() {
        return SkillType.Passive;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Spirit;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_SPIRIT;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_SPIRIT_AVAILABILITY;
    }
}
