package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;

import javax.persistence.Entity;

@Entity
public class Power extends Skill {
    public Power() {
        setValue(Constants.SKILL_POWER_INCREASE_DMG_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public SkillType getType() {
        return SkillType.Passive;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_POWER_AVAILABILITY;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Power;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_POWER;
    }

    @Override
    public Individual applySkill(Individual Individual) {
        setValue(Constants.SKILL_POWER_INCREASE_DMG_PER_LVL * getLevel() * 100 + "%");
        Individual.setFinalDamageMin((int) (Individual.getEquipDamageMin() * (1 + (Constants.SKILL_POWER_INCREASE_DMG_PER_LVL * getLevel()))));
        Individual.setFinalDamageMax((int) (Individual.getEquipDamageMax() * (1 + (Constants.SKILL_POWER_INCREASE_DMG_PER_LVL * getLevel()))));
        return Individual;
    }
}
