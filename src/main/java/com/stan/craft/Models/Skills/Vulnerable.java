package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;

import javax.persistence.Entity;

@Entity
public class Vulnerable extends Skill {
    public Vulnerable() {
        setValue("-" + Constants.SKILL_VULNERABLE_DECREASE_ARMOR_PER_LVL * getLevel() * 100 + "%");
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue("-" + Constants.SKILL_VULNERABLE_DECREASE_ARMOR_PER_LVL * getLevel() * 100 + "%");
        individual.setFinalArmor((int) (individual.getFinalArmor() *
                (1 - (Constants.SKILL_VULNERABLE_DECREASE_ARMOR_PER_LVL * getLevel()))));
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Curse;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Vulnerable;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_VULNERABLE;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_VULNERABLE_AVAILABILITY;
    }
}
