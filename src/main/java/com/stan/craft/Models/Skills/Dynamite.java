package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class Dynamite extends Skill {
    public Dynamite() {
        setValue(String.valueOf(Utils.round(1, 1)));
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue(String.valueOf(Utils.round(1 + ((getLevel() - 1) * 0.0264), 2))); // 1 - 1.5
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Active;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.Dynamite;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_DYNAMITE;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_DYNAMITE_AVAILABILITY;
    }
}
