package com.stan.craft.Models.Skills;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Individual;
import com.stan.craft.Models.Skill;
import com.stan.craft.Models.SkillClass;
import com.stan.craft.Models.SkillType;
import com.stan.craft.Utils.Utils;

import javax.persistence.Entity;

@Entity
public class MultiHit extends Skill {
    public MultiHit() {
        setValue(String.valueOf(Utils.round(0.1, 2)));
    }

    @Override
    public Individual applySkill(Individual individual) {
        setValue(String.valueOf(Utils.round(0.1 + ((getLevel() - 1) * 0.016), 2))); // 10% - 40%
        return individual;
    }

    @Override
    public SkillType getType() {
        return SkillType.Active;
    }

    @Override
    public SkillClass getSkillClass() {
        return SkillClass.MultiHit;
    }

    @Override
    public String getDescription() {
        return Constants.SKILL_MULTIHIT;
    }

    @Override
    public String getAvailability() {
        return Constants.SKILL_MULTIHIT_AVAILABILITY;
    }
}
