package com.stan.craft.Models;

public enum ZoneClass {
    Rest,
    Duel,
    Arena,
    LowCreeps,
    MidCreeps,
    HighCreeps,
    LowBosses,
    MidBosses,
    HighBosses,
    Ares,
    Thanatos,
    Achlys,
    Erebus,
    Chaos,
    Hades,
    Tartarus
}
