package com.stan.craft.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "creeps")
//@SequenceGenerator(name = "default_gen2", sequenceName = "creep_seq", initialValue = 200, allocationSize = 1)
public class Creep extends Individual implements Serializable {
    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @SequenceGenerator(name="default_gen2",sequenceName = "creep_seq", initialValue=10000)
//    private int id;

    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "creep_class")
    private CreepClass creepClass;

    @Column(name = "isBoss")
    private int isBoss; //0 = Creep; 1 = Boss;

    public Creep() {
    }

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public CreepClass getCreepClass() {
        return creepClass;
    }

    public void setCreepClass(CreepClass creepClass) {
        this.creepClass = creepClass;
    }

    public int getIsBoss() {
        return isBoss;
    }

    public void setIsBoss(int isBoss) {
        this.isBoss = isBoss;
    }
}
