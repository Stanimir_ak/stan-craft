package com.stan.craft.Models;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "characters")
//@SequenceGenerator(name = "default_gen", sequenceName = "char_seq", initialValue = 10000, allocationSize = 1)
public class Character extends Individual implements Serializable {
    private static final long serialVersionUID = 1L;

    //1. @MappedSuperclass + @Table(name = "characters"). Id only in classes with StaticVar.IndividualId - **PROBLEM** with JPA and saveAndFlush
    //2. @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS) + @Inheritance(same) id: Id only in classes with StaticVar.IndividualId
    // - **PROBLEM** with Skill - id- id foreign
    //3. Individual - @Entity + @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS). Skill have Individual object - **PROBLEM** -Indiv must have own id
    //4. Individual - @Entity + @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS) + own id - **PROBLEM** with the Sequence id of the clasees

    //5. IT WORKS!!!! Individual - @Entity + @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS) +
    //@SequenceGenerator(name = "default_gen", sequenceName = "ind_seq", initialValue = 10000, allocationSize = 1) +
    // id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen"). SubClasses dont have own id

//    @Id
    //    @GeneratedValue(strategy = GenerationType.IDENTITY, initialValue = 10000)
//        @SequenceGenerator(name = "default_gen", sequenceName = "char_seq", initialValue = 10000, allocationSize = 1)
    //    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="mysequence")
//    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "character_class")
    private CharacterClass characterClass;

    @ManyToMany
//            (cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
//    @NotFound(action = NotFoundAction.IGNORE) //problem - inventory table 25 000 size - this is not solvin the problem
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "characters_inventory",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private List<ItemPrivate> inventory = new ArrayList<>();

    @ManyToMany
//            (cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "characters_equipment",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private List<ItemPrivate> equipment = new ArrayList<>();

    @ManyToMany
//            (cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "characters_bag",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private List<ItemPrivate> bag = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "zone")
    private ZoneClass zone = ZoneClass.LowCreeps;

    @Column(name = "is_fighting_characters")
    private boolean isFightingCharacters = false;

    @Column(name = "is_creeping")
    private boolean isCreeping = false;

    @Column(name = "duel")
    private String duel;

    private int crystals = 0;

    public Character() {
    }

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }


    public int getCrystals() {
        return crystals;
    }

    public void setCrystals(int crystals) {
        this.crystals = crystals;
    }

    public String getDuel() {
        return duel;
    }

    public void setDuel(String duel) {
        this.duel = duel;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public List<ItemPrivate> getInventory() {
        return inventory;
    }

    public void setInventory(List<ItemPrivate> inventory) {
        this.inventory = inventory;
    }

    public List<ItemPrivate> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<ItemPrivate> equipment) {
        this.equipment = equipment;
    }

    public List<ItemPrivate> getBag() {
        return bag;
    }

    public void setBag(List<ItemPrivate> bag) {
        this.bag = bag;
    }

    public boolean isFightingCharacters() {
        return isFightingCharacters;
    }

    public void setFightingCharacters(boolean fightingCharacters) {
        isFightingCharacters = fightingCharacters;
    }

    public boolean isCreeping() {
        return isCreeping;
    }

    public void setCreeping(boolean creeping) {
        isCreeping = creeping;
    }

    public ZoneClass getZone() {
        return zone;
    }

    public void setZone(ZoneClass zone) {
        this.zone = zone;
    }
}
