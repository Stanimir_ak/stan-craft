package com.stan.craft.Models;

public enum Gods {
    Ares,
    Thanatos,
    Achlys,
    Erebus,
    Chaos,
    Hades,
    Tartarus
}
