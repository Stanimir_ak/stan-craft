package com.stan.craft.Models;

public enum IndividualType {
    Character,
    Creep,
    Boss,
    God
}
