package com.stan.craft.Models;

public enum CharacterClass {
    Titan,
    Knight,
    Shaman,
    Wizard,
    Slayer
}
