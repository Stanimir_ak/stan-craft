package com.stan.craft.Models.Factories;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Beans.FightResultBean;
import com.stan.craft.Models.Beans.FightResultBeanLight;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.*;
import com.stan.craft.Models.DTO.CharacterDTO;
import com.stan.craft.Models.DTO.CreepDTO;
import com.stan.craft.Models.Skills.*;
import com.stan.craft.Services.ItemPrivateService;
import com.stan.craft.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ModelFactory {

    private ItemPrivateService itemPrivateService;

    @Autowired
    public ModelFactory(ItemPrivateService itemPrivateService) {
        this.itemPrivateService = itemPrivateService;
    }

    public static Character createCharacterFromDTO(CharacterDTO characterDTO) {
        Character character = new Character();
        character.setName(characterDTO.getName());
        character.setType(IndividualType.Character);

        if (characterDTO.getCharacterClass().name().equals("Knight")) {
            character.setCharacterClass(CharacterClass.Knight);
            character.setDamageMinBase(2);
            character.setDamageMaxBase(5);
            character.setMagicDamageBase(0);
            character.setLifeBase(25);
            character.setArmorBase(4);
            character.setBlockChance(0.1);
            character.setSpeed(1.9);
        }
        if (characterDTO.getCharacterClass().name().equals("Titan")) {
            character.setCharacterClass(CharacterClass.Titan);
            character.setDamageMinBase(5);
            character.setDamageMaxBase(8);
            character.setMagicDamageBase(0);
            character.setLifeBase(37);
            character.setArmorBase(1);
            character.setBlockChance(0.1);
            character.setSpeed(2.0);
        }
        if (characterDTO.getCharacterClass().name().equals("Shaman")) {
            character.setCharacterClass(CharacterClass.Shaman);
            character.setDamageMinBase(1);
            character.setDamageMaxBase(9);
            character.setMagicDamageBase(0);
            character.setLifeBase(50);
            character.setArmorBase(1);
            character.setBlockChance(0.1);
            character.setSpeed(2.0);
        }
        if (characterDTO.getCharacterClass().name().equals("Slayer")) {
            character.setCharacterClass(CharacterClass.Slayer);
            character.setDamageMinBase(2);
            character.setDamageMaxBase(4);
            character.setMagicDamageBase(0);
            character.setLifeBase(25);
            character.setArmorBase(2);
            character.setBlockChance(0.1);
            character.setSpeed(1.4);
        }
        if (characterDTO.getCharacterClass().name().equals("Wizard")) {
            character.setCharacterClass(CharacterClass.Wizard);
            character.setDamageMinBase(0);
            character.setDamageMaxBase(0);
            character.setMagicDamageBase(3);
            character.setLifeBase(24);
            character.setArmorBase(2);
            character.setBlockChance(0.1);
            character.setSpeed(2.0);
        }

        character.setDamageMin(character.getDamageMinBase());
        character.setDamageMax(character.getDamageMaxBase());
        character.setMagicDamage(character.getMagicDamageBase());
        character.setLife(character.getLifeBase());
        character.setArmor(character.getArmorBase());

        character.setEquipDamageMin(character.getDamageMin());
        character.setEquipDamageMax(character.getDamageMax());
        character.setEquipMagicDamage(character.getMagicDamage());
        character.setEquipLife(character.getLife());
        character.setEquipArmor(character.getArmor());
        character.setEquipBlockChance(character.getBlockChance());
        character.setEquipSpeed(character.getSpeed());

        character.setFinalDamageMin(character.getEquipDamageMin());
        character.setFinalDamageMax(character.getEquipDamageMax());
        character.setFinalMagicDamage(character.getEquipMagicDamage());
        character.setFinalLife(character.getEquipLife());
        character.setFinalArmor(character.getEquipArmor());
        character.setFinalBlockChance(character.getEquipBlockChance());
        character.setFinalSpeed(character.getEquipSpeed());

        character.setDropChances(createDropChancesFromId(1));
        return character;
    }

    public static DropChances createDropChancesFromId(int id) {
        DropChances dropChances = new DropChances();
        dropChances.setDropId(id);
        switch (id) {
            case 1:
                dropChances.setChanceLevel1(90);
                dropChances.setChanceLevel10(8);
                dropChances.setChanceLevel20(1);
                dropChances.setChanceLevel30(0);
                dropChances.setChanceLevel40(0);
                dropChances.setChanceLevel50(0);
                dropChances.setChanceLevel60(0);
                dropChances.setChanceLevel70(0); //Unique
                dropChances.setChanceLevel80(0); //God Item
                dropChances.setChanceCrystal(1); //Crystal
                break;
            case 2:
                dropChances.setChanceLevel1(80);
                dropChances.setChanceLevel10(10);
                dropChances.setChanceLevel20(6);
                dropChances.setChanceLevel30(2);
                dropChances.setChanceLevel40(1);
                dropChances.setChanceLevel50(0);
                dropChances.setChanceLevel60(0);
                dropChances.setChanceLevel70(0); //Unique
                dropChances.setChanceLevel80(0); //God Item
                dropChances.setChanceCrystal(1); //Crystal
                break;
            case 3:
                dropChances.setChanceLevel1(65);
                dropChances.setChanceLevel10(10);
                dropChances.setChanceLevel20(10);
                dropChances.setChanceLevel30(5);
                dropChances.setChanceLevel40(4);
                dropChances.setChanceLevel50(3);
                dropChances.setChanceLevel60(2);
                dropChances.setChanceLevel70(0); //Unique
                dropChances.setChanceLevel80(0); //God Item
                dropChances.setChanceCrystal(1); //Crystal
                break;
            case 4:
                dropChances.setChanceLevel1(5);
                dropChances.setChanceLevel10(20);
                dropChances.setChanceLevel20(30);
                dropChances.setChanceLevel30(16);
                dropChances.setChanceLevel40(12);
                dropChances.setChanceLevel50(9);
                dropChances.setChanceLevel60(6);
                dropChances.setChanceLevel70(1); //Unique
                dropChances.setChanceLevel80(0); //God Item
                dropChances.setChanceCrystal(1); //Crystal
                break;
            case 5: //Gods Erebus, Chaos and Hades only
                dropChances.setChanceLevel1(0);
                dropChances.setChanceLevel10(0);
                dropChances.setChanceLevel20(0);
                dropChances.setChanceLevel30(50);
                dropChances.setChanceLevel40(20);
                dropChances.setChanceLevel50(15);
                dropChances.setChanceLevel60(10);
                dropChances.setChanceLevel70(3); //Unique
                dropChances.setChanceLevel80(1); //God Item
                dropChances.setChanceCrystal(1); //Crystal
                break;
            case 6:// God Tartarus only
                dropChances.setChanceLevel1(0);
                dropChances.setChanceLevel10(0);
                dropChances.setChanceLevel20(0);
                dropChances.setChanceLevel30(0);
                dropChances.setChanceLevel40(0);
                dropChances.setChanceLevel50(53);
                dropChances.setChanceLevel60(40);
                dropChances.setChanceLevel70(5); //Unique
                dropChances.setChanceLevel80(1); //God Item
                dropChances.setChanceCrystal(1); //Crystal
                break;
        }

        return dropChances;
    }

    public static List<Integer> getAllLevels() {
        List<Integer> levels = new ArrayList<>();
        //1 - 10
        levels.add(150);
        levels.add(500);
        levels.add(900);
        levels.add(1800);
        levels.add(3600);
        levels.add(7200);
        levels.add(11000);
        levels.add(16000);
        levels.add(24000);
        levels.add(36000);

        //11 - 20
        levels.add(55000);
        levels.add(82000);
        levels.add(98000);
        levels.add(118000);
        levels.add(142000);
        levels.add(170000);
        levels.add(204000);
        levels.add(245000);
        levels.add(294000);
        levels.add(353000);

        //21 - 30
        levels.add(423000);
        levels.add(508000);
        levels.add(609000);
        levels.add(701000);
        levels.add(806000);
        levels.add(927000);
        levels.add(1070000);
        levels.add(1230000);
        levels.add(1410000);
        levels.add(1620000);

        //31 - 40
        levels.add(1860000);
        levels.add(2050000);
        levels.add(2260000);
        levels.add(2480000);
        levels.add(2730000);
        levels.add(3000000);
        levels.add(3300000);
        levels.add(3630000);
        levels.add(4000000);
        levels.add(4400000);

        //41 - 50
        levels.add(4750000);
        levels.add(5130000);
        levels.add(5540000);
        levels.add(5980000);
        levels.add(6460000);
        levels.add(6970000);
        levels.add(7530000);
        levels.add(8140000);
        levels.add(8790000);
        levels.add(9490000);

        //51 - 60
        levels.add(10200000);
        levels.add(11100000);
        levels.add(11700000);
        levels.add(12400000);
        levels.add(13200000);
        levels.add(14000000);
        levels.add(14800000);
        levels.add(15700000);
        levels.add(16600000);
        levels.add(17600000);

        //61 - 70
        levels.add(18700000);
        levels.add(19800000);
        levels.add(21000000);
        levels.add(22300000);
        levels.add(23600000);
        levels.add(25000000);
        levels.add(26500000);
        levels.add(28100000);
        levels.add(29800000);
        levels.add(31600000);

        //71 - 80
        levels.add(33200000);
        levels.add(34800000);
        levels.add(36600000);
        levels.add(38400000);
        levels.add(40300000);
        levels.add(42300000);
        levels.add(44500000);
        levels.add(46700000);
        levels.add(49000000);
        levels.add(51500000);

        //81 - 90
        levels.add(54000000);
        levels.add(56700000);
        levels.add(59600000);
        levels.add(62000000);
        levels.add(64400000);
        levels.add(67000000);
        levels.add(69700000);
        levels.add(72500000);
        levels.add(75400000);
        levels.add(78400000);

        //91 - 100
        levels.add(81500000);
        levels.add(84800000);
        levels.add(88200000);
        levels.add(91700000);
        levels.add(95400000);
        levels.add(99200000);
        levels.add(103200000);
        levels.add(107300000);
        levels.add(111600000);
        levels.add(115000000);
        return levels;
    }

    public static Creep createCreepFromDTO(CreepDTO creepDTO) {
        Creep creep = new Creep();
        creep.setType(creepDTO.getType());
        creep.setCreepClass(creepDTO.getCreepClass());
        creep.setName(creepDTO.getName());
        creep.setLevel(creepDTO.getLevel());
        creep.setDropChances(createDropChancesFromId(creepDTO.getDropChancesId()));
//        creep.setPicture(creepDTO.getPicture());
        creep.setFinalBlockChance(creepDTO.getBlockChance());
        creep.setIsBoss(creepDTO.getIsBoss());

        creep.setFinalDamageMin(creepDTO.getDamageMin());
        creep.setFinalDamageMax(creepDTO.getDamageMax());
        creep.setFinalMagicDamage(creepDTO.getMagicDamage());
        creep.setFinalArmor(creepDTO.getArmor());
        creep.setFinalLife(creepDTO.getLife());
        creep.setFinalSpeed(creepDTO.getSpeed());
        creep.setExperience(creepDTO.getExperience());
        creep.setGold(creepDTO.getGold());
        return creep;
    }

    public FightResultBeanLight createLightBeanFromFightBean(FightResultBean fightResultBean) {
        FightResultBeanLight lightBean = new FightResultBeanLight();
        String fightLog = concatenateStringsWithLimit(fightResultBean.getFightLog()).toString();

        lightBean.setWinner(fightResultBean.getWinner().getName());
        lightBean.setLooser(fightResultBean.getLooser().getName());
        lightBean.setFightLog(fightLog);
        return lightBean;
    }

    private StringBuilder concatenateStringsWithLimit(List<String> list) {
        StringBuilder sb = new StringBuilder();
        if (list.size() <= Constants.SIZE_FIGHT_LOG_LINES) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(list.get(i));
                sb.append(Constants.LOG_SEPARATOR);
            }
        } else {
            for (int i = 0; i < Constants.FIRST_LINES_FIGHT_LOG; i++) {
                sb.append(list.get(i));
                sb.append(Constants.LOG_SEPARATOR);
            }

            sb.append(".......skipping some moves....................");
            sb.append(Constants.LOG_SEPARATOR);

            int index2 = list.size() - Constants.SIZE_FIGHT_LOG_LINES + Constants.FIRST_LINES_FIGHT_LOG;

            for (int i = index2; i < list.size(); i++) {
                sb.append(list.get(i));
                sb.append(Constants.LOG_SEPARATOR);
            }

        }
        return sb;
    }

    public static Skill skillsFactoryMethod(SkillClass skillClass) {
        Skill skill = null;
        switch (skillClass) {
            case Power:
                skill = new Power();
                break;
            case Swift:
                skill = new Swift();
                break;
            case Defence:
                skill = new Defence();
                break;
            case Toughness:
                skill = new Toughness();
                break;
            case GodShield:
                skill = new GodShield();
                break;
            case Spirit:
                skill = new Spirit();
                break;
            case Retard:
                skill = new Retard();
                break;
            case Weak:
                skill = new Weak();
                break;
            case Vulnerable:
                skill = new Vulnerable();
                break;
            case Invalid:
                skill = new Invalid();
                break;
            case Armless:
                skill = new Armless();
                break;
            case Shock:
                skill = new Shock();
                break;
            case Critical:
                skill = new Critical();
                break;
            case MultiHit:
                skill = new MultiHit();
                break;
            case Dynamite:
                skill = new Dynamite();
                break;
        }
        return skill;
    }

    public List<Skill> createListAvailableSkills(String str) {
        List<Skill> resultSkills = new ArrayList<>();
        List<String> skills = Arrays.asList(str.split(","));
        for (String skill : skills) {
            SkillClass skillClass = SkillClass.valueOf(skill);
            resultSkills.add(skillsFactoryMethod(skillClass));
        }
        return resultSkills;
    }
}
























