package com.stan.craft.Models;

import javax.persistence.*;

@Entity
@Table(name = "items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id")
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "item_class")
    private ItemClass itemClass;

    @Column(name = "is_unique")
    private int isUnique; //0 = No; 1 = Yes

    @Column(name = "name")
    private String name;

    @Column(name = "level")
    private int level;

    @Column(name = "price")
    private int price;

    @Column(name = "damage_min")
    private int damageMin;

    @Column(name = "damage_max")
    private int damageMax;

    @Column(name = "magic_damage")
    private int magicDamage;

    @Column(name = "armor")
    private int armor;

    @Column(name = "block_chance")
    private double blockChance;

    @Column(name = "speed")
    private double speed;

    @Column(name = "life")
    private int life;

    @Column(name = "rise")
    private double rise;

    @Column(name = "available_in_shop")
    private int availableInShop; //0 = No; 1 = Yes

    @Column(name = "capacity")
    private int capacity;

    @Column(name = "description")
    private String description;

    @Lob
    @Column(name = "picture")
    private String picture;

    public Item() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ItemClass getItemClass() {
        return itemClass;
    }

    public void setItemClass(ItemClass itemClass) {
        this.itemClass = itemClass;
    }

    public int getIsUnique() {
        return isUnique;
    }

    public void setIsUnique(int isUnique) {
        this.isUnique = isUnique;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDamageMin() {
        return damageMin;
    }

    public void setDamageMin(int damageMin) {
        this.damageMin = damageMin;
    }

    public int getDamageMax() {
        return damageMax;
    }

    public void setDamageMax(int damageMax) {
        this.damageMax = damageMax;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getAvailableInShop() {
        return availableInShop;
    }

    public void setAvailableInShop(int availableInShop) {
        this.availableInShop = availableInShop;
    }

    public double getBlockChance() {
        return blockChance;
    }

    public void setBlockChance(double blockChance) {
        this.blockChance = blockChance;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public double getRise() {
        return rise;
    }

    public void setRise(double rise) {
        this.rise = rise;
    }

    public int getMagicDamage() {
        return magicDamage;
    }

    public void setMagicDamage(int magicDamage) {
        this.magicDamage = magicDamage;
    }
}
