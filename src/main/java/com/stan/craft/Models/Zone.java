//package com.stan.craft.Models;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "zones")
//public class Zone {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int zoneId;
//    @Enumerated(EnumType.STRING)
//    private ZoneClass zoneClass;
//    private int requiredLevel;
//    private int levelFrom;
//    private int levelTo;
//    private IndividualType individualType;
//
//    public Zone() {
//    }
//
//    public int getZoneId() {
//        return zoneId;
//    }
//
//    public void setZoneId(int zoneId) {
//        this.zoneId = zoneId;
//    }
//
//    public ZoneClass getZoneClass() {
//        return zoneClass;
//    }
//
//    public void setZoneClass(ZoneClass zoneClass) {
//        this.zoneClass = zoneClass;
//    }
//
//    public int getRequiredLevel() {
//        return requiredLevel;
//    }
//
//    public void setRequiredLevel(int requiredLevel) {
//        this.requiredLevel = requiredLevel;
//    }
//
//    public int getLevelFrom() {
//        return levelFrom;
//    }
//
//    public void setLevelFrom(int levelFrom) {
//        this.levelFrom = levelFrom;
//    }
//
//    public int getLevelTo() {
//        return levelTo;
//    }
//
//    public void setLevelTo(int levelTo) {
//        this.levelTo = levelTo;
//    }
//
//    public IndividualType getIndividualType() {
//        return individualType;
//    }
//
//    public void setIndividualType(IndividualType individualType) {
//        this.individualType = individualType;
//    }
//}
