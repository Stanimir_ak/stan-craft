package com.stan.craft.Models;

public enum ItemClass {
  WEAPON,
  SHIELD,
  ARMOR,
  HELM,
  AMULET,
  DAGGER,
  BAG,
  RING,
  BOOTS,
  GLOVES,
  CRYSTAL
}
