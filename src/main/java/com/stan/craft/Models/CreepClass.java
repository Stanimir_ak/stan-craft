package com.stan.craft.Models;

public enum CreepClass {
    Tiger,
    Zombie,
    Dragon,
    Vampire,
    Cobra,
    Death,
    Undead,
    Criminal,
    Rex,
    Monster,
    God
}
