//package com.stan.craft.Models;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "skills_private")
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//public abstract class SkillPrivate {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "skill_id")
//    private int id;
//
//    @Column(name = "name")
//    private SkillClass skillClass;
//
//    @Column(name = "description")
//    private String description;
//
//    @Column(name = "level")
//    private int level = 1;
//
//    private SkillType type;
//
//    private String availability;
//    //example CharacterClass.Knight.name() + ',' + CharacterClass.Shaman.name();
//
//    public SkillPrivate() {
//    }
//
//    public abstract Character applySkill(Character character);
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public SkillClass getSkillClass() {
//        return skillClass;
//    }
//
//    public void setSkillClass(SkillClass skillClass) {
//        this.skillClass = skillClass;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public int getLevel() {
//        return level;
//    }
//
//    public void setLevel(int level) {
//        this.level = level;
//    }
//
//    public SkillType getType() {
//        return type;
//    }
//
//    public void setType(SkillType type) {
//        this.type = type;
//    }
//
//    public String getAvailability() {
//        return availability;
//    }
//
//    public void setAvailability(String availability) {
//        this.availability = availability;
//    }
//}
