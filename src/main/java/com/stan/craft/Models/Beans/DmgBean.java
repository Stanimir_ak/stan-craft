package com.stan.craft.Models.Beans;

public class DmgBean {
    private int dmgMin;
    private int dmgMax;
    private int dmgMagic;

    public DmgBean(int dmgMin, int dmgMax, int dmgMagic) {
        this.dmgMin = dmgMin;
        this.dmgMax = dmgMax;
        this.dmgMagic = dmgMagic;
    }

    public int getDmgMin() {
        return dmgMin;
    }

    public void setDmgMin(int dmgMin) {
        this.dmgMin = dmgMin;
    }

    public int getDmgMax() {
        return dmgMax;
    }

    public void setDmgMax(int dmgMax) {
        this.dmgMax = dmgMax;
    }

    public int getDmgMagic() {
        return dmgMagic;
    }

    public void setDmgMagic(int dmgMagic) {
        this.dmgMagic = dmgMagic;
    }
}
