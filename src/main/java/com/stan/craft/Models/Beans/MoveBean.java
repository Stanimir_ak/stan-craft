package com.stan.craft.Models.Beans;

public class MoveBean {
    private int move; //left = 1; right = 2; up = 3; down = 4;

    public MoveBean() {
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }
}
