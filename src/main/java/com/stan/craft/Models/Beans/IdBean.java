package com.stan.craft.Models.Beans;

public class IdBean {
    int id;

    public IdBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
