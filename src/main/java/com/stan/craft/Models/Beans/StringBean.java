package com.stan.craft.Models.Beans;

/**
 * used in HTML in order to have several POST methods in one Controller/URL
 * the properties string1, 2, 3... are just a data careers.
 */
public class StringBean {
    private String method;
    private String string1;
    private String string2;
    private String string3;

    public StringBean() {
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getString1() {
        return string1;
    }

    public void setString1(String string1) {
        this.string1 = string1;
    }

    public String getString2() {
        return string2;
    }

    public void setString2(String string2) {
        this.string2 = string2;
    }

    public String getString3() {
        return string3;
    }

    public void setString3(String string3) {
        this.string3 = string3;
    }
}
