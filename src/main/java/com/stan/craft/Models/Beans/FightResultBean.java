package com.stan.craft.Models.Beans;

import com.stan.craft.Models.Individual;
import com.stan.craft.Models.ItemPrivate;

import java.util.List;


public class FightResultBean {
    //FightResultBean or List<List<String>> fight() --->>>
    // (List1 = winner; 2 = exp; 3 = gold; 4 = Item; 5 = log with moves)

    private Individual winner;
    private Individual looser;
    private long experience;
    private long gold;
    private ItemPrivate itemDropped;
    private List<String> fightLog;

    public FightResultBean() {
    }

    public Individual getWinner() {
        return winner;
    }

    public void setWinner(Individual winner) {
        this.winner = winner;
    }

    public long getExperience() {
        return experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public long getGold() {
        return gold;
    }

    public void setGold(long gold) {
        this.gold = gold;
    }

    public ItemPrivate getItemDropped() {
        return itemDropped;
    }

    public void setItemDropped(ItemPrivate itemDropped) {
        this.itemDropped = itemDropped;
    }

    public List<String> getFightLog() {
        return fightLog;
    }

    public void setFightLog(List<String> fightLog) {
        this.fightLog = fightLog;
    }

    public Individual getLooser() {
        return looser;
    }

    public void setLooser(Individual looser) {
        this.looser = looser;
    }
}
