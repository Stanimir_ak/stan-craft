package com.stan.craft.Models.Beans;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity
@Table(name = "fight_log")
public class FightResultBeanLight {

    @Id
    @Column(name = "log_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "date")
    private String date;

    @Column(name = "winner")
    private String winner;

    @Column(name = "looser")
    private String looser;

    @Column(name = "log")
    @Size(max = 4000)
    private String fightLog;

    public FightResultBeanLight() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        this.date = formatter.format(date);
        Date date1 = new Date();
        date1.setTime(date1.getTime() - 864000000L); // 10 days in milliseconds
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLooser() {
        return looser;
    }

    public void setLooser(String looser) {
        this.looser = looser;
    }

    public String getFightLog() {
        return fightLog;
    }

    public void setFightLog(String fightLog) {
        this.fightLog = fightLog;
    }
}
