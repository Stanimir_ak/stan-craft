package com.stan.craft.Models;

public enum SkillType {
    Passive,
    Curse,
    Active
}
