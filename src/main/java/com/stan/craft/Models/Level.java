package com.stan.craft.Models;

import javax.persistence.*;

@Entity
@Table(name = "levels")
public class Level {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "exp_reaching")
    private long expReaching;

    @Column(name = "exp_next_level")
    private long expNextLevel;

    public Level(long expReaching, long expNextLevel) {
        this.expReaching = expReaching;
        this.expNextLevel = expNextLevel;
    }

    public Level() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getExpReaching() {
        return expReaching;
    }

    public void setExpReaching(long expReaching) {
        this.expReaching = expReaching;
    }

    public long getExpNextLevel() {
        return expNextLevel;
    }

    public void setExpNextLevel(long expNextLevel) {
        this.expNextLevel = expNextLevel;
    }
}
