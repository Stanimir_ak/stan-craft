package com.stan.craft.Models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
//@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(name = "default_gen", sequenceName = "ind_seq", initialValue = 10000, allocationSize = 1)
public abstract class Individual implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id = 0;

    @Enumerated(EnumType.STRING)
    private IndividualType type;
    private String name;
    private int level = 1;
    private long experience = 50L;
    private long gold = 50L;
    private int damageMinBase;
    private int damageMin;
    private int equipDamageMin;
    private int finalDamageMin;
    private int damageMaxBase;
    private int damageMax;
    private int equipDamageMax;
    private int finalDamageMax;
    private int magicDamageBase;
    private int magicDamage;
    private int equipMagicDamage;
    private int finalMagicDamage;
    private int armorBase;
    private int armor;
    private int equipArmor;
    private int finalArmor;
    private double blockChance;
    private double equipBlockChance;
    private double finalBlockChance;
    private int lifeBase;
    private int life;
    private int equipLife;
    private int finalLife;
    private double speed;
    private double equipSpeed;
    private double finalSpeed;
    private String userCreator;

    //    @ManyToMany
//    @LazyCollection(LazyCollectionOption.FALSE)
//    @JsonBackReference
//    @JoinTable(
//            name = "individuals_skills",
//            joinColumns = @JoinColumn(name = "id"),
//            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    @OneToMany(mappedBy = "individual")
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonBackReference
    private List<Skill> skills = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "drop_id", referencedColumnName = "drop_id")
    private DropChances dropChances;

    public Individual() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public IndividualType getType() {
        return type;
    }

    public void setType(IndividualType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getExperience() {
        return experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public long getGold() {
        return gold;
    }

    public void setGold(long gold) {
        this.gold = gold;
    }

    public int getDamageMinBase() {
        return damageMinBase;
    }

    public void setDamageMinBase(int damageMinBase) {
        this.damageMinBase = damageMinBase;
    }

    public int getDamageMin() {
        return damageMin;
    }

    public void setDamageMin(int damageMin) {
        this.damageMin = damageMin;
    }

    public int getEquipDamageMin() {
        return equipDamageMin;
    }

    public void setEquipDamageMin(int equipDamageMin) {
        this.equipDamageMin = equipDamageMin;
    }

    public int getFinalDamageMin() {
        return finalDamageMin;
    }

    public void setFinalDamageMin(int finalDamageMin) {
        this.finalDamageMin = finalDamageMin;
    }

    public int getDamageMaxBase() {
        return damageMaxBase;
    }

    public void setDamageMaxBase(int damageMaxBase) {
        this.damageMaxBase = damageMaxBase;
    }

    public int getDamageMax() {
        return damageMax;
    }

    public void setDamageMax(int damageMax) {
        this.damageMax = damageMax;
    }

    public int getEquipDamageMax() {
        return equipDamageMax;
    }

    public void setEquipDamageMax(int equipDamageMax) {
        this.equipDamageMax = equipDamageMax;
    }

    public int getFinalDamageMax() {
        return finalDamageMax;
    }

    public void setFinalDamageMax(int finalDamageMax) {
        this.finalDamageMax = finalDamageMax;
    }

    public int getMagicDamageBase() {
        return magicDamageBase;
    }

    public void setMagicDamageBase(int magicDamageBase) {
        this.magicDamageBase = magicDamageBase;
    }

    public int getMagicDamage() {
        return magicDamage;
    }

    public void setMagicDamage(int magicDamage) {
        this.magicDamage = magicDamage;
    }

    public int getEquipMagicDamage() {
        return equipMagicDamage;
    }

    public void setEquipMagicDamage(int equipMagicDamage) {
        this.equipMagicDamage = equipMagicDamage;
    }

    public int getFinalMagicDamage() {
        return finalMagicDamage;
    }

    public void setFinalMagicDamage(int finalMagicDamage) {
        this.finalMagicDamage = finalMagicDamage;
    }

    public int getArmorBase() {
        return armorBase;
    }

    public void setArmorBase(int armorBase) {
        this.armorBase = armorBase;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getEquipArmor() {
        return equipArmor;
    }

    public void setEquipArmor(int equipArmor) {
        this.equipArmor = equipArmor;
    }

    public int getFinalArmor() {
        return finalArmor;
    }

    public void setFinalArmor(int finalArmor) {
        this.finalArmor = finalArmor;
    }

    public double getBlockChance() {
        return blockChance;
    }

    public void setBlockChance(double blockChance) {
        this.blockChance = blockChance;
    }

    public double getEquipBlockChance() {
        return equipBlockChance;
    }

    public void setEquipBlockChance(double equipBlockChance) {
        this.equipBlockChance = equipBlockChance;
    }

    public double getFinalBlockChance() {
        return finalBlockChance;
    }

    public void setFinalBlockChance(double finalBlockChance) {
        this.finalBlockChance = finalBlockChance;
    }

    public int getLifeBase() {
        return lifeBase;
    }

    public void setLifeBase(int lifeBase) {
        this.lifeBase = lifeBase;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getEquipLife() {
        return equipLife;
    }

    public void setEquipLife(int equipLife) {
        this.equipLife = equipLife;
    }

    public int getFinalLife() {
        return finalLife;
    }

    public void setFinalLife(int finalLife) {
        this.finalLife = finalLife;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getEquipSpeed() {
        return equipSpeed;
    }

    public void setEquipSpeed(double equipSpeed) {
        this.equipSpeed = equipSpeed;
    }

    public double getFinalSpeed() {
        return finalSpeed;
    }

    public void setFinalSpeed(double finalSpeed) {
        this.finalSpeed = finalSpeed;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public DropChances getDropChances() {
        return dropChances;
    }

    public void setDropChances(DropChances dropChances) {
        this.dropChances = dropChances;
    }

    public String getUserCreator() {
        return userCreator;
    }

    public void setUserCreator(String userCreator) {
        this.userCreator = userCreator;
    }
}
