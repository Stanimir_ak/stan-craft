package com.stan.craft.Models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
/**
 * To create Skill: Strategy pattern
 * 1.Create subclass
 * 2.Implement abstract methods
 * 3.Create constants
 * 4.Override the constructor and setValue()
 * 5.if(active skill) -> implement it in CharacterService fight() method
 * 6.Add picture in resources/static/images/skills
 * 7.Add still to ModelFactory.skillsFactoryMethod()
 */
@Entity
@Table(name = "skills")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "skill_id")
    private int id;

    @Column(name = "level")
    private int level = 1;

    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id")
    @JsonBackReference
    private Individual individual;

    public Skill() {
    }

    public abstract Individual applySkill(Individual individual);
    public abstract SkillType getType();
    public abstract SkillClass getSkillClass();
    public abstract String getDescription();
    public abstract String getAvailability();

    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
