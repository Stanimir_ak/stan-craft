//package com.stan.craft.Repositories;
//
//import com.stan.craft.Models.Skill;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class SkillRepositoryImpl implements SkillRepository {
//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public SkillRepositoryImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public Skill create(Skill skill) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.save(skill);
//            session.getTransaction().commit();
//            session.close();
//            return skill;
//        }
//    }
//
//    @Override
//    public Skill getOne(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Skill> query = session.createQuery("FROM Skill WHERE id = :id");
//            query.setParameter("id", id);
//            List<Skill> skills = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return skills == null ? null : skills.get(0);
//        }
//    }
//
//    @Override
//    public List<Skill> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Skill> query = session.createQuery("FROM Skill");
//            List<Skill> skills = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return skills;
//        }
//    }
//
//    @Override
//    public Skill update(Skill skill) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(skill);
//            session.getTransaction().commit();
//            session.close();
//            return skill;
//        }
//    }
//
//    @Override
//    public Skill delete(int skillId) {
//        Skill skill = getOne(skillId);
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.delete(skill);
//            session.getTransaction().commit();
//            session.close();
//            return skill;
//        }
//    }
//}
