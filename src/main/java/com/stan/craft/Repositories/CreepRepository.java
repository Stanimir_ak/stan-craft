package com.stan.craft.Repositories;

import com.stan.craft.Models.Character;
import com.stan.craft.Models.Creep;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreepRepository extends JpaRepository<Creep, Integer>, JpaSpecificationExecutor<Creep> {

    @Query(value = " SELECT c from Creep c WHERE c.level >= :levelFrom and c.level <= :levelTo and isBoss = 0 ")
    List<Creep> getCreepsByLevel(@Param("levelFrom") int levelFrom, @Param("levelTo") int levelTo);

    @Query(value = " SELECT c from Creep c WHERE c.level >= :levelFrom and c.level <= :levelTo and isBoss = 1 ")
    List<Creep> getBossesByLevel(@Param("levelFrom") int levelFrom, @Param("levelTo") int levelTo);

    @Query(value = " SELECT c from Creep c WHERE isBoss = 0 ")
    List<Creep> getAllNormalCreeps();

    @Query(value = " SELECT c from Creep c WHERE isBoss = 1 ")
    List<Creep> getAllBosses();

    @Query(value = " SELECT c from Creep c WHERE creepClass = 'God' ")
    List<Creep> getAllGods();

//    List<Item> dropItems(Creep creep, Character character);

    //    @Query(nativeQuery = true, value = " SELECT c.id from Creep as c order by c.id desc LIMIT 1 ")
    Creep findFirstByOrderByIdDesc();
}
