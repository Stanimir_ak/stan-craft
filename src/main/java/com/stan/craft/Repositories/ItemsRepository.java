package com.stan.craft.Repositories;

import com.stan.craft.Models.DropChances;
import com.stan.craft.Models.Item;
import com.stan.craft.Models.ItemClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemsRepository extends JpaRepository<Item, Integer>, JpaSpecificationExecutor<Item> {

    @Query(value = " SELECT i FROM Item i WHERE i.level = :level ")
    List<Item> getItemsAccordingToLevel(@Param("level") int level);

    @Query(value = " SELECT i FROM Item i WHERE i.level = :level AND i.itemClass = :itemClass ")
    Item getOneByLevelAndClass(@Param("level") int level, @Param("itemClass") ItemClass itemClass);

    @Query(value = " SELECT i FROM Item i WHERE i.name = :name ")
    Item getOneByName(@Param("name") String name);
}
