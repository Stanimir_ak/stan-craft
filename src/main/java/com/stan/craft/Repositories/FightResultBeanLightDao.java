package com.stan.craft.Repositories;

import com.stan.craft.Models.Beans.FightResultBeanLight;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface FightResultBeanLightDao extends JpaRepository<FightResultBeanLight, Long> {

    @Query(value = " SELECT f from FightResultBeanLight f WHERE f.winner = :charName OR f.looser = :charName ")
    Slice<FightResultBeanLight> getFightLogByCharacter(Pageable pageable, @Param("charName") String charName);

    @Transactional
    @Modifying
    @Query(value = " DELETE from FightResultBeanLight f where f.id <= ((SELECT max(g.id) from FightResultBeanLight g) - :MAX_FIGHT_LOG_COUNT) ")
    void deleteOldFightLog(@Param("MAX_FIGHT_LOG_COUNT") long MAX_FIGHT_LOG_COUNT);
}
