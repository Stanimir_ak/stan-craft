//package com.stan.craft.Repositories;
//
//import com.stan.craft.Models.Beans.LogBean;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//@Transactional
//@Repository
//public class LogDaoImpl implements LogDao {
//
//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public LogDaoImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public List<LogBean> getLogByUsername(int LOGS_PER_PAGE, int page, String username) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//
////            List users = session.createCriteria(LogBean.class)
////                    .addOrder(Order.asc("id"))
////                    .setFirstResult(0) // Index of first row to be retrieved.
////                    .setMaxResults(10) // Amount of rows to be retrieved.
////                    .list();
////********************************************************
//            String hql = " FROM LogBean WHERE username = :username ORDER BY id DESC ";
//            int firstResult = LOGS_PER_PAGE * (page - 1);
//            Query query = session.createQuery(hql)
//                    .setFirstResult(firstResult)
//                    .setMaxResults(LOGS_PER_PAGE);
//            query.setParameter("username", username);
//            List<LogBean> logs = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return logs;
//
////********************************************************
//
//
////            String hql = "FROM LogBean WHERE username = :username";
////            Query query = session.createQuery(hql);
////            query.setParameter("username", username);
////            int pageSize = 10;
////
////            ScrollableResults resultScroll = query.scroll(ScrollMode.FORWARD_ONLY);
////            resultScroll.first();
////            resultScroll.scroll(0);
//////            List<LogBean> log = Lists.newArrayList();
////            List<LogBean> log = new ArrayList<>();
////            int i = 0;
////            while (pageSize > i++) {
////                log.add((LogBean) resultScroll.get(0));
////                if (!resultScroll.next())
////                    break;
////            }
////
////            //I must make my own implementation of Pageable!
////            if (log.size() == 0) {
////                return null;
////            } else {
////                return log;
////            }
//        }
//    }
//
//    @Override
//    public LogBean logMessage(LogBean logBean) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.save(logBean);
//            session.getTransaction().commit();
//            session.close();
//            return logBean;
//        }
//    }
//
//    @Override
//    public void deleteOldLog(long MAX_MAIN_LOG_COUNT) {
//        try (Session session = sessionFactory.openSession()) {
//            String hql = " DELETE from LogBean lb where lb.id <= ((SELECT max(g.id) from LogBean g) - :maxCount) ";
//            session.beginTransaction();
//            Query query = session.createQuery(hql);
//            query.setParameter("maxCount", MAX_MAIN_LOG_COUNT);
//            query.executeUpdate();
//            session.getTransaction().commit();
//            session.close();
//        }
//    }
//}
