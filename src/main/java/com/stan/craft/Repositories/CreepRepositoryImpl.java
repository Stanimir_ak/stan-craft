//package com.stan.craft.Repositories;
//
//import com.stan.craft.Constants.StaticVar;
//import com.stan.craft.Models.Character;
//import com.stan.craft.Models.Creep;
//import com.stan.craft.Models.Item;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class CreepRepositoryImpl implements CreepRepository {
//
//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public CreepRepositoryImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public Creep getOne(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep WHERE id = :id");
//            query.setParameter("id", id);
//            List<Creep> creeps = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return creeps == null ? null : creeps.get(0);
//        }
//    }
//
//    @Override
//    public List<Creep> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep");
//            List<Creep> creeps = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return creeps;
//        }
//    }
//
//    @Override
//    public List<Creep> getCreepsByLevel(int levelFrom, int levelTo) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep WHERE level >= :levelFrom AND level <= :levelTo AND isBoss = 0");
//            query.setParameter("levelFrom", levelFrom);
//            query.setParameter("levelTo", levelTo);
//            List<Creep> creeps = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return creeps;
//        }
//    }
//
//    @Override
//    public List<Creep> getBossesByLevel(int levelFrom, int levelTo) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep WHERE level >= :levelFrom AND level <= :levelTo AND isBoss = 1");
//            query.setParameter("levelFrom", levelFrom);
//            query.setParameter("levelTo", levelTo);
//            List<Creep> bosses = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return bosses;
//        }
//    }
//
//    @Override
//    public int getGamesNumberOfCreeps() {
//        try (Session session = sessionFactory.openSession()) {
//            return session.createQuery("SELECT COUNT(id) FROM Creep ").getFirstResult();
//            //TODO DOES NOT WORK!!
//        }
//    }
//
//    @Override
//    public List<Creep> getAllNormalCreeps() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep WHERE isBoss = 0");
//            List<Creep> creeps = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return creeps;
//        }
//    }
//
//    @Override
//    public List<Creep> getAllBosses() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep WHERE isBoss = 1");
//            List<Creep> bosses = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return bosses;
//        }
//    }
//
//    @Override
//    public List<Creep> getAllGods() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep WHERE creepClass = 'God' ");
//            List<Creep> gods = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return gods;
//        }
//    }
//
//    @Override
//    public Creep create(Creep creep) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            creep.setId(StaticVar.IndividualId);
//            StaticVar.IndividualId++;
//            session.save(creep);
//            session.getTransaction().commit();
//            session.close();
//            return creep;
//        }
//    }
//
//    @Override
//    public Creep update(Creep creep) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(creep);
//            session.getTransaction().commit();
//            session.close();
//            return creep;
//        }
//    }
//
//    @Override
//    public Creep delete(int id) {
//        Creep creep = getOne(id);
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.delete(creep);
//            session.getTransaction().commit();
//            session.close();
//            return creep;
//        }
//    }
//
//    @Override
//    public List<Item> dropItems(Creep creep, Character character) {
//        return null;
//    }
//
//    @Override
//    public int getMaxId() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Creep> query = session.createQuery("FROM Creep ORDER BY id DESC ");
//            query.setMaxResults(1);
//            List<Creep> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            if (list.size() == 0) {
//                return 0;
//            }
//            return list.get(0).getId();
//        }
//    }
//}
