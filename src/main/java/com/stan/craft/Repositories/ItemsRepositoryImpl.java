//package com.stan.craft.Repositories;
//
//import com.stan.craft.Models.Item;
//import com.stan.craft.Models.ItemClass;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class ItemsRepositoryImpl implements ItemsRepository {
//
//    SessionFactory sessionFactory;
//
//    @Autowired
//    public ItemsRepositoryImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public List<Item> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Item> query = session.createQuery("FROM Item");
//            List<Item> items = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return items;
//        }
//    }
//
//    @Override
//    public List<Item> getItemsAccordingToLevel(int level) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Item> query = session.createQuery("FROM Item WHERE level = :level");
//            query.setParameter("level", level);
//            List<Item> items = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return items;
//        }
//    }
//
//    @Override
//    public Item getOne(int itemId) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Item> query = session.createQuery("FROM Item WHERE id = :xxx");
//            query.setParameter("xxx", itemId);
//            List<Item> items = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return items == null ? null : items.get(0);
//        }
//    }
//
//    @Override
//    public Item getOneByLevelAndClass(int level, ItemClass itemClass) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Item> query = session.createQuery("FROM Item WHERE level = :level AND itemClass = :itemClass");
//            query.setParameter("level", level);
//            query.setParameter("itemClass", itemClass);
//            List<Item> items = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return items == null ? null : items.get(0);
//        }
//    }
//
//    @Override
//    public Item getOneByName(String name) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Item> query = session.createQuery("FROM Item WHERE name = :name");
//            query.setParameter("name", name);
//            List<Item> items = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return items == null ? null : items.get(0);
//        }
//    }
//
//    @Override
//    public Item create(Item item) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.save(item);
//            session.getTransaction().commit();
//            session.close();
//            return item;
//        }
//    }
//
//    @Override
//    public Item update(Item item) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(item);
//            session.getTransaction().commit();
//            session.close();
//            return item;
//        }
//    }
//
//    @Override
//    public Item delete(int itemId) {
//        try (Session session = sessionFactory.openSession()) {
//            Item itemToDelete = getOne(itemId);
//            session.beginTransaction();
//            session.delete(itemToDelete);
//            session.getTransaction().commit();
//            session.close();
//            return itemToDelete;
//        }
//    }
//
//    @Override
//    public boolean ifExists(int itemId) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Item> query = session.createQuery("FROM Item WHERE id = :xxx");
//            query.setParameter("xxx", itemId);
//            boolean ifExists = query.list().size() == 1;
//            session.getTransaction().commit();
//            session.close();
//            return ifExists;
//        }
//    }
//}
