//package com.stan.craft.Repositories;
//
//import com.stan.craft.Constants.Constants;
//import com.stan.craft.Constants.StaticVar;
//import com.stan.craft.Models.Character;
//import com.stan.craft.Models.Level;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class CharacterRepositoryImpl implements CharacterRepository {
//
//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public CharacterRepositoryImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public List<Character> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character");
//            List<Character> chars = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return chars;
//        }
//    }
//
//    @Override
//    public List<Character> getAllSortedByExp() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character ORDER BY experience DESC ");
//            List<Character> chars = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return chars;
//        }
//    }
//
//    @Override
//    public Character getOne(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character WHERE id = :xxx");
//            query.setParameter("xxx", id);
//            List<Character> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list == null ? null : list.get(0);
//        }
//    }
//
//    @Override
//    public Character getOneByName(String name) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character WHERE name = :xxx");
//            query.setParameter("xxx", name);
//            List<Character> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list == null ? null : list.get(0);
//        }
//    }
//
//    @Override
//    public Character create(Character character) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            character.setId(StaticVar.IndividualId);
//            StaticVar.IndividualId++;
//            session.save(character);
//            session.getTransaction().commit();
//            session.close();
//            return character;
//        }
//    }
//
//    @Override
//    public Character update(Character character) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(character);
//            session.getTransaction().commit();
//            session.close();
//            return character;
//        }
//    }
//
//    @Override
//    public Character delete(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            Character charToDelete = getOne(id);
//            session.beginTransaction();
//            session.delete(charToDelete);
//            session.getTransaction().commit();
//            session.close();
//            return charToDelete;
//        }
//    }
//
//    @Override
//    public boolean ifExists(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character WHERE id = :xxx");
//            query.setParameter("xxx", id);
//            List<Character> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list.size() == 1;
//        }
//    }
//
//    @Override
//    public boolean ifExists(String characterName) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character WHERE name = :xxx");
//            query.setParameter("xxx", characterName);
//            List<Character> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list.size() == 1;
//        }
//    }
//
//    @Override
//    public boolean nameIsFree(String characterName) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character WHERE name = :xxx");
//            query.setParameter("xxx", characterName);
//            List<Character> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list.size() == 0;
//        }
//    }
//
//    @Override
//    public int calculateLevel(Character character) {
//        if (character.getLevel() == Constants.MAX_CHARACTER_LEVEL) {
//            return character.getLevel();
//        }
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Level> query = session.createQuery("FROM Level WHERE expReaching <= :xxx " +
//                    "AND expNextLevel > :xxx");
//            query.setParameter("xxx", character.getExperience());
//            List<Level> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            if (list.size() == 0) {
//                System.out.println("Bug! - calculating level of char - experience not mach in DB");
//            }
//            return list.get(0).getId();
//        }
//    }
//
//    @Override
//    public int getGamesNumberOfItems() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            int n = session.createQuery("SELECT COUNT(id) FROM Item ").executeUpdate();
//            session.getTransaction().commit();
//            session.close();
//            return n;
//        }
//    }
//
//    @Override
//    public List<Character> getFighters() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character WHERE zone <> 'Rest' ");
//            List<Character> chars = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return chars;
//        }
//    }
//
//    @Override
//    public int getMaxId() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<Character> query = session.createQuery("FROM Character ORDER BY id DESC ");
//            query.setMaxResults(1);
//            List<Character> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            if (list.size() == 0) {
//                return 0;
//            }
//            return list.get(0).getId();
//        }
//    }
//}
