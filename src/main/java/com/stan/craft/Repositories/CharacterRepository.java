package com.stan.craft.Repositories;

import com.stan.craft.Models.Character;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer>, JpaSpecificationExecutor<Character> {

    @NotNull List<Character> findAll(@NotNull Sort sort);

    @NotNull List<Character> findAll();

    Character getOneByName(String name);

    Optional<Character> findByName(String name);

    @Query(value = " SELECT c FROM Character c WHERE c.zone <> 'Rest' ")
    List<Character> bringAllFighters();

    //    @Query(value = " SELECT c.id from Character as c order by c.id desc LIMIT 1 ")
    Character findFirstByOrderByIdDesc();
}
