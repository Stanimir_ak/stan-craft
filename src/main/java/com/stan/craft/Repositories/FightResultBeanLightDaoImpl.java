//package com.stan.craft.Repositories;
//
//import com.stan.craft.Exceptions.EntityNotFoundException;
//import com.stan.craft.Models.Beans.FightResultBeanLight;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class FightResultBeanLightDaoImpl implements FightResultBeanLightDao {
//
//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public FightResultBeanLightDaoImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public FightResultBeanLight getOne(long id) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<FightResultBeanLight> query = session.createQuery("FROM FightResultBeanLight WHERE id = :id");
//            query.setParameter("id", id);
//            List<FightResultBeanLight> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            if (list == null || list.size() == 0) {
//                throw new EntityNotFoundException("No such log id");
//            }
//            return list.get(0);
//        }
//    }
//
//    @Override
//    public List<FightResultBeanLight> getFightLogByCharacter(int FIGHT_LOGS_PER_PAGE, int page, String charName) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            String hql = " " +
//                    "  FROM FightResultBeanLight " +
//                    "  WHERE winner = :charName " +
//                    "  OR looser = :charName " +
//                    "  ORDER BY id DESC ";
//            int firstResult = FIGHT_LOGS_PER_PAGE * (page - 1);
//            Query query = session.createQuery(hql)
//                    .setFirstResult(firstResult)
//                    .setMaxResults(FIGHT_LOGS_PER_PAGE);
//            query.setParameter("charName", charName);
//            List<FightResultBeanLight> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list;
//        }
//
//    }
//
//    @Override
//    public FightResultBeanLight logMessage(FightResultBeanLight fightResultBeanLight) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.save(fightResultBeanLight);
//            session.getTransaction().commit();
//            session.close();
//            return fightResultBeanLight;
//        }
//    }
//
//    @Override
//    public void deleteOldFightLog(long MAX_FIGHT_LOG_COUNT) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            String hql = " DELETE from FightResultBeanLight f where f.id <= " +
//                    " ((SELECT max(g.id) from FightResultBeanLight g) - :maxCount) ";
//            Query query = session.createQuery(hql);
//            query.setParameter("maxCount", MAX_FIGHT_LOG_COUNT);
//            query.executeUpdate();
//            session.getTransaction().commit();
//            session.close();
//        }
//    }
//}
