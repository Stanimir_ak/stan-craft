package com.stan.craft.Repositories;

import com.stan.craft.Models.Beans.LogBean;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface LogDao extends JpaRepository<LogBean, Long> {

    @Query(value = " SELECT lb from LogBean lb WHERE lb.username = :username ")
    Slice<LogBean> getLogByUsername(Pageable pageable, @Param("username") String username);

    @Transactional
    @Modifying
    @Query(value = " DELETE from LogBean lb where lb.id <= ((SELECT max(g.id) from LogBean g) - :MAX_MAIN_LOG_COUNT) ")
    void deleteOldLog(@Param("MAX_MAIN_LOG_COUNT") long MAX_MAIN_LOG_COUNT);
}
