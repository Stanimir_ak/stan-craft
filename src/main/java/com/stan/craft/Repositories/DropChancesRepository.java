package com.stan.craft.Repositories;

import com.stan.craft.Models.Creep;
import com.stan.craft.Models.DropChances;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DropChancesRepository extends JpaRepository<DropChances, Integer>, JpaSpecificationExecutor<DropChances> {
}
