//package com.stan.craft.Repositories;
//
//import com.stan.craft.Models.ItemPrivate;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class ItemPrivateRepositoryImpl implements ItemPrivateRepository {
//
//    private SessionFactory sessionFactory;
//
//    @Autowired
//    public ItemPrivateRepositoryImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public List<ItemPrivate> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<ItemPrivate> query = session.createQuery("FROM ItemPrivate");
//            List<ItemPrivate> items = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return items;
//        }
//    }
//
//    @Override
//    public ItemPrivate getOne(int itemId) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<ItemPrivate> query = session.createQuery("FROM ItemPrivate WHERE id = :xxx");
//            query.setParameter("xxx", itemId);
//            List<ItemPrivate> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list == null ? null : list.get(0);
//        }
//    }
//
//    @Override
//    public ItemPrivate create(ItemPrivate item) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.save(item);
//            session.getTransaction().commit();
//            session.close();
//            return item;
//        }
//    }
//
//    @Override
//    public ItemPrivate update(ItemPrivate item) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(item);
//            session.getTransaction().commit();
//            session.close();
//            return item;
//        }
//    }
//
//    @Override
//    public ItemPrivate delete(int itemId) {
//        try (Session session = sessionFactory.openSession()) {
//            ItemPrivate itemToDelete = getOne(itemId);
//            session.beginTransaction();
//            session.delete(itemToDelete);
//            session.getTransaction().commit();
//            session.close();
//            return itemToDelete;
//        }
//    }
//
//    @Override
//    public boolean ifExists(int itemId) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<ItemPrivate> query = session.createQuery("FROM ItemPrivate WHERE id = :xxx");
//            query.setParameter("xxx", itemId);
//            boolean ifExists = query.list().size() == 1;
//            session.getTransaction().commit();
//            session.close();
//            return ifExists;
//        }
//    }
//}
