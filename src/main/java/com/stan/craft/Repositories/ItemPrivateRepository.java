package com.stan.craft.Repositories;

import com.stan.craft.Models.ItemPrivate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ItemPrivateRepository extends JpaRepository<ItemPrivate, Integer>, JpaSpecificationExecutor<ItemPrivate> {
    @Transactional
    @Modifying
    @Query(value = " DELETE from ItemPrivate i where i.id = :id ")
    void forceDelete(@Param("id") int id);

    @Transactional
    @Modifying
    @Query(value = " DELETE from ItemPrivate i where i.markedForDelete = '1' ")
    void deleteItemsMarkedForDelete();

    @Query(value = " SELECT i.id from ItemPrivate i where i.markedForDelete = '1' ")
    List<Integer> getItemsIDMarkedForDelete();
}
