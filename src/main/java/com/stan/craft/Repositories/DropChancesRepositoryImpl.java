//package com.stan.craft.Repositories;
//
//import com.stan.craft.Models.DropChances;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class DropChancesRepositoryImpl implements DropChancesRepository {
//
//    private SessionFactory sessionFactory;
//
//    @Autowired
//    public DropChancesRepositoryImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public DropChances create(DropChances dropChances) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.save(dropChances);
//            session.getTransaction().commit();
//            session.close();
//            return dropChances;
//        }
//    }
//
//    @Override
//    public DropChances getOne(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<DropChances> query = session.createQuery("FROM DropChances WHERE dropId = :id");
//            query.setParameter("id", id);
//            List<DropChances> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list == null ? null : list.get(0);
//        }
//    }
//
//    @Override
//    public boolean ifExists(int dropChancesId) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<DropChances> query = session.createQuery("FROM DropChances WHERE dropId = :dropChancesId");
//            query.setParameter("dropChancesId", dropChancesId);
//            List<DropChances> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list.size() == 1;
//        }
//    }
//
//    @Override
//    public List<DropChances> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<DropChances> query = session.createQuery("FROM DropChances");
//            List<DropChances> list = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return list;
//        }
//    }
//
//    @Override
//    public DropChances update(DropChances dropChances) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(dropChances);
//            session.getTransaction().commit();
//            session.close();
//            return dropChances;
//        }
//    }
//
//    @Override
//    public DropChances delete(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            DropChances dropChances = getOne(id);
//            session.beginTransaction();
//            session.update(dropChances);
//            session.getTransaction().commit();
//            session.close();
//            return dropChances;
//        }
//    }
//}
