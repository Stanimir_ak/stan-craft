//package com.stan.craft.Repositories;
//
//import com.stan.craft.Models.User;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Transactional
//@Repository
//public class UserRepositoryImpl implements UserRepository {
//
//    private SessionFactory sessionFactory;
//
//    @Autowired
//    public UserRepositoryImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    @Override
//    public User getByName(String username) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<User> query = session.createQuery("FROM User WHERE username = :username");
//            query.setParameter("username", username);
//            List<User> users = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return users == null ? null : users.get(0);
//        }
//    }
//
//    @Override
//    public User getById(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<User> query = session.createQuery("FROM User WHERE id = :id");
//            query.setParameter("id", id);
//            List<User> users = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return users == null ? null : users.get(0);
//        }
//    }
//
//    @Override
//    public User update(User user) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(user);
//            session.getTransaction().commit();
//            session.close();
//            return user;
//        }
//    }
//
//    @Override
//    public boolean ifExists(String username) {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<User> query = session.createQuery("FROM User WHERE username = :username");
//            query.setParameter("username", username);
//            boolean ifExists = query.list().size() == 1;
//            session.getTransaction().commit();
//            session.close();
//            return ifExists;
//        }
//    }
//
//    @Override
//    public List<User> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            Query<User> query = session.createQuery("FROM User");
//            List<User> users = query.list();
//            session.getTransaction().commit();
//            session.close();
//            return users;
//        }
//    }
//}
