package com.stan.craft.Repositories;

import com.stan.craft.Models.Skill;
import com.stan.craft.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    @Query(value = " SELECT u from User u WHERE u.username = :username ")
    User getByUsername(@Param("username") String username);

    Optional<User> findByUsername(@Param("name") String name);
}
