package com.stan.craft.Utils;

import com.stan.craft.Models.Character;
import com.stan.craft.Models.ItemClass;
import com.stan.craft.Models.ItemPrivate;
import com.stan.craft.Models.Skill;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Utils {

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    //Luckily, you don’t have to do this—it’s been
    //done for you. It’s called Random.nextInt(int). You needn’t concern yourself
    //with the details of how it does its job (although you can study the documentation
    //or the source code if you’re curious). A senior engineer with a background in
    //algorithms spent a good deal of time designing, implementing, and testing this
    //method and then showed it to several experts in the field to make sure it was right.
    //Then the library was beta tested, released, and used extensively by millions of
    //programmers for almost two decades. No flaws have yet been found in the
    //method, but if a flaw were to be discovered, it would be fixed in the next release.
    //By using a standard library, you take advantage of the knowledge of the
    //experts who wrote it and the experience of those who used it before you.
    //As of Java 7, you should no longer use Random. For most uses, the random
    //number generator of choice is now ThreadLocalRandom. It produces higher
    //quality random numbers, and it’s very fast. On my machine, it is 3.6 times faster
    //than Random.

    public static int getRandomNumberUsingNextInt(int min, int max) {
        //depricated
        //Random random = new Random();
        try {
            return ThreadLocalRandom.current().nextInt(min, max + 1);
        } catch (Exception e) {
            System.out.println("Error on random number");
            return 1;
        }
    }

    public static double getRandomNumberUsingNextDouble(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max + 0.01);
    }

    public static boolean randomChance(double chancePerCent) {
        //get random number from 1 to 100. If the number is les than chance*100 --> success
        int randomNumber = getRandomNumberUsingNextInt(1, 100);
        double doubleChance = chancePerCent * 100;
        int intChance = (int) doubleChance;
        return randomNumber <= intChance;
    }

    public static List<List<ItemPrivate>> convertListToMatrix(List<ItemPrivate> list, int rowSize) {
        List<List<ItemPrivate>> matrix = new ArrayList<>();
        int index = 0;
        int row = 0;
        while (index < list.size()) {
            matrix.add(new ArrayList<>());
            for (int i = 0; i < rowSize && index < list.size(); i++) {
                matrix.get(row).add(list.get(index));
                index++;
            }
            row++;
        }
        return matrix;
    }

    public static List<List<Skill>> convertListSkillsToMatrix(List<Skill> list, int rowSize) {
        List<List<Skill>> matrix = new ArrayList<>();
        int index = 0;
        int row = 0;
        while (index < list.size()) {
            matrix.add(new ArrayList<>());
            for (int i = 0; i < rowSize && index < list.size(); i++) {
                matrix.get(row).add(list.get(index));
                index++;
            }
            row++;
        }
        return matrix;
    }

    public static boolean hasItemClassEquipped(Character character, ItemClass itemClass) {
        List<ItemPrivate> equipment = character.getEquipment();
        return equipment.stream().anyMatch(item -> (item.getItemClass().equals(itemClass)));
    }

    public static ItemPrivate getItemFromCollectionByClass(List<ItemPrivate> list, ItemClass itemClass) {
        return list.stream()
                .filter(item -> (item.getItemClass().equals(itemClass)))
                .collect(Collectors
                        .toList()).get(0);
    }
}
