package com.stan.craft.Controllers;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Skill;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/seeOtherCharacter")
public class SeeOtherCharacterController {
    private final CharacterService characterService;

    @Autowired
    public SeeOtherCharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @GetMapping("/{otherCharacter}")
    public String seeCharacter(@PathVariable(name = "otherCharacter") String otherCharacter,
                               Model model, Principal principal) {
        model.addAttribute("otherCharacter", null);
        try {
            characterService.checkIfExistsAndThrow(otherCharacter);
            Character character = characterService.getOneByName(otherCharacter);
            model.addAttribute("otherCharacter", character);

            List<Skill> skillsSorted = character.getSkills().stream()
                    .sorted(Comparator.comparing(Skill::getType)
                            .thenComparing(Skill::getSkillClass))
                    .collect(Collectors.toList());

            List<List<Skill>> characterSkillsMatrix = Utils.convertListSkillsToMatrix
                    (skillsSorted, Constants.CHARACTER_SKILLS_ROW_SIZE);

            model.addAttribute("characterSkillsMatrix", characterSkillsMatrix);
            return "seeChar";
        } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException | NullPointerException e) {
            model.addAttribute("error", e.getMessage());
            return "seeChar";
        }
    }
}
