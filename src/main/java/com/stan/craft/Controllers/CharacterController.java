package com.stan.craft.Controllers;

import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.DTO.CharacterDTO;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Models.*;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.ItemPrivateService;
import com.stan.craft.Services.ItemsService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/auth/characters")
public class CharacterController {

    private CharacterService characterService;
    private UserService userService;
    private ModelFactory modelFactory;
    private ItemsService itemsService;
    private ItemPrivateService itemPrivateService;

    @Autowired
    public CharacterController(CharacterService characterService, UserService userService, ModelFactory modelFactory, ItemsService itemsService, ItemPrivateService itemPrivateService) {
        this.characterService = characterService;
        this.userService = userService;
        this.modelFactory = modelFactory;
        this.itemsService = itemsService;
        this.itemPrivateService = itemPrivateService;
    }

    @GetMapping("/newCharacter")
    public String newCharacterData(Model model) {
        CharacterDTO characterDTO = new CharacterDTO();
        model.addAttribute("CharacterDTO", characterDTO);
        return "newChar";
    }

//    @GetMapping("/{id}/charImage")
//    public void renderCharImageFormDB(@PathVariable int id, HttpServletResponse response) throws IOException {
//        Character character = characterService.getOne(id);
//        if (character.getPicture() != null) {
//            response.setContentType("image/jpeg");
//            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(character.getPicture()));
//            IOUtils.copy(is, response.getOutputStream());
//        }
//    }

    @Transactional
    @PostMapping("/newCharacter")
    public String newCharacter(Model model, @ModelAttribute("CharacterDTO") CharacterDTO characterDTO, BindingResult errors,
                               Principal principal) {
        if (errors.hasErrors()) {
            model.addAttribute("error", "error");
            return "newChar";
        }
        if (!characterService.nameIsFree(characterDTO.getName())) {
            model.addAttribute("error", "Character name is not free");
        }

        try {
            Character character;
            User principalUser = userService.getPrincipalUser(principal);
            character = modelFactory.createCharacterFromDTO(characterDTO);
            character.setType(IndividualType.Character);
//            character.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            character.setUserCreator(principalUser.getUsername());
            ItemPrivate bag = itemsService.createItemPrivateFromBaseItem(itemsService.getOneByLevelAndClass(1, ItemClass.BAG));
            itemPrivateService.create(bag);
            character.getEquipment().add(bag);
            characterService.create(principal, character);

        } catch (DuplicateEntityException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "newChar";
    }

}
