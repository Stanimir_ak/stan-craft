package com.stan.craft.Controllers;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Role;
import com.stan.craft.Models.User;
import com.stan.craft.Services.LogService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegistrationController {
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;
    private LogService logService;
    private UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder,
                                  LogService logService, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.logService = logService;
        this.userService = userService;
    }

    @GetMapping("/sign-up")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "sign-up";
    }

    @PostMapping("/sign-up")
    public String registerUser(@ModelAttribute User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error",
                    "Username/password can't be empty, or less than 2 symbols!");
            return "sign-up";
        }

        if (userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("error", "User with the same username already exists");
            return "sign-up";
        }

        if (!user.getUsername().matches(Constants.REGEX_INPUT_PATTERN) || !user.getPassword().matches(Constants.REGEX_INPUT_PATTERN)) {
            model.addAttribute("error", "Name and pass must be ENG letters, between 3 and 20 symbols");
            return "sign-up";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Passwords don't match");
            return "sign-up";
        }

//        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        User newUser = new User(user.getUsername(), passwordEncoder.encode(user.getPassword()));
        newUser.addAuthority(new Role("ROLE_USER"));
        newUser.setEnabled(1);
        userDetailsManager.createUser(newUser);
        userService.findAll();
//        logService.notifyAllUsers("user: " + newUser.getUsername() + " joined the game!");

//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.update(newUser);
//            session.getTransaction().commit();
//        }
        return "sign-up-confirmation";
    }

    @GetMapping("/sign-up-confirmation")
    public String showRegisterConfirmation() {
        return "sign-up-confirmation";
    }
}
