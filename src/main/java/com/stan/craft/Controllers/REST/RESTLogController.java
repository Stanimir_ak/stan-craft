package com.stan.craft.Controllers.REST;

import com.stan.craft.Services.FightResultBeanLightService;
import com.stan.craft.Services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("deleteLog")
public class RESTLogController {

    private final LogService logService;
    private final FightResultBeanLightService fightResultBeanLightService;

    @Autowired
    public RESTLogController(LogService logService, FightResultBeanLightService fightResultBeanLightService) {
        this.logService = logService;
        this.fightResultBeanLightService = fightResultBeanLightService;
    }

    @PostMapping("")
    public void deleteOldLog() {
        logService.deleteOldLog();
    }

    @PostMapping("/fightLog")
    public void deleteOldFightLog() {
        fightResultBeanLightService.deleteOldFightLog();
    }
}
