package com.stan.craft.Controllers.REST;

import com.stan.craft.Models.DropChances;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Services.DropChancesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/rest/dropChances")
public class RESTDropChancesController {

    private DropChancesService dropChancesService;

    @Autowired
    public RESTDropChancesController(DropChancesService dropChancesService) {
        this.dropChancesService = dropChancesService;
    }

    @GetMapping("/getOne")
    DropChances getOne(int id) {
        return ModelFactory.createDropChancesFromId(id);
    }

    @GetMapping("/all")
    List<DropChances> getAll() {
        return dropChancesService.getAll();
    }

    @PostMapping("/create")
    DropChances create(Principal principal, DropChances dropChances) {
        return dropChancesService.create(principal, dropChances);
    }

    @PostMapping("/update")
    DropChances update(Principal principal, DropChances dropChances) {
        return dropChancesService.update(principal, dropChances);
    }

    @PostMapping("/delete")
    void delete(Principal principal, int id) {
        dropChancesService.delete(principal, id);
    }
}
