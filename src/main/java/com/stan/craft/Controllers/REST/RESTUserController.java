package com.stan.craft.Controllers.REST;

import com.stan.craft.Models.Character;
import com.stan.craft.Models.User;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/rest/users")
public class RESTUserController {

    private UserService userService;
    private CharacterService characterService;

    @Autowired
    public RESTUserController(UserService userService, CharacterService characterService) {
        this.userService = userService;
        this.characterService = characterService;
    }

    @PostMapping("/changeCurrentCharacter")
    public User changeCurrentCharacter(@RequestParam String newCharName, Principal principal) {
        User user = userService.getByName(principal.getName());
        Character character = characterService.getOneByName(newCharName);
        return userService.changeCurrentCharacter(user, character);
    }
}
