package com.stan.craft.Controllers.REST;

import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.DTO.CharacterDTO;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Models.*;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.ItemPrivateService;
import com.stan.craft.Services.ItemsService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/rest/characters")
public class RESTCharacterController {

    private final ModelFactory modelFactory;
    private final CharacterService characterService;
    private final UserService userService;
    private final ItemsService itemsService;
    private final ItemPrivateService itemPrivateService;

    @Autowired
    public RESTCharacterController(ModelFactory modelFactory, CharacterService characterService, UserService userService, ItemsService itemsService, ItemPrivateService itemPrivateService) {
        this.modelFactory = modelFactory;
        this.characterService = characterService;
        this.userService = userService;
        this.itemsService = itemsService;
        this.itemPrivateService = itemPrivateService;
    }


    @GetMapping("/all")
    List<Character> getAll() {
        return characterService.getAll();
    }

    @GetMapping("/one")
    Character getOne(@RequestParam int id) {
        try {
            return characterService.getOne(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @GetMapping("/{id}/charImage")
//    public void renderPostImageFormDB(@PathVariable int id, HttpServletResponse response) throws IOException {
//        Character character = characterService.getOne(id);
//        if (character.getPicture() != null) {
//            response.setContentType("image/jpeg");
//            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(character.getPicture()));
//            IOUtils.copy(is, response.getOutputStream());
//        }
//    }

    @PostMapping("/create")
    Character create(@RequestBody CharacterDTO characterDTO, Principal principal) {
        if (!characterService.nameIsFree(characterDTO.getName())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Character name is not free");
        }

        try {
            Character character;
            User principalUser = userService.getPrincipalUser(principal);
            character = modelFactory.createCharacterFromDTO(characterDTO);
            character.setType(IndividualType.Character);
//            character.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            character.setUserCreator(principalUser.getUsername());
            ItemPrivate bag = itemsService.createItemPrivateFromBaseItem(itemsService.getOneByLevelAndClass(1, ItemClass.BAG));
            itemPrivateService.create(bag);
            character.getEquipment().add(bag);
            characterService.create(principal, character);

            return character;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/update")
    Character update(@RequestBody int id, CharacterDTO characterDTO, Principal principal) {
        try {
            Character character = characterService.getOne(id);
            character.setCharacterClass(characterDTO.getCharacterClass());
            character.setName(characterDTO.getName());
            return characterService.userUpdatesDB(character, principal);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/delete")
    public void delete(@RequestBody int id, Principal principal) {
        try {
            characterService.delete(principal, id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

//    @PostMapping("/fight")
//    FightResultBean fight(@RequestParam int id1, int id2) {
//        Character char1 = characterService.getOne(id1);
//        Character char2 = characterService.getOne(id2);
//        return characterService.fight(char1, char2);
//    }

//    @PostMapping("/executeNRandomFights")
//    public List<String> executeNRandomFights(@RequestParam int n) {
//        return characterService.executeNRandomFights(n);
//    }

    @PostMapping("/giveItemToOtherCharacter")
    public void giveItemToOtherCharacter(@RequestParam int idGiver, String charNameTaker, int itemId) {
        try {
            characterService.giveItemToOtherCharacter(idGiver, charNameTaker, itemId);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/useItemFromOwnInventory")
    public void useItemFromOwnInventory(Principal principal, int itemId) {
        try {
            User userPrincipal = userService.getPrincipalUser(principal);
            Character character = userPrincipal.getCurrentCharacter();
            character = characterService.useItemFromOwnInventory(character, character .getId(), itemId);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
