//package com.stan.craft.Controllers.REST;
//
//import com.stan.craft.Models.ItemClass;
//import com.stan.craft.Services.ItemClassService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/rest/ItemClass")
//public class RESTItemClassController {
//
//    private final ItemClassService itemClassService;
//
//    @Autowired
//    public RESTItemClassController(ItemClassService itemClassService) {
//        this.itemClassService = itemClassService;
//    }
//
//    @GetMapping("/all")
//    public List<ItemClass> getAll() {
//        return itemClassService.getAll();
//    }
//
//    @GetMapping("/one")
//    public ItemClass getOne(@RequestParam int id) {
//        return itemClassService.getOne(id);
//    }
//
//    @PostMapping("/create")
//    public ItemClass create(@RequestParam String itemClass) {
//        return itemClassService.create(itemClass);
//    }
//
//    @PostMapping("/update")
//    public ItemClass update(@RequestParam int id, String itemClass) {
//        return itemClassService.update(id, itemClass);
//    }
//
//    @PostMapping("/delete")
//    public ItemClass delete(@RequestParam int id) {
//        return itemClassService.delete(id);
//    }
//
//
//}
