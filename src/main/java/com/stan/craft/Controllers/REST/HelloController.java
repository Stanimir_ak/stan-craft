package com.stan.craft.Controllers.REST;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/sayHello")
public class HelloController {

    @GetMapping("")
    public String sayHallo() {
        RestTemplate restTemplate = new RestTemplate();
        String tartarusResourceUrl = "https://stanutils.herokuapp.com/sayHello";
        ResponseEntity<String> response = restTemplate.getForEntity(tartarusResourceUrl, String.class);
        return "Hello from Tartarus";
    }
}
