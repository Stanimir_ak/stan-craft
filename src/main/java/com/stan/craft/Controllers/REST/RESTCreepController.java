package com.stan.craft.Controllers.REST;

import com.stan.craft.Constants.StaticVar;
import com.stan.craft.Models.Creep;
import com.stan.craft.Models.DTO.CreepDTO;
import com.stan.craft.Models.DropChances;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Services.CreepService;
import com.stan.craft.Services.DropChancesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/rest/creeps")
public class RESTCreepController {

    private final CreepService creepService;
    private ModelFactory modelFactory;
    private DropChancesService dropChancesService;

    @Autowired
    public RESTCreepController(CreepService creepService, ModelFactory modelFactory, DropChancesService dropChancesService) {
        this.creepService = creepService;
        this.modelFactory = modelFactory;
        this.dropChancesService = dropChancesService;
    }

    @GetMapping("/getOne")
    public Creep getOne(@RequestParam int id) {
        return creepService.getOne(id);
    }

    @GetMapping("/getAll")
    public List<Creep> getAll() {
        return StaticVar.AllCreeps;
    }

    @GetMapping("/getGamesNumberOfCreeps")
    public int getGamesNumberOfCreeps() {
        return creepService.getGamesNumberOfCreeps();
    }

    @GetMapping("/getAllNormalCreeps")
    public List<Creep> getAllNormalCreeps() {
        return creepService.getAllNormalCreeps();
    }

    @GetMapping("/getAllBosses")
    public List<Creep> getAllBosses() {
        return creepService.getAllBosses();
    }

    @PostMapping("/create")
    public Creep create(@RequestBody CreepDTO creepDTO, Principal principal) {
        Creep creep = modelFactory.createCreepFromDTO(creepDTO);
        DropChances dropChances = ModelFactory.createDropChancesFromId(creepDTO.getDropChancesId());
        creep.setDropChances(dropChances);
        return creepService.create(creep, principal);
    }

    @PostMapping("/update")
    public Creep update(@RequestBody Creep creep, Principal principal) {
        return creepService.update(creep, principal);
    }

    @PostMapping("/delete")
    public void delete(@RequestParam int id, Principal principal) {
        creepService.delete(id, principal);
    }
}





















