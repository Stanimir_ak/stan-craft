package com.stan.craft.Controllers.REST;

import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Exceptions.InvalidOperationException;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Item;
import com.stan.craft.Models.ItemPrivate;
import com.stan.craft.Models.User;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.ItemsService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/rest/items")
public class RESTItemsController {

    private final ItemsService itemsService;
    private CharacterService characterService;
    private UserService userService;

    @Autowired
    public RESTItemsController(ItemsService itemsService, CharacterService characterService, UserService userService) {
        this.itemsService = itemsService;
        this.characterService = characterService;
        this.userService = userService;
    }

    @GetMapping("/all")
    public List<Item> getAll() {
        return itemsService.getAll();
    }

    @GetMapping("/one")
    public Item getOne(@RequestParam int id) {
        return itemsService.getOne(id);
    }

    @PostMapping("/create")
    public Item create(@RequestBody Item item, Principal principal) {
        try {
            return itemsService.create(item, principal);
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/update")
    public Item update(@RequestBody Item item, Principal principal) {
        try {
            return itemsService.update(item, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/delete")
    public void delete(@RequestParam int id, Principal principal) {
        try {
            itemsService.delete(id, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/useItemFromOwnInventory")
    User useItemFromOwnInventory(Principal principal, @RequestParam int string1) {
        User userPrincipal = userService.getPrincipalUser(principal);
        Character character = userPrincipal.getCurrentCharacter();
        characterService.useItemFromOwnInventory(character,character.getId(), string1);
        return userService.getPrincipalUser(principal);
    }

    @GetMapping("getInventory")
    List<ItemPrivate> getInventory(int id) {
        return characterService.getOne(id).getInventory();
    }

    @GetMapping("getEquipment")
    List<ItemPrivate> getEquipment(int id) {
        return characterService.getOne(id).getEquipment();
    }


}
