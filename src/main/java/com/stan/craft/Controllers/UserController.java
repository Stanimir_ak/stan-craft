package com.stan.craft.Controllers;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Exceptions.InvalidOperationException;
import com.stan.craft.Models.Beans.StringBean;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.DTO.CharacterDTO;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Models.*;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.ItemPrivateService;
import com.stan.craft.Services.ItemsService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/auth/user")
public class UserController {

    private UserService userService;
    private CharacterService characterService;
    private ModelFactory modelFactory;
    private ItemsService itemsService;
    private ItemPrivateService itemPrivateService;

    @Autowired
    public UserController(UserService userService, CharacterService characterService, ModelFactory modelFactory, ItemsService itemsService, ItemPrivateService itemPrivateService) {
        this.userService = userService;
        this.characterService = characterService;
        this.modelFactory = modelFactory;
        this.itemsService = itemsService;
        this.itemPrivateService = itemPrivateService;
    }

    @ModelAttribute("user")
    public User getUser(Principal principal) {
        return userService.getPrincipalUser(principal);
    }

    @ModelAttribute("userCharacters")
    public List<Character> userCharacters(Principal principal) {
        return userService.getPrincipalUser(principal).getCharacters().stream()
                .sorted(Comparator.comparing(Character::getLevel).reversed())
                .collect(Collectors.toList());
    }

    @ModelAttribute("CharacterDTO")
    public CharacterDTO newCharacterData() {
        CharacterDTO characterDTO = new CharacterDTO();
        return characterDTO;
    }

    @ModelAttribute("StringBean")
    public StringBean getStringBean(Principal principal) {
        return new StringBean();
    }

    @GetMapping("")
    public String loadPage() {
        return "user";
    }


    @Transactional
    @PostMapping("")
    public String newCharacter(@ModelAttribute("CharacterDTO") CharacterDTO characterDTO, Model model,
                               @ModelAttribute("StringBean") StringBean stringBean, BindingResult errors,
                               Principal principal) {
        if (stringBean.getMethod().equals("createChar")) {
            if (errors.hasErrors()) {
                return "user";
            }

            try {
                if (!characterService.nameIsFree(characterDTO.getName())) {
                    model.addAttribute("error", "Character name is not free");
                    return "user";
                }
                if (!characterDTO.getName().matches(Constants.REGEX_INPUT_PATTERN)) {
                    model.addAttribute("error", "Name must be ENG letters, between 3 and 20 symbols");
                    return "user";
                }
                Character character;
                User principalUser = userService.getPrincipalUser(principal);
                character = ModelFactory.createCharacterFromDTO(characterDTO);
                character.setType(IndividualType.Character);
//                InputStream in = getClass().getResourceAsStream("src/main/resources/static/Knight.jpg");
//                if (file != null && !file.isEmpty()) {
//                    character.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
//                }
                character.setUserCreator(principalUser.getUsername());
                ItemPrivate bag = itemsService.createItemPrivateFromBaseItem(itemsService.getOneByLevelAndClass(1, ItemClass.BAG));
                itemPrivateService.create(bag);
                character.getEquipment().add(bag);
                //TODO check if bag is here
                characterService.create(principal, character);

            } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException e) {
                model.addAttribute("error", e.getMessage());
                return "user";
            }
            return "redirect:/auth/user";
        }
        if (stringBean.getMethod().equals("changeChar")) {
            try {
                User user = userService.getByName(principal.getName());
                Character character = characterService.getOneByName(stringBean.getString1());
                userService.changeCurrentCharacter(user, character);
                return "redirect:/auth/user";
            } catch (EntityNotFoundException | DuplicateEntityException | InvalidOperationException e) {
                model.addAttribute("error", e.getMessage());
                return "user";
            }
        }
        return "user";
    }

}
