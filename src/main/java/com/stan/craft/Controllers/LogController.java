package com.stan.craft.Controllers;

import com.stan.craft.Models.Beans.LogBean;
import com.stan.craft.Models.Page;
import com.stan.craft.Models.User;
import com.stan.craft.Services.LogService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/auth/log")
public class LogController {

    private final LogService logService;
    private final UserService userService;

    @Autowired
    public LogController(LogService logService, UserService userService) {
        this.logService = logService;
        this.userService = userService;
    }

    @ModelAttribute("page")
    public Page populatePage() {
        Page page = new Page();
        page.setIndex(1);
        return page;
    }

    @ModelAttribute("user")
    public User populateUser(Principal principal) {
        return userService.getPrincipalUser(principal);
    }

    @ModelAttribute("logBean")
    public LogBean populateLogBean() {
        return new LogBean();
    }

    @GetMapping("/{page}")
    public String getLog(Model model, Principal principal, @ModelAttribute(name = "page") Page page) {
        Slice<LogBean> log = logService.getLogByUsername(principal, page);
        model.addAttribute("log", log);
        boolean hasPrevious = page.getIndex() != 1;
        boolean hasNext = log.hasNext();

        model.addAttribute("hasNext", hasNext);
        model.addAttribute("hasPrevious", hasPrevious);
        return "log";
    }

    @PostMapping("/{page}")
    public String chat(Model model, Principal principal,
                       @ModelAttribute(name = "page") Page page,
                       @ModelAttribute(name = "logBean") LogBean logBean) {
        try {
            logService.chat(principal.getName(), logBean.getUsername(), logBean.getMessage());
        } catch (Exception e) {
            //do something :)
        }
        Slice<LogBean> log = logService.getLogByUsername(principal, page);
        model.addAttribute("log", log);
        boolean hasPrevious = page.getIndex() != 1;
        boolean hasNext = log.hasNext();

        model.addAttribute("hasNext", hasNext);
        model.addAttribute("hasPrevious", hasPrevious);
        return "log";
    }
}
