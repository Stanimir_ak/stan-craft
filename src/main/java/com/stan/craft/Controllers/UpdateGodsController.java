package com.stan.craft.Controllers;

import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Beans.StringBean;
import com.stan.craft.Services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/admin/updateGods")
public class UpdateGodsController {
    private final AdminService adminService;

    @Autowired
    public UpdateGodsController(AdminService adminService) {
        this.adminService = adminService;
    }

    @ModelAttribute("StringBean")
    public StringBean populateStringBean() {
        return new StringBean();
    }

    @GetMapping("")
    public String updateGods(Model model) {
        try {
            return "updateGods";
        } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "updateGods";
        }
    }

    @PostMapping("")
    public String postMethods(Principal principal, Model model,
                              @ModelAttribute(name = "StringBean") StringBean stringBean
    ) {
        try {
            if ("addSkillsToGods".equalsIgnoreCase(stringBean.getMethod())) {
                adminService.addSkillsToGods(principal);
                return "redirect:/admin/updateGods";
            }

        } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "updateGods";
        }
        return "redirect:/admin/updateGods";
    }
}

