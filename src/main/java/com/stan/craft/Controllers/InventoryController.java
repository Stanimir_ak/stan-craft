package com.stan.craft.Controllers;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Beans.StringBean;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.ItemPrivate;
import com.stan.craft.Models.User;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.UserService;
import com.stan.craft.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/auth/inventory")
public class InventoryController {

    private UserService userService;
    private CharacterService characterService;

    @Autowired
    public InventoryController(UserService userService, CharacterService characterService) {
        this.userService = userService;
        this.characterService = characterService;
    }

    @ModelAttribute("user")
    public User getUser(Principal principal) {
        return userService.getPrincipalUser(principal);
    }

    @ModelAttribute("StringBean")
    public StringBean populateStringBean() {
        return new StringBean();
    }

    @ModelAttribute("isBestEquipped")
    public boolean isBestEquipped(Principal principal, Model model) {
        if (userService.getPrincipalUser(principal).getCurrentCharacter() == null) {
            return false;
        }
        return characterService.isBestEquipped(principal);
    }

    @ModelAttribute("inventorySorted")
    public List<ItemPrivate> inventorySorted(Principal principal, Model model) {
        User userPrincipal = userService.getPrincipalUser(principal);
        if (userPrincipal.getCurrentCharacter() == null) {
            return null;
        }
        return userPrincipal.getCurrentCharacter().getInventory().stream()
                .sorted(Comparator.comparing(ItemPrivate::getLevel)
                        .thenComparing(ItemPrivate::getItemClass)
                        .thenComparing(ItemPrivate::getName)
                        .thenComparing(ItemPrivate::getRise).reversed())
                .collect(Collectors.toList());
    }

    @ModelAttribute("inventoryMatrix")
    public List<List<ItemPrivate>> inventory(Principal principal, Model model) {
        User userPrincipal = userService.getPrincipalUser(principal);
        if (userPrincipal.getCurrentCharacter() == null) {
            return null;
        }
        List<ItemPrivate> itemsSorted = userPrincipal.getCurrentCharacter().getInventory().stream()
                .sorted(Comparator.comparing(ItemPrivate::getLevel)
                        .thenComparing(ItemPrivate::getItemClass)
                        .thenComparing(ItemPrivate::getName)
                        .thenComparing(ItemPrivate::getRise).reversed())
                .collect(Collectors.toList());
        try {
            return Utils.convertListToMatrix(itemsSorted, Constants.INVENTORY_ITEM_ROW_SIZE);
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return Utils.convertListToMatrix(itemsSorted, Constants.INVENTORY_ITEM_ROW_SIZE);
        }
    }

    @ModelAttribute("bagMatrix")
    public List<List<ItemPrivate>> bag(Principal principal, Model model) {
        User userPrincipal = userService.getPrincipalUser(principal);
        if (userPrincipal.getCurrentCharacter() == null) {
            return null;
        }
        List<ItemPrivate> itemsSorted = userPrincipal.getCurrentCharacter().getBag().stream()
                .sorted(Comparator.comparing(ItemPrivate::getItemClass)
                        .thenComparing(ItemPrivate::getName))
                .collect(Collectors.toList());
        try {
            return Utils.convertListToMatrix(itemsSorted, Constants.BAG_ITEM_ROW_SIZE);
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return Utils.convertListToMatrix(itemsSorted, Constants.BAG_ITEM_ROW_SIZE);
        }
    }

    @ModelAttribute("bagItems")
    public List<ItemPrivate> bagItems(Principal principal, Model model) {
        User userPrincipal = userService.getPrincipalUser(principal);
        if (userPrincipal.getCurrentCharacter() == null) {
            return null;
        }
        List<ItemPrivate> itemsSorted = userPrincipal.getCurrentCharacter().getBag().stream()
                .sorted(Comparator.comparing(ItemPrivate::getItemClass)
                        .thenComparing(ItemPrivate::getName))
                .collect(Collectors.toList());
        try {
            return itemsSorted;
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return itemsSorted;
        }
    }

    @GetMapping("")
    public String showInventory(Model model, Principal principal) {
        try {
            model.addAttribute("user", userService.getPrincipalUser(principal));
            return "inventory2";
        } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException | NullPointerException e) {
            model.addAttribute("error", e.getMessage());
            return "inventory2";
        }
    }

    @PostMapping("")
    public String postMethods(Principal principal, Model model,
                              @ModelAttribute(name = "StringBean") StringBean stringBean,
                              @ModelAttribute(name = "user") User userPrincipal) {
        try {
//            User userPrincipal = userService.getPrincipalUser(principal);
            Character character = userPrincipal.getCurrentCharacter();
            if ("giveItemToOtherCharacter".equalsIgnoreCase(stringBean.getMethod()) ||
                    "giveItemFromBagToOtherCharacter".equalsIgnoreCase(stringBean.getMethod())) {
                int idGiver = userPrincipal.getCurrentCharacter().getId();
                String characterTakerName = stringBean.getString1();
                int itemId = Integer.parseInt(stringBean.getString2());
                characterService.giveItemToOtherCharacter(idGiver, characterTakerName, itemId);
            }

            if ("useItemFromOwnInventory".equalsIgnoreCase(stringBean.getMethod())) {
                int id = userPrincipal.getCurrentCharacter().getId();
                int itemId = Integer.parseInt(stringBean.getString1());
                character = characterService.useItemFromOwnInventory(character, id, itemId);
            }

            if ("useItemFromOwnBag".equalsIgnoreCase(stringBean.getMethod())) {
                int id = userPrincipal.getCurrentCharacter().getId();
                int itemId = Integer.parseInt(stringBean.getString1());
                characterService.useItemFromOwnBag(character, id, itemId);
            }

            if ("transformRing".equalsIgnoreCase(stringBean.getMethod())) {
                int ringId = Integer.parseInt(stringBean.getString1());
                int crystalId = Integer.parseInt(stringBean.getString2());
                characterService.transformRing(userPrincipal.getCurrentCharacter(), ringId, crystalId);
            }
        } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException | NullPointerException e) {
            model.addAttribute("error", e.getMessage());
            return "inventory2";
        }

        return "redirect:/auth/inventory";
    }

}
