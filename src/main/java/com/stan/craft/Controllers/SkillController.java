package com.stan.craft.Controllers;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.*;
import com.stan.craft.Models.Beans.StringBean;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.SkillsService;
import com.stan.craft.Services.UserService;
import com.stan.craft.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/auth/skills")
public class SkillController {
    private final UserService userService;
    private final CharacterService characterService;
    private final ModelFactory modelFactory;
    private final SkillsService skillsService;

    @Autowired
    public SkillController(UserService userService, CharacterService characterService, ModelFactory modelFactory, SkillsService skillsService) {
        this.userService = userService;
        this.characterService = characterService;
        this.modelFactory = modelFactory;
        this.skillsService = skillsService;
    }

    @ModelAttribute("user")
    public User getUser(Principal principal) {
        return userService.getPrincipalUser(principal);
    }

    @ModelAttribute("StringBean")
    public StringBean populateStringBean() {
        return new StringBean();
    }

    @ModelAttribute("availableSkillsMatrix")
    public List<List<Skill>> availableSkills(Principal principal, Model model) {
        User userPrincipal = userService.getPrincipalUser(principal);
        if (userPrincipal.getCurrentCharacter() == null) {
            return null;
        }
        CharacterClass charClass = userPrincipal.getCurrentCharacter().getCharacterClass();
        List<Skill> skillsSorted = modelFactory.createListAvailableSkills(skillsService.availableSkillsForCharClass(charClass)).stream()
                .sorted(Comparator.comparing(Skill::getType)
                        .thenComparing(Skill::getSkillClass))
                .collect(Collectors.toList());
        try {
            return Utils.convertListSkillsToMatrix(skillsSorted, Constants.AVAILABLE_SKILLS_ROW_SIZE);
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return Utils.convertListSkillsToMatrix(skillsSorted, Constants.AVAILABLE_SKILLS_ROW_SIZE);
        }
    }

    @ModelAttribute("characterSkillsMatrix")
    public List<List<Skill>> characterSkills(Principal principal, Model model) {
        User userPrincipal = userService.getPrincipalUser(principal);
        if (userPrincipal.getCurrentCharacter() == null) {
            return null;
        }
        List<Skill> skillsSorted = userPrincipal.getCurrentCharacter().getSkills().stream()
                .sorted(Comparator.comparing(Skill::getType)
                        .thenComparing(Skill::getSkillClass))
                .collect(Collectors.toList());
        try {
            return Utils.convertListSkillsToMatrix(skillsSorted, Constants.CHARACTER_SKILLS_ROW_SIZE);
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return Utils.convertListSkillsToMatrix(skillsSorted, Constants.CHARACTER_SKILLS_ROW_SIZE);
        }
    }

    @GetMapping("")
    public String showSkills(Model model, @ModelAttribute(name = "user") User userPrincipal) {
        if (userPrincipal.getCurrentCharacter() == null) {
            return "skills";
        }
        try {
            StringBean stringBean = new StringBean();
            List<Skill> skills = userPrincipal.getCurrentCharacter().getSkills();
            int totalSkillPointsUsed = 0;
            for (Skill skill : skills) {
                totalSkillPointsUsed += skill.getLevel();
            }
            stringBean.setString1(String.valueOf(totalSkillPointsUsed));
            stringBean.setString2(String.valueOf(userPrincipal.getCurrentCharacter().getLevel() - totalSkillPointsUsed));
            model.addAttribute("totalSkillsBean", stringBean);
            return "skills";
        } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "skills";
        }
    }

    @PostMapping("")
    public String postMethods(Principal principal, Model model,
                              @ModelAttribute(name = "StringBean") StringBean stringBean,
                              @ModelAttribute(name = "user") User userPrincipal) {
        //populating
        StringBean stringBean2 = new StringBean();
        int totalSkillPointsUsed = characterService.calculateTotalSkillPointsUsed(userPrincipal.getCurrentCharacter());
        stringBean2.setString1(String.valueOf(totalSkillPointsUsed));
        stringBean2.setString2(String.valueOf(userPrincipal.getCurrentCharacter().getLevel() - totalSkillPointsUsed));
        model.addAttribute("totalSkillsBean", stringBean2);

        try {
            if ("addSkill".equalsIgnoreCase(stringBean.getMethod())) {
                String skillClass = stringBean.getString1();
                characterService.addSkillAndUpdateDB(userPrincipal.getCurrentCharacter(), SkillClass.valueOf(skillClass));
                return "redirect:/auth/skills";
            }
            if ("removeSkill".equalsIgnoreCase(stringBean.getMethod())) {
                String skillClass = stringBean.getString1();
                characterService.removeSkillAndUpdateDB(userPrincipal.getCurrentCharacter(), SkillClass.valueOf(skillClass));
                return "redirect:/auth/skills";
            }
            if ("updateSkill".equalsIgnoreCase(stringBean.getMethod())) {
                String skillClass = stringBean.getString1();
                int level = Integer.parseInt(stringBean.getString2());
                SkillClass skillClass1 = SkillClass.valueOf(skillClass);
//                Skill skill = modelFactory.skillsFactoryMethod(skillClass1);
                characterService.skillLevelUpdateAndUpdateDB(userPrincipal.getCurrentCharacter(), skillClass1, level);
                return "redirect:/auth/skills";
            }
        } catch (EntityNotFoundException | DuplicateEntityException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "skills";
        }
        return "redirect:/auth/skills";
    }
}
