package com.stan.craft.Controllers;

import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Exceptions.InvalidOperationException;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Creep;
import com.stan.craft.Models.DTO.CharacterDTO;
import com.stan.craft.Models.DTO.CreepDTO;
import com.stan.craft.Models.Item;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.CreepService;
import com.stan.craft.Services.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.Base64;

@Controller
@RequestMapping("/pictures")
public class PictureController {

    private final CharacterService characterService;
    private final ItemsService itemsService;
    private final CreepService creepService;

    @Autowired
    public PictureController(CharacterService characterService, ItemsService itemsService, CreepService creepService) {
        this.characterService = characterService;
        this.itemsService = itemsService;
        this.creepService = creepService;
    }

    @GetMapping("/admin/uploadPictures")
    public String newItemData(Model model) {
        Item item = new Item();
        CharacterDTO characterDTO = new CharacterDTO();
        CreepDTO creepDTO = new CreepDTO();
        model.addAttribute("Item", item);
        model.addAttribute("CharacterDTO", characterDTO);
        model.addAttribute("CreepDTO", creepDTO);
        return "pictures";
    }

    @Transactional
    @PostMapping("/admin/uploadPictures")
    public String newItem(Model model, @ModelAttribute("Item") Item item,
                          @ModelAttribute("CharacterDTO") CharacterDTO characterDTO,
                          @ModelAttribute("CreepDTO") CreepDTO creepDTO, BindingResult errors,
                          @RequestParam("imagefile1") @Nullable MultipartFile file1,
                          @RequestParam("imagefile2") @Nullable MultipartFile file2,
                          @RequestParam("imagefile3") @Nullable MultipartFile file3, Principal principal) throws IOException {
        if (errors.hasErrors()) {
            return "pictures";
        }
        if (file1 != null) {
            try {
                Item item1 = itemsService.getOne(item.getId());
                item1.setPicture(Base64.getEncoder().encodeToString(file1.getBytes()));
                itemsService.update(item1, principal);
                return "pictures";
            } catch (InvalidOperationException | DuplicateEntityException | EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "index";
            }
        }

        if (file2 != null) {
            try {
                Creep creep = creepService.getOne(creepDTO.getCreepId());
//                creep.setPicture(Base64.getEncoder().encodeToString(file2.getBytes()));
                creepService.update(creep, principal);
                return "pictures";
            } catch (InvalidOperationException | DuplicateEntityException | EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "index";
            }
        }

        if (file3 != null) {
            try {
                Character character = characterService.getOneByName(characterDTO.getName());
//                character.setPicture(Base64.getEncoder().encodeToString(file3.getBytes()));
                characterService.userUpdatesDB(character, principal);

//                //Fixing bug - GameRepository is not in the DB - we need to update the char there
//                if (gameRepository.getZoneArena().stream().anyMatch(c -> c.getId() == character.getId())) {
//                    for (int i = 0; i < gameRepository.getZoneArena().size(); i++) {
//                        if (gameRepository.getZoneArena().get(i).getId() == character.getId()) {
//                            gameRepository.getZoneArena().get(i).setPicture(Base64.getEncoder().encodeToString(file3.getBytes()));
//                        }
//                    }
//                } else if (gameRepository.getZoneBosses().stream().anyMatch(c -> c.getId() == character.getId())) {
//                    for (int i = 0; i < gameRepository.getZoneBosses().size(); i++) {
//                        if (gameRepository.getZoneBosses().get(i).getId() == character.getId()) {
//                            gameRepository.getZoneBosses().get(i).setPicture(Base64.getEncoder().encodeToString(file3.getBytes()));
//                        }
//                    }
//                } else if (gameRepository.getZoneCreeping().stream().anyMatch(c -> c.getId() == character.getId())) {
//                    for (int i = 0; i < gameRepository.getZoneCreeping().size(); i++) {
//                        if (gameRepository.getZoneCreeping().get(i).getId() == character.getId()) {
//                            gameRepository.getZoneCreeping().get(i).setPicture(Base64.getEncoder().encodeToString(file3.getBytes()));
//                        }
//                    }
//                }
//                User principalUser = userService.getPrincipalUser(principal);
//                if (principalUser.getCurrentCharacter().getid() == character.getid()) {
//                    principalUser.setCurrentCharacter(character);
//                }
//                userService.update(principalUser);
                return "pictures";
            } catch (InvalidOperationException | DuplicateEntityException | EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "index";
            }
        }
        return "pictures";
    }
}
