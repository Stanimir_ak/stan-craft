package com.stan.craft.Controllers;

import com.stan.craft.Models.Item;
import com.stan.craft.Services.ItemsService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/allItems")
public class AllItemsController {

    private ItemsService itemsService;
    private UserService userService;

    @Autowired
    public AllItemsController(ItemsService itemsService, UserService userService) {
        this.itemsService = itemsService;
        this.userService = userService;
    }

    @ModelAttribute("Items")
    public List<Item> characters() {
        return itemsService.getAll().stream()
                .sorted(Comparator.comparing(Item::getItemClass)
                        .thenComparing(Item::getLevel))
                .collect(Collectors.toList());
    }

    @GetMapping("")
    public String allItems(Model model, Principal principal) {
//        if (!userService.isAdmin(principal)) {
//            return "index";
//        }
        return "allItems";
    }

}
