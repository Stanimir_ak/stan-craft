package com.stan.craft.Controllers;

import com.stan.craft.Constants.TimeBean;
import com.stan.craft.Models.Beans.LogBean;
import com.stan.craft.Models.Beans.StringBean;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Page;
import com.stan.craft.Models.User;
import com.stan.craft.Models.ZoneClass;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.LogService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/auth/game")
public class GameController {

    private final CharacterService characterService;
    private final UserService userService;
    private final LogService logService;

    @Autowired
    public GameController(CharacterService characterService, UserService userService,
                          LogService logService) {
        this.characterService = characterService;
        this.userService = userService;
        this.logService = logService;
    }

    @ModelAttribute("characters")
    public List<Character> characters() {
        return characterService.getAll();
    }

    @ModelAttribute("TimeBean")
    public TimeBean getTimeBean() {
        return new TimeBean();
    }

    @ModelAttribute("Zones")
    public ZoneClass[] getZones() {
        return ZoneClass.values();
    }

    @ModelAttribute("StringBean")
    public StringBean populateStringBean() {
        return new StringBean();
    }

    @ModelAttribute("user")
    public User getUser(Principal principal) {
        return userService.getPrincipalUser(principal);
    }

    @ModelAttribute("ladder")
    public List<Character> getLadder() {
        return characterService.getAllSortedByExp();
    }

    @ModelAttribute("page")
    public Page populatePage() {
        Page page = new Page();
        page.setIndex(1);
        return page;
    }

    @ModelAttribute("logBean")
    public LogBean populateLogBean() {
        return new LogBean();
    }


    @GetMapping("/{page}")
    public String log(Model model, @ModelAttribute(name = "page") Page page, Principal principal) {
        Slice<LogBean> log = logService.getLogByUsername(principal, page);
        model.addAttribute("log", log);
        boolean hasPrevious = page.getIndex() > 1;
        boolean hasNext = log.hasNext();

        model.addAttribute("hasNext", hasNext);
        model.addAttribute("hasPrevious", hasPrevious);
        return "game2";
    }

    @GetMapping("/observe")
    public String observe(Model model) {
        return "game";
    }

    @PostMapping("/{page}")
    public String move(Principal principal, Model model,
                       @ModelAttribute(name = "user") User userPrincipal,
                       @ModelAttribute(name = "page") Page page,
                       @ModelAttribute(name = "logBean") LogBean logBean,
                       @ModelAttribute(name = "StringBean") StringBean stringBean) {

        //Zones
        try {
            if ("changeZone".equalsIgnoreCase(stringBean.getMethod())) {
                String zoneNew = stringBean.getString1();
                String characterDuel = stringBean.getString2();
                ZoneClass zone = ZoneClass.valueOf(zoneNew);
                characterService.changeZone(userPrincipal.getCurrentCharacter(), zone, characterDuel);
            }
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
        }

        //chat
        try {
            logService.chat(principal.getName(), logBean.getUsername(), logBean.getMessage());
        } catch (Exception e) {
            model.addAttribute("error", "Something went wrong");
        }

        //just populating
        Slice<LogBean> log = logService.getLogByUsername(principal, page);
        model.addAttribute("log", log);
        boolean hasPrevious = page.getIndex() > 1;
        boolean hasNext = log.hasNext();

        model.addAttribute("hasNext", hasNext);
        model.addAttribute("hasPrevious", hasPrevious);
//        model.addAttribute("matrix", gameRepositoryImpl.getMatrix());

        //populate user in order to refresh coordinates of the current character!
        model.addAttribute("user", userService.getPrincipalUser(principal));
        return "game2";
    }
}
