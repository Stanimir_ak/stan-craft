package com.stan.craft.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class LoginController {

    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

    @GetMapping("/auth/confirmation")
    public String authConfirmation() {
        return "auth-confirmation";
    }

    @GetMapping("/login/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }

}
