package com.stan.craft.Controllers;

import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Exceptions.InvalidOperationException;
import com.stan.craft.Models.Creep;
import com.stan.craft.Models.DTO.CreepDTO;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Services.CreepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/creeps")
public class CreepController {

    private final CreepService creepService;
    private final ModelFactory modelFactory;

    @Autowired
    public CreepController(CreepService creepService, ModelFactory modelFactory) {
        this.creepService = creepService;
        this.modelFactory = modelFactory;
    }

    @GetMapping("/admin/newCreep")
    public String newCreepData(Model model) {
        CreepDTO creep = new CreepDTO();
        model.addAttribute("Creep", creep);
        return "newCreep";
    }

//    @GetMapping("/{id}/creepImage")
//    public void renderCreepImageFormDB(@PathVariable int id, HttpServletResponse response) throws IOException {
//        Creep creep = creepService.getOne(id);
//        if (creep.getPicture() != null) {
//            response.setContentType("image/jpeg");
//            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(creep.getPicture()));
//            IOUtils.copy(is, response.getOutputStream());
//        }
//    }

    @Transactional
    @PostMapping("/admin/newCreep")
    public String newCreep(Model model, @ModelAttribute("Creep") CreepDTO creepDTO, BindingResult errors,
                           Principal principal) {
        if (errors.hasErrors()) {
            return "newCreep";
        }
        try {
//            creepDTO.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            Creep creep = ModelFactory.createCreepFromDTO(creepDTO);
            creepService.create(creep, principal);
            return "newCreep";
        } catch (InvalidOperationException | DuplicateEntityException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "index";
        }
    }
}
