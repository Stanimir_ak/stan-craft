package com.stan.craft.Controllers;

import com.stan.craft.Exceptions.DuplicateEntityException;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Exceptions.InvalidOperationException;
import com.stan.craft.Models.Item;
import com.stan.craft.Services.ItemsService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Base64;

@Controller
@RequestMapping("/items")
public class ItemController {

    private ItemsService itemsService;

    @Autowired
    public ItemController(ItemsService itemsService) {
        this.itemsService = itemsService;
    }


    @GetMapping("/admin/newItem")
    public String newItemData(Model model) {
        Item item = new Item();
        model.addAttribute("Item", item);
        return "newItem";
    }

    @GetMapping("/{id}/itemImage")
    public void renderItemImageFormDB(@PathVariable int id, HttpServletResponse response) throws IOException {
        Item item = itemsService.getOne(id);
        if (item.getPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(item.getPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    @Transactional
    @PostMapping("/admin/newItem")
    public String newItem(Model model, @ModelAttribute("Item") Item item, BindingResult errors,
                          @RequestParam("imagefile") MultipartFile file, Principal principal) throws IOException {
        if (errors.hasErrors()) {
            return "newItem";
        }
        try {
            item.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            itemsService.create(item, principal);
            return "newItem";
        } catch (InvalidOperationException | DuplicateEntityException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "index";
        }
    }
}
