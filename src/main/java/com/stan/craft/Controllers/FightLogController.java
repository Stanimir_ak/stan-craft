package com.stan.craft.Controllers;

import com.stan.craft.Models.Beans.FightResultBeanLight;
import com.stan.craft.Models.Beans.IdBean;
import com.stan.craft.Models.Page;
import com.stan.craft.Models.User;
import com.stan.craft.Services.FightResultBeanLightService;
import com.stan.craft.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/fightLog")
public class FightLogController {

    private final FightResultBeanLightService fightResultBeanLightService;
    private final UserService userService;

    @Autowired
    public FightLogController(FightResultBeanLightService fightResultBeanLightService, UserService userService) {
        this.fightResultBeanLightService = fightResultBeanLightService;
        this.userService = userService;
    }

    @ModelAttribute("page")
    public Page populatePage() {
        Page page = new Page();
        page.setIndex(1);
        return page;
    }

    @ModelAttribute("user")
    public User populateUser(Principal principal) {
        return userService.getPrincipalUser(principal);
    }

    @GetMapping("/{page}")
    public String getFightLog(Model model, Principal principal, @ModelAttribute(name = "page") Page page) {
        Slice<FightResultBeanLight> fightLog = fightResultBeanLightService.getFightLogByCharacter(principal, page);
        model.addAttribute("fightLog", fightLog);
        boolean hasPrevious = page.getIndex() != 1;
        boolean hasNext = fightLog.hasNext();

        model.addAttribute("hasNext", hasNext);
        model.addAttribute("hasPrevious", hasPrevious);
        return "fightLog";
    }

//    @ModelAttribute("idBean")
//    public IdBean populateIdBean() {
//        IdBean idBean = new IdBean();
//        return idBean;
//    }

    @GetMapping("/one")
    public String populateIdBean(Model model) {
        IdBean idBean = new IdBean();
        model.addAttribute("idBean", idBean);
        return "oneFightLog";
    }

    @PostMapping("/one")
    public String getOne(Model model, Principal principal, @ModelAttribute(name = "idBean") IdBean idBean) {
        List<String> fightLog = fightResultBeanLightService.getOneFightLog(idBean.getId());
        model.addAttribute("fightLog", fightLog);
        return "oneFightLog";
    }

    @GetMapping("/one/{id}")
    public String getOneById(Model model, @PathVariable long id) {
        List<String> fightLog = fightResultBeanLightService.getOneFightLog2(id);
        model.addAttribute("fightLog", fightLog);

        //remove dots and br
        List<String> fightLog2 = new ArrayList<>();
        for (String str : fightLog) {
            str = str.replace(".", "");
            str = str.replace("<br />", "");
            str = str.replace("--", "");
            str = str.replace("life ", "life*");

            //change arrow with the first fighter
            String[] arr = str.split(" ");
            if (arr.length > 4) {
                int arrowIndex = arr.length - 5;
                String arrow = arr[arrowIndex];
                arr[arrowIndex] = arr[arrowIndex + 1];
                arr[arrowIndex + 1] = arr[arrowIndex + 2];
                arr[arrowIndex + 2] = arrow + " [" + arr[arrowIndex - 1] + "]  ";
                arr[arrowIndex - 1] = "";
                str = "";
                for (String strr : arr) {
                    str += strr + " ";
                }
                str = str.replace("*", " ");
            }

            fightLog2.add(str);
        }
//        List<String> fightLog2 = fightResultBeanLightService.getOneFightLog(id);
        model.addAttribute("fightLog2", fightLog2);

        return "oneFightLogLink";
    }
}
