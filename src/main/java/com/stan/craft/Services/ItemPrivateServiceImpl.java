package com.stan.craft.Services;

import com.stan.craft.Models.ItemPrivate;
import com.stan.craft.Repositories.ItemPrivateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemPrivateServiceImpl implements ItemPrivateService {

    private ItemPrivateRepository itemPrivateRepository;

    @Autowired
    public ItemPrivateServiceImpl(ItemPrivateRepository itemPrivateRepository) {
        this.itemPrivateRepository = itemPrivateRepository;
    }

    @Override
    public List<ItemPrivate> getAll() {
        return itemPrivateRepository.findAll();
    }

    @Override
    public ItemPrivate getOne(int itemId) {
        return itemPrivateRepository.getOne(itemId);
    }

    @Override
    @Transactional
    public ItemPrivate create(ItemPrivate item) {
        return itemPrivateRepository.saveAndFlush(item);
    }

    @Override
    @Transactional
    public ItemPrivate update(ItemPrivate item) {
        itemPrivateRepository.saveAndFlush(item);
        itemPrivateRepository.flush();
        return item;
    }

    @Override
    public void delete(int itemId) {
        itemPrivateRepository.deleteById(itemId);
    }

    @Override
    public void forceDelete(int id) {
        itemPrivateRepository.forceDelete(id);
    }

    @Override
    public void markForDelete(ItemPrivate item) {
        item.setMarkedForDelete(true);
        update(item);
    }

    @Override
    public void deleteItemsMarkedForDelete() {
        itemPrivateRepository.deleteItemsMarkedForDelete();
    }

    @Override
    public List<Integer> getItemsIDMarkedForDelete() {
        return itemPrivateRepository.getItemsIDMarkedForDelete();
    }


    @Override
    public boolean ifExists(int itemId) {
        return itemPrivateRepository.existsById(itemId);
    }
}
