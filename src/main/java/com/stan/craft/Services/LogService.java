package com.stan.craft.Services;

import com.stan.craft.Models.Beans.LogBean;
import com.stan.craft.Models.Page;
import org.springframework.data.domain.Slice;

import java.security.Principal;

public interface LogService {
    Slice<LogBean> getLogByUsername(Principal principal, Page page);

    LogBean logMessage(String username, String message);

    void chat(String sender, String receiver, String message);

    LogBean saveChat(String username, String receiver, String message);

    void deleteOldLog();

    void notifyAllUsers(String message);
}
