package com.stan.craft.Services;

import com.stan.craft.Models.Creep;
import com.stan.craft.Models.Item;

import java.util.List;

public interface GameService {

    void startTimeGenerator();

    void setMaxIndividualId();

    void deleteOldFightLog();

    void deleteOldLog();

    List<Creep> getAllCreeps();

    List<Integer> getAllLevels();

    List<Item> getAllItems();
}
