package com.stan.craft.Services;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.*;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Repositories.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SkillsServiceImpl implements SkillsService {

    private final SkillRepository skillRepository;
    private final ModelFactory modelFactory;

    @Autowired
    public SkillsServiceImpl(SkillRepository skillRepository, ModelFactory modelFactory) {
        this.skillRepository = skillRepository;
        this.modelFactory = modelFactory;
    }

    @Override
    public Skill create(Skill skill) {
        return skillRepository.saveAndFlush(skill);
    }

    @Override
    public Skill getOne(int skillId) {
        return skillRepository.getOne(skillId);
    }

    @Override
    public List<Skill> getAll() {
        return skillRepository.findAll();
    }

    @Override
//    @Transactional
    public Skill update(Skill skill) {
        return skillRepository.saveAndFlush(skill);
    }

    @Override
    @Transactional
    public void delete(int skillId) {
        skillRepository.deleteById(skillId);
    }

    @Override
//    @Transactional
    public Character applySkills(Individual individual, Character character) {
        //get only passive skills
        List<Skill> skills = character.getSkills().stream()
                .filter(s -> s.getType() == SkillType.Passive)
                .collect(Collectors.toList());

        for (Skill skill : skills) {
            if (isSkillAvailable(skill, character.getCharacterClass().name())) {
                individual = skill.applySkill(individual);
                update(skill); //I doubt it will work here...
            }
        }
        character = copyStatsFromIndivToChar(individual, character);
        return character;
    }

    @Override
    @Transactional
    public Individual applyCurses(Individual attacker, Individual cursed, String characterClassOrCreepName) {
        //get only curses
        List<Skill> curses = attacker.getSkills().stream()
                .filter(s -> s.getType() == SkillType.Curse)
                .collect(Collectors.toList());

        for (Skill curse : curses) {
            if (isSkillAvailable(curse, characterClassOrCreepName)) {
                cursed = curse.applySkill(cursed);
            }
        }
        return cursed;
    }

    @Override
    public boolean isSkillAvailable(Skill skill, String characterClassOrCreepName) {
        List<String> availability = Arrays.asList(skill.getAvailability().split(","));
        return availability.contains(characterClassOrCreepName);
    }

    @Override
    @Transactional
    public Character addSkill(Individual individual, Character character, SkillClass skillClass) {
        if (character.getSkills().stream().anyMatch(s -> s.getSkillClass() == skillClass)) {
            throw new IllegalArgumentException("You already have this skill");
        }
        List<Skill> skills = individual.getSkills();
        int totalSkillPointsUsed = 0;
        for (Skill skill : skills) {
            totalSkillPointsUsed += skill.getLevel();
        }
        if (totalSkillPointsUsed >= individual.getLevel()) {
            throw new IllegalArgumentException("No available skill points");
        }
        Skill skill = ModelFactory.skillsFactoryMethod(skillClass);
        if (skill.getType() == SkillType.Active &&
                individual.getSkills().stream().anyMatch(s -> s.getType() == SkillType.Active)) {
            throw new IllegalArgumentException("You can only use 1 active skill");
        }
        skill.setIndividual(individual);
        create(skill);

        character.getSkills().add(skill);
        if (skill.getType() == SkillType.Passive) {
            individual = skill.applySkill(individual);
        }
        character = copyStatsFromIndivToChar(individual, character);
        return character;
    }

    @Override
    @Transactional
    public Character removeSkill(Character character, SkillClass skillClass) {
        if (character.getSkills().stream().noneMatch(s -> s.getSkillClass() == skillClass)) {
            throw new IllegalArgumentException("You don't have this skill");
        }
        Skill skill = null;
        for (int i = 0; i < character.getSkills().size(); i++) {
            if (character.getSkills().get(i).getSkillClass() == skillClass) {
                skill = character.getSkills().get(i);
                break;
            }
        }
        character.getSkills().remove(skill);
        assert skill != null;
        delete(skill.getId());
        return character;
    }

    @Override
    @Transactional
    public Character skillLevelUpdate(Character character, Skill skill, int level) {
        if (skill == null) {
            throw new IllegalArgumentException("Skill not added yet");
        }
        skill.setLevel(level);
        if (skill.getType() != SkillType.Passive) {
            skill.applySkill(character); // just to trigger  setValue() of the Skill. We do not change Char because we do NOT write character = skill.apply() - check it!!
        }
        update(skill);
        return character;
    }

    @Override
    @Transactional
    public String availableSkillsForCharClass(CharacterClass characterClass) {
        String skills = null;
        switch (characterClass) {
            case Knight:
                skills = Constants.KNIGHT_SKILLS_AVAILABILITY;
                break;
            case Titan:
                skills = Constants.TITAN_SKILLS_AVAILABILITY;
                break;
            case Shaman:
                skills = Constants.SHAMAN_SKILLS_AVAILABILITY;
                break;
            case Slayer:
                skills = Constants.SLAYER_SKILLS_AVAILABILITY;
                break;
            case Wizard:
                skills = Constants.WIZARD_SKILLS_AVAILABILITY;
                break;
        }
        return skills;
    }

    @Override
    public Character copyStatsFromIndivToChar(Individual individual, Character character) {
        character.setFinalDamageMin(individual.getFinalDamageMin());
        character.setFinalDamageMax(individual.getFinalDamageMax());
        character.setFinalMagicDamage(individual.getFinalMagicDamage());
        character.setFinalLife(individual.getFinalLife());
        character.setFinalArmor(individual.getFinalArmor());
        character.setFinalSpeed(individual.getFinalSpeed());
        character.setFinalBlockChance(individual.getFinalBlockChance());
        return character;
    }

}
