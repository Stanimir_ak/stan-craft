package com.stan.craft.Services;

import com.stan.craft.Models.Character;
import com.stan.craft.Models.*;

import java.util.List;

public interface SkillsService {
    Skill create(Skill skill);

    Skill getOne(int skillId);

    List<Skill> getAll();

    Skill update(Skill skill);

    void delete(int skillId);

    Character applySkills(Individual individual, Character character);

    Individual applyCurses(Individual attacker, Individual cursed, String characterClassOrCreepName);

    boolean isSkillAvailable(Skill skill, String characterClassOrCreepName);

    Character addSkill(Individual individual, Character character, SkillClass skillClass);

    Character removeSkill(Character character, SkillClass skillClass);

    Character skillLevelUpdate(Character character, Skill skill, int level);

    String availableSkillsForCharClass(CharacterClass characterClass);

    Character copyStatsFromIndivToChar(Individual individual, Character character);
}
