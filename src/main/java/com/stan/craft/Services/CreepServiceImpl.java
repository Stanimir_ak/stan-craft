package com.stan.craft.Services;

import com.stan.craft.Constants.StaticVar;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Creep;
import com.stan.craft.Models.Item;
import com.stan.craft.Repositories.CreepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;

@Service
public class CreepServiceImpl implements CreepService {


    private final CreepRepository creepRepository;
    private final UserService userService;

    @Autowired
    public CreepServiceImpl(CreepRepository creepRepository, UserService userService) {
        this.creepRepository = creepRepository;
        this.userService = userService;
    }

    @Override
    public Creep getOne(int id) {
        return creepRepository.getOne(id);
    }

    @Override
    public List<Creep> getAll() {
        return creepRepository.findAll();
    }

    @Override
    public List<Creep> getCreepsByLevel(int levelFrom, int levelTo) {
        return creepRepository.getCreepsByLevel(levelFrom, levelTo);
    }

    @Override
    public List<Creep> getBossesByLevel(int levelFrom, int levelTo) {
        return creepRepository.getBossesByLevel(levelFrom, levelTo);
    }

    @Override
    public int getGamesNumberOfCreeps() {
        return (int) creepRepository.count();
    }

    @Override
    public List<Creep> getAllNormalCreeps() {
        return creepRepository.getAllNormalCreeps();
    }

    @Override
    public List<Creep> getAllBosses() {
        return creepRepository.getAllBosses();
    }

    @Override
    public List<Creep> getAllGods() {
        return creepRepository.getAllGods();
    }

    @Override
    public Creep create(Creep creep, Principal principal) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
//        creep.setId(StaticVar.IndividualId);
//        StaticVar.IndividualId++;
        return creepRepository.saveAndFlush(creep);
    }

    @Override
    @Transactional
    public Creep update(Creep creep, Principal principal) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
        return creepRepository.saveAndFlush(creep);
    }

    @Override
    @Transactional
    public void delete(int id, Principal principal) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
        creepRepository.deleteById(id);
    }

    @Override
    public List<Item> dropItems(Creep creep, Character character) {
        return null;
    }

    @Override
    public int getMaxId() {
        return creepRepository.count() == 0 ? 0 : creepRepository.findFirstByOrderByIdDesc().getId();
    }
}
