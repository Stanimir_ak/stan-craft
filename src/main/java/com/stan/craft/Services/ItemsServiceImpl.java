package com.stan.craft.Services;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Constants.StaticVar;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.*;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Repositories.ItemsRepository;
import com.stan.craft.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemsServiceImpl implements ItemsService {

    private final ItemsRepository itemsRepository;
    private UserService userService;
    private ModelFactory modelFactory;
    private DropChancesService dropChancesService;
    private ItemPrivateService itemPrivateService;

    @Autowired
    public ItemsServiceImpl(ItemsRepository itemsRepository, UserService userService, ModelFactory modelFactory,
                            DropChancesService dropChancesService, ItemPrivateService itemPrivateService) {
        this.itemsRepository = itemsRepository;
        this.userService = userService;
        this.modelFactory = modelFactory;
        this.dropChancesService = dropChancesService;
        this.itemPrivateService = itemPrivateService;
    }

    @Override
    public List<Item> getAll() {
        return itemsRepository.findAll();
    }

    @Override
    public List<Item> getItemsAccordingToLevelFromDB(int level) {
        return itemsRepository.getItemsAccordingToLevel(level);
    }

    @Override
    public List<Item> getItemsAccordingToLevelFromRAM(int level) {
        return StaticVar.AllItems.stream()
                .filter(i -> i.getLevel() == level)
                .collect(Collectors.toList());
    }

    @Override
    public Item getOne(int itemId) {
        if (!ifExists(itemId)) {
            throw new EntityNotFoundException("Item not found");
        }
        return itemsRepository.getOne(itemId);
    }

    @Override
    public Item getOneByLevelAndClass(int level, ItemClass itemClass) {
        return itemsRepository.getOneByLevelAndClass(level, itemClass);
    }

    @Override
    public Item getOneByName(String name) {
        return itemsRepository.getOneByName(name);
    }

    @Override
    @Transactional
    public Item create(Item item, Principal principal) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
        return itemsRepository.saveAndFlush(item);
    }

    @Override
    @Transactional
    public Item update(Item item, Principal principal) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
        if (!ifExists(item.getId())) {
            throw new EntityNotFoundException("Item not found");
        }
        return itemsRepository.saveAndFlush(item);
    }

    @Override
    @Transactional
    public void delete(int itemId, Principal principal) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
        if (!ifExists(itemId)) {
            throw new EntityNotFoundException("Item not found");
        }
        itemsRepository.deleteById(itemId);
    }

    @Override
    public boolean ifExists(int itemId) {
        return itemsRepository.existsById(itemId);
    }

    @Override
    @Transactional
    public ItemPrivate dropItemFromLooser(Character winner, DropChances dropChancesLooser) {
        DropChances dropChances = dropChancesLooser;
        boolean hasCrystalRingWithGold = false;
        if (Utils.hasItemClassEquipped(winner, ItemClass.RING)) {
            if ("Gold".equals(Utils.getItemFromCollectionByClass(winner.getEquipment(), ItemClass.RING).getCrystal())) {
                if (dropChancesLooser.getDropId() < Constants.MAX_ID_OF_DROP_CHANCES) {
                    dropChances = ModelFactory.createDropChancesFromId(dropChancesLooser.getDropId() + 1);
                    //God items cannot be dropped from Boss because of the crystal
                    if (dropChances.getDropId() == 5) { // because we already incremented + 1
                        hasCrystalRingWithGold = true;
                    }
                }
            }

        }
        return dropItemFromDropChances(winner, dropChances, hasCrystalRingWithGold);
    }

    @Override
    @Transactional
    public ItemPrivate dropItemFromDropChances(Character winner, DropChances dropChances, boolean hasCrystalRingWithGold) {
        int randomNum = Utils.getRandomNumberUsingNextInt(1, 100);
        int dropItemLevel = 0;
        int[] arr = createArrOfDropChances(dropChances);
        //God items cannot be dropped from Boss because of the crystal
        if (hasCrystalRingWithGold) {
            arr[7] += arr[8];
            arr[8] = 0;
        }
        int firstLevelWithPercentage = calculateFirstLevelWithPercentage(arr);
        int sum = 0;

        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if (randomNum <= sum) {
                if (i == 0) {
                    dropItemLevel = 1;
                } else if (i == 9) {
                    if (Utils.randomChance(0.1)) { //Total chance =10% * 1% = 0.001 =  10% chance to get Crystal if you get the 1% of the ItemClass !!
                        dropItemLevel = 0;
                    } else {
                        dropItemLevel = firstLevelWithPercentage;
                    }
                } else {
                    dropItemLevel = i * 10;
                }
                break;
            }
        }

        List<Item> dropList;
        if (StaticVar.AllItems == null) {
            //If we use Tests, the StaticVar.AllItems is not loaded yet - so get from DB
            // 1 x DB connection - must not come to this line
            dropList = getItemsAccordingToLevelFromDB(dropItemLevel);
        } else {
            dropList = getItemsAccordingToLevelFromRAM(dropItemLevel);
        }

        if (dropList == null || dropList.size() == 0) {
            return null;
        }

        Item itemDropped = dropList.get(Utils.getRandomNumberUsingNextInt(0, dropList.size() - 1));

        ItemPrivate itemPrivateDropped = createItemPrivateFromBaseItem(itemDropped);
        if (itemBetterOfWhatCharHas(winner, itemPrivateDropped)) {
            //TODO itemBetterOfWhatCharHave - why is letting metal item pass through althogh the are already 2 metal with the same name?????
            //TODO 1 x DB connection
            itemPrivateService.create(itemPrivateDropped);
        } else {
            //if char already has 2 better items with the same name in the inventory - no need to give him this one...
            return null;
        }
        //used for Testing, it is @Transient
        if (itemPrivateDropped == null) {
            itemPrivateDropped = new ItemPrivate();
            itemPrivateDropped.setDropItemLevel(dropItemLevel);
        }
        itemPrivateDropped.setDropItemLevel(dropItemLevel);
        return itemPrivateDropped;
    }

    private int[] createArrOfDropChances(DropChances dropChances) {
        int[] arr = new int[Constants.NUMBER_OF_ITEM_LEVELS];
        arr[0] = dropChances.getChanceLevel1();
        arr[1] = dropChances.getChanceLevel10();
        arr[2] = dropChances.getChanceLevel20();
        arr[3] = dropChances.getChanceLevel30();
        arr[4] = dropChances.getChanceLevel40();
        arr[5] = dropChances.getChanceLevel50();
        arr[6] = dropChances.getChanceLevel60();
        arr[7] = dropChances.getChanceLevel70();
        arr[8] = dropChances.getChanceLevel80();
        arr[9] = dropChances.getChanceCrystal();
        return arr;
    }

    private int calculateFirstLevelWithPercentage(int[] arr) {
        int result = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                if (i == 0) {
                    result = 1;
                } else if (i == 9) {
                    result = 0;
                } else {
                    result = i * 10;
                }
                break;
            }
        }
        return result;
    }

    public ItemPrivate createItemPrivateFromBaseItem(Item item) {
        ItemPrivate itemPrivate = new ItemPrivate();
        double riseMax = item.getRise();
        double riseFinal = Utils.round(Utils.getRandomNumberUsingNextDouble(1.0, riseMax), 2);

        //increase ItemPrivate's stats by rise number
        itemPrivate.setDamageMin((int) (item.getDamageMin() * riseFinal));
        itemPrivate.setDamageMax((int) (item.getDamageMax() * riseFinal));
        itemPrivate.setMagicDamage((int) (item.getMagicDamage() * riseFinal));
        itemPrivate.setArmor((int) (item.getArmor() * riseFinal));
        itemPrivate.setLife((int) (item.getLife() * riseFinal));

        itemPrivate.setBaseItemId(item.getId());
        if ("Crystal Ring".equals(item.getName())) {
            itemPrivate.setLevel(1);
        } else {
            itemPrivate.setLevel(item.getLevel());
        }
        itemPrivate.setItemClass(item.getItemClass());
        itemPrivate.setName(item.getName());
        itemPrivate.setIsUnique(item.getIsUnique());
        itemPrivate.setAvailableInShop(item.getAvailableInShop());
        itemPrivate.setPrice(item.getPrice());
        itemPrivate.setBlockChance(item.getBlockChance());
        itemPrivate.setSpeed(item.getSpeed());
        itemPrivate.setCapacity(item.getCapacity());
        itemPrivate.setDescription(item.getDescription());

        itemPrivate.setCrystal("none");
        itemPrivate.setRise(riseFinal);

        return itemPrivate;
    }

    private boolean itemBetterOfWhatCharHas(Character winner, ItemPrivate itemPrivateDropped) {
        boolean result = true;
        int count = 0;
        List<ItemPrivate> itemsInInventoryWithSameName = winner.getInventory().stream()
                .filter(i -> i.getName().equals(itemPrivateDropped.getName()))
                .collect(Collectors.toList());

        for (ItemPrivate item : itemsInInventoryWithSameName) {
            //TODO itemBetterOfWhatCharHave - I ROUNDED IT!! - check it -
            // why is letting metal item pass through althogh the are already 2 metal with the same name?????
            if (Utils.round(item.getRise(), 2) >= Utils.round(itemPrivateDropped.getRise(), 2)) {
                count++;
            }
            if (count == 2) {
                result = false;
                break;
            }
        }
        if (itemPrivateDropped.getName().contains("Metal") && result && itemsInInventoryWithSameName.size() == 2) {
            System.out.println("Bug - itemBetterOfWhatCharHas - metal item");
        }
        return result;
    }
}
