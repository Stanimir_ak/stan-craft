package com.stan.craft.Services;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Constants.StaticVar;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Beans.DmgBean;
import com.stan.craft.Models.Beans.FightResultBean;
import com.stan.craft.Models.Beans.FightResultBeanLight;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.*;
import com.stan.craft.Models.DTO.FightHelperDTO;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Repositories.CharacterRepository;
import com.stan.craft.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;
    private final ItemsService itemsService;
    private final ItemPrivateService itemPrivateService;
    private final CreepService creepService;
    private final UserService userService;
    private final LogService logService;
    private final FightResultBeanLightService fightResultBeanLightService;
    private final ModelFactory modelFactory;
    private final SkillsService skillsService;

    @Autowired
    public CharacterServiceImpl(CharacterRepository characterRepository, ItemsService itemsService,
                                ItemPrivateService itemPrivateService, CreepService creepService, UserService userService,
                                LogService logService, FightResultBeanLightService fightResultBeanLightService,
                                ModelFactory modelFactory, SkillsService skillsService) {
        this.characterRepository = characterRepository;
        this.itemsService = itemsService;
        this.itemPrivateService = itemPrivateService;
        this.creepService = creepService;
        this.userService = userService;
        this.logService = logService;
        this.fightResultBeanLightService = fightResultBeanLightService;
        this.modelFactory = modelFactory;
        this.skillsService = skillsService;
    }

    @Override
    public List<Character> getAll() {
        return characterRepository.findAll();
    }

    @Override
    public List<Character> getAllSortedByExp() {
        return characterRepository.findAll(Sort.by(Sort.Direction.DESC, "experience"));
    }

    @Override
    public Character getOne(int id) {
        checkIfExistsAndThrow(id);
        return characterRepository.getOne(id);
    }

    @Override
    public Character getOneByName(String name) {
        checkIfExistsAndThrow(name);
        return characterRepository.getOneByName(name);
    }

    @Override
    @Transactional
    public Character create(Principal principal, Character character) {
        if (principal == null) {
            throw new IllegalArgumentException("Not authorized");
        }
        User user = userService.getByName(principal.getName());
//        character.setId(StaticVar.IndividualId);
//        StaticVar.IndividualId++;
        Character character1 = characterRepository.saveAndFlush(character);
        user.getCharacters().add(character1);
        user.setCurrentCharacter(character);
        userService.update(user);
        return character1;
    }

    @Override
    @Transactional
    public Character updateStatsFromBaseStats(Character character) {
        int level = character.getLevel();
        character.setDamageMin(character.getDamageMinBase() * level);
        character.setDamageMax(character.getDamageMaxBase() * level);
        character.setMagicDamage(character.getMagicDamageBase() * level);
        character.setArmor(character.getArmorBase() * level);
        character.setLife(character.getLifeBase() * level);
        return character;
    }

    @Override
    @Transactional
    public Character updateEquipStats(Character character) {
        checkIfExistsAndThrow(character.getId());
        List<ItemPrivate> equipment = character.getEquipment();
        int equipDmgMin = character.getDamageMin();
        int equipDmgMax = character.getDamageMax();
        int equipMagicDamage = character.getMagicDamage();
        int equipArmor = character.getArmor();
        int equipLife = character.getLife();
        double equipBlock = 0;
        if (!hasItemClassEquipped(character, ItemClass.SHIELD)) {
            equipBlock = character.getBlockChance();
        }
        double equipSpeed = character.getSpeed();

        for (int i = 0; i < equipment.size(); i++) {
            equipDmgMin += equipment.get(i).getDamageMin();
            equipDmgMax += equipment.get(i).getDamageMax();
            equipMagicDamage += equipment.get(i).getMagicDamage();
            equipArmor += equipment.get(i).getArmor();
            equipLife += equipment.get(i).getLife();
            equipBlock += Utils.round(equipment.get(i).getBlockChance(), 2);
            equipSpeed -= Utils.round(equipment.get(i).getSpeed(), 2);
        }

        character.setEquipDamageMin(equipDmgMin);
        character.setEquipDamageMax(equipDmgMax);
        character.setEquipMagicDamage(equipMagicDamage);
        character.setEquipArmor(equipArmor);
        character.setEquipBlockChance(equipBlock);
        character.setEquipLife(equipLife);
        character.setEquipSpeed(Utils.round(equipSpeed, 1));

        //RING increases in the end!!
        if (hasItemClassEquipped(character, ItemClass.RING)) {
            ItemPrivate ring = getItemFromCollectionByClass(character.getEquipment(), ItemClass.RING);
            if ("Amber".equals(ring.getCrystal())) {
                equipSpeed = Utils.round(equipSpeed * 0.8, 1);
                character.setEquipSpeed(equipSpeed);
            }
            if ("Amethyst".equals(ring.getCrystal())) {
                equipArmor = (int) (equipArmor * 1.2);
                character.setEquipArmor(equipArmor);
            }
            if ("Diamond".equals(ring.getCrystal())) {
                equipMagicDamage = (int) (equipMagicDamage * 1.2);
                character.setEquipMagicDamage(equipMagicDamage);
            }
            if ("Emerald".equals(ring.getCrystal())) {
                equipBlock = Utils.round(equipBlock + 0.1, 2);
                character.setEquipBlockChance(equipBlock);
            }
            if ("Gold".equals(ring.getCrystal())) {
                //is implemented in ItemServiceImpl
            }
            if ("Sapphire".equals(ring.getCrystal())) {
                equipDmgMin = (int) (equipDmgMin * 1.2);
                equipDmgMax = (int) (equipDmgMax * 1.2);
                character.setEquipDamageMin(equipDmgMin);
                character.setEquipDamageMax(equipDmgMax);
            }
            if ("Ruby".equals(ring.getCrystal())) {
                equipLife = (int) (equipLife * 1.2);
                character.setEquipLife(equipLife);
            }
        }

        //update also final stats, because the char may not have skills, and may not go into updateFinalStats method
        character.setFinalDamageMin(equipDmgMin);
        character.setFinalDamageMax(equipDmgMax);
        character.setFinalMagicDamage(equipMagicDamage);
        character.setFinalArmor(equipArmor);
        character.setFinalBlockChance(equipBlock);
        character.setFinalLife(equipLife);
        character.setFinalSpeed(Utils.round(equipSpeed, 1));
        return character;
    }

    @Override
    @Transactional
    public Character updateFinalStatsAndDB(Character character) {
        checkIfExistsAndThrow(character.getId());
        character = skillsService.applySkills(character, character);
        characterRepository.saveAndFlush(character);
        return character;
    }

    @Override
    @Transactional
    public Character userUpdatesDB(Character character, Principal principal) {
        checkIfExistsAndThrow(character.getId());
        userService.isAdminOrCharCreatorAndThrow(principal, character);
        characterRepository.saveAndFlush(character);
        return character;
    }

    @Override
    @Transactional
    public Character updatesDB(Character character) {
        characterRepository.saveAndFlush(character);
        return character;
    }

    @Override
    @Transactional
    public void delete(Principal principal, int id) {
        checkIfExistsAndThrow(id);
        User user = userService.getPrincipalUser(principal);
        if (user.getCurrentCharacter() != null && user.getCurrentCharacter().getId() == id) {
            throw new IllegalArgumentException("Can not delete the current character. To delete it first change current character");
        }
        boolean userHasTheChar = user.getCharacters().stream().anyMatch(c -> c.getId() == id);
        if (!userHasTheChar && !userService.isAdmin(principal)) {
            throw new EntityNotFoundException("This is not your Character. Cannot delete.");
        }
        checkIfSomeoneIsDuelingThisCharAndRemoveIt(id);
        characterRepository.deleteById(id);
    }

    @Override
    public boolean ifExistsByName(int id) {
        return characterRepository.existsById(id);
    }

    @Override
    public void checkIfExistsAndThrow(int id) {
        if (!ifExistsByName(id)) {
            throw new EntityNotFoundException("Character not found");
        }
    }

    @Override
    public void checkIfExistsAndThrow(String charName) {
        if (!ifExistsByName(charName)) {
            throw new EntityNotFoundException("Character not found");
        }
    }

    @Override
    public boolean ifExistsByName(String characterName) {
        //JUnit cannot work with jpa repo .findByName(characterName).isPresent() we I will bypass this check and return true
        if (isJUnitTest()) {
            return true;
        }
        return characterRepository.findByName(characterName).isPresent();
    }

    @Override
    public void buy(ItemPrivate item) {
    }

    @Override
    @Transactional
    public Character useItemIfBetterOrStashIt(Character character, ItemPrivate itemNew) {
        ItemClass itemNewClass = itemNew.getItemClass();
        if (ItemClass.RING == itemNewClass || ItemClass.CRYSTAL == itemNewClass) {
            return useIfRingOrCrystal(character, itemNew);
        } else {
            if (itemNew.getLevel() > character.getLevel()) {
                stashItem(character, itemNew);
            } else {
                if (!hasItemClassEquipped(character, itemNew.getItemClass())) {
                    character.getEquipment().add(itemNew);
                    logMessageWhenEquipped(character, itemNew);
                } else {
                    ItemPrivate itemEquipped = getItemFromCollectionByClass(character.getEquipment(), itemNew.getItemClass());
                    if (itemEquipped.getIsUnique() == 1) {
                        useIfEquippedIsUnique(character, itemNew, itemEquipped);
                    } else {
                        if (itemNew.getIsUnique() == 1) {
                            useIfNewUniqueAndEqquippedNot(character, itemNew, itemEquipped);
                        } else {
                            useIfBothNotUnique(character, itemNew, itemEquipped);
                        }
                    }
                }
            }
        }
        return character;
    }

    private void useIfBothNotUnique(Character character, ItemPrivate itemNew, ItemPrivate itemEquipped) {
        if (itemEquipped.getLevel() < itemNew.getLevel() ||
                (itemEquipped.getLevel() == itemNew.getLevel() && itemEquipped.getRise() < itemNew.getRise())) {
            //new is better - use NEW
            character.getEquipment().remove(itemEquipped);
            addToInventoryAndRemoveIfNeeded(character, itemEquipped);
            character.getEquipment().add(itemNew);
            logMessageWhenEquipped(character, itemNew);
        } else {
            //equipped is better- use equipped
            addToInventoryAndRemoveIfNeeded(character, itemNew);
        }
    }

    private void useIfNewUniqueAndEqquippedNot(Character character, ItemPrivate itemNew, ItemPrivate itemEquipped) {
        //equipped not unique; newItem - unique but lower level - use EQUIPPED
        if (itemEquipped.getLevel() > itemNew.getLevel()) {
            addToInventoryAndRemoveIfNeeded(character, itemNew);
        } else {
            //equipped not unique; newItem - unique and equal or higher level - use NEW
            character.getEquipment().remove(itemEquipped);
            addToInventoryAndRemoveIfNeeded(character, itemEquipped);
            character.getEquipment().add(itemNew);
            logMessageWhenEquipped(character, itemNew);
        }
    }

    private void useIfEquippedIsUnique(Character character, ItemPrivate itemNew, ItemPrivate itemEquipped) {
        if (itemNew.getIsUnique() == 1) {
            if (itemNew.getLevel() > itemEquipped.getLevel()) {
                //both unique, new bigger level
                character.getEquipment().remove(itemEquipped);
                addToInventoryAndRemoveIfNeeded(character, itemEquipped);
                character.getEquipment().add(itemNew);
                logMessageWhenEquipped(character, itemNew);
            } else {
                //both unique, new equal or lower level
                addToInventoryAndRemoveIfNeeded(character, itemNew);
            }
        } else {
            //equipped unique, new - not
            addToInventoryAndRemoveIfNeeded(character, itemNew);
        }
    }

    private void stashItem(Character character, ItemPrivate itemNew) {
        logService.logMessage(character.getUserCreator(),
                character.getName() + ": " + "-----Item----- " + itemNew.getName() +
                        " Item's level greater than character's level!!");
        addToInventoryAndRemoveIfNeeded(character, itemNew);
    }

    private Character useIfRingOrCrystal(Character character, ItemPrivate itemNew) {
        if (!hasItemClassEquipped(character, ItemClass.BAG)) {
            itemPrivateService.markForDelete(itemNew);
            return character;
        }
        ItemPrivate bagEquipped = getItemFromCollectionByClass(character.getEquipment(), ItemClass.BAG);
        if (character.getBag().size() < bagEquipped.getCapacity()) {
            if (ItemClass.CRYSTAL == itemNew.getItemClass()) {
                character.getBag().add(itemNew);
                character.setCrystals(character.getCrystals() + 1);
            } else {
                if (ItemClass.RING == itemNew.getItemClass() && !hasTheItemNTimesBag(3, character, itemNew.getName())) {
                    character.getBag().add(itemNew);
                } else {
                    //it is ring and we have 3 already
                    itemPrivateService.markForDelete(itemNew);
                }
            }
        } else {
            itemPrivateService.markForDelete(itemNew);
        }
        return character;
    }

    private void addToInventoryAndRemoveIfNeeded(Character character, ItemPrivate item) {
        if (!hasTheItemNTimesInventory(2, character, item.getName())) {
            character.getInventory().add(item);
            //items with rise 1.0 (metal) no need to switch them
        } else {
            if (Utils.round(item.getRise(), 1) > Utils.round(1.0, 1)) {
                //remove the worst item
                character.getInventory().add(item);
                ItemPrivate itemToRemove = getTheWorstItemFromInvent(character, item);
                if (itemToRemove != null) {
                    character.getInventory().remove(itemToRemove);
                    //TODO  2 -- DB conn: 1 to remove item from inventory, 2 to remmove it from table item_private
                    updatesDB(character);
                    itemPrivateService.markForDelete(itemToRemove);
                } else {
                    System.out.println("Bug?! getTheWorstItemFromInvent() return null??");
                }
            } else {
                itemPrivateService.markForDelete(item);
            }
        }
    }

    @Override
    @Transactional
    public Character useItemFromOwnInventory(Character character, int id, int itemId) {
        checkIfExistsAndThrow(id);
        ItemPrivate item = null;
        try {
            item = getItemFromCollectionById(character.getInventory(), itemId);
        } catch (Exception e) {
            throw new EntityNotFoundException("Item not found in this collection, or item name is wrong");
        }
        if (item.getLevel() > character.getLevel()) {
            throw new IllegalArgumentException(item.getName() + " requires level: " + item.getLevel());
        }
        if (item.getItemClass() == ItemClass.BAG && character.getBag().size() > item.getCapacity()) {
            throw new IllegalArgumentException("This bag cannot cary items from current bag. To change them remove items from current bag");
        }

        //remove equipped if any and put it in invent if there are 2 of this name ot less
        if (hasItemClassEquipped(character, item.getItemClass())) {
            ItemPrivate itemEquipped = getItemFromCollectionByClass(character.getEquipment(), item.getItemClass());

            character.getEquipment().remove(itemEquipped);
            character.getInventory().remove(item);
            character.getEquipment().add(item);
            addToInventoryAndRemoveIfNeeded(character, itemEquipped);
        } else {
            //put the new item
            character.getEquipment().add(item);
            character.getInventory().remove(item);
        }

        character = updateEquipStats(character);
        return updateFinalStatsAndDB(character);
    }

    @Override
    @Transactional
    public Character transformRing(Character character, int ringId, int crystalId) {
        if (!itemPrivateService.ifExists(ringId) || !itemPrivateService.ifExists(crystalId)) {
            throw new EntityNotFoundException("Item not found in this collection, or item name is wrong");
        }
        ItemPrivate ring = itemPrivateService.getOne(ringId);
        if (!"Empty Ring".equals(ring.getName())) {
            throw new IllegalArgumentException("Only Empty Rings can be transformed");
        }
        ItemPrivate crystal = itemPrivateService.getOne(crystalId);

        boolean ringIdInBag = character.getBag().stream().anyMatch(i -> i.getId() == ringId);
        boolean crystalIdInBag = character.getBag().stream().anyMatch(i -> i.getId() == crystalId);
        if (!ringIdInBag || !crystalIdInBag) {
            throw new IllegalArgumentException("Can transform ring only if you carry the ring and crystal in the bag");
        }

        ItemPrivate newCrystalRing = itemsService.createItemPrivateFromBaseItem(itemsService.getOneByName("Crystal Ring"));
        newCrystalRing.setCrystal(crystal.getName());
        newCrystalRing.setDescription(newCrystalRing.getDescription() + crystal.getDescription());
        itemPrivateService.create(newCrystalRing);

        ItemPrivate ringToRemove = getItemFromCollectionById(character.getBag(), ringId);
        ItemPrivate crystalToRemove = getItemFromCollectionById(character.getBag(), crystalId);

        character.getBag().remove(ringToRemove);
        character.getBag().remove(crystalToRemove);
        character.setCrystals(character.getCrystals() - 1);
        character.getBag().add(newCrystalRing);

        itemPrivateService.delete(ringId);
        itemPrivateService.delete(crystalId);

        character = updateEquipStats(character);
        updateFinalStatsAndDB(character);
        User creator = userService.getByName(character.getUserCreator());
        userService.update(creator);
        return character;
    }

    @Override
    @Transactional
    public Character useItemFromOwnBag(Character character, int id, int itemId) {
        checkIfExistsAndThrow(id);
        ItemPrivate item;
        try {
            item = getItemFromCollectionById(character.getBag(), itemId);
        } catch (Exception e) {
            throw new EntityNotFoundException("Item not found in this collection, or item name is wrong");
        }
        if (item.getLevel() > character.getLevel()) {
            throw new IllegalArgumentException(item.getName() + " requires level: " + item.getLevel());
        }
        if (item.getItemClass() == ItemClass.CRYSTAL) {
            throw new IllegalArgumentException("Cannot ware raw Crystals, they must be transformed with Empty Ring");
        }

        //remove equipped if any and put it in Bag if there are 2 of this name ot less
        if (hasItemClassEquipped(character, item.getItemClass())) {
            ItemPrivate itemEquipped = getItemFromCollectionByClass(character.getEquipment(), item.getItemClass());
            character.getBag().add(itemEquipped);
            character.getEquipment().remove(itemEquipped);
        }

        //put the new item
        character.getEquipment().add(item);
        character.getBag().remove(item);

        character = updateEquipStats(character);
        updateFinalStatsAndDB(character);
        User creator = userService.getByName(character.getUserCreator());
        userService.update(creator);
        return character;
    }

    //NOT USED SO FAR
    @Override
    @Transactional
    public void removeItemFromEquipment(Principal principal, int id, int itemId) {
        //Remove only if there are already 2 copies of the item in the inventory, else - add to inventory
//        checkIfExistsAndThrow(id);
//        Character character = getOne(id);
//        //TODO DB connection to be removed! import character
//        userService.isAdminOrCharCreatorAndThrow(principal, character);
//
//        ItemPrivate item;
//        try {
//            item = getItemFromCollectionById(character.getEquipment(), itemId);
//        } catch (Exception e) {
//            throw new EntityNotFoundException("Item not found in this collection, or item name is wrong");
//        }
//        if ("RING".equals(item.getItemClass().name())) {
//            boolean hasBagEquipped = character.getEquipment().stream()
//                    .anyMatch(i -> i.getItemClass().name().equals("BAG"));
//            if (!hasBagEquipped) {
//                return;
//            }
//            ItemPrivate bagEquipped = character.getEquipment().stream()
//                    .filter(i -> i.getItemClass().name().equals("BAG")).findFirst().get();
//            if (character.getBag().size() < bagEquipped.getCapacity()) {
//                if ("Empty Ring".equals(item.getName()) &&
//                        hasTheItemNTimesBag(3, character, item.getName())) {
//                    throw new IllegalArgumentException("Bag can carry maximum 3 Empty Rings");
//                }
//                character.getBag().add(item);
//            } else {
//                throw new IllegalArgumentException("The Bag is full");
//            }
//        } else {
//            character.getInventory().add(item);
//            if (hasTheItemNTimesInventory(3, character, item.getName())) {
//                //remove the worst item
//                ItemPrivate itemToRemove = getTheWorstItemFromInvent(character, item);
//                character.getInventory().remove(itemToRemove);
//                itemPrivateService.delete(itemToRemove.getId());
//            }
//        }
//        character.getEquipment().remove(item);
//
//        character = updateEquipStats(character);
//        updateFinalStatsAndDB(character);
    }

    //NOT USED SO FAR
    @Override
    @Transactional
    public void removeItemFromInventory(Character character, ItemPrivate item) {
//        //TODO DB connection to be removed! import character
//        userService.isAdminOrCharCreatorAndThrow(principal, character);

//        try {
//            item = getItemFromCollectionById(character.getInventory(), itemId);
//        } catch (Exception e) {
//            throw new EntityNotFoundException("Item not found in this collection, or item name is wrong");
//        }

        character.getInventory().remove(item);
        //the item will be deleted in the next method
//        itemPrivateService.delete(item.getId());
        updatesDB(updateEquipStats(character));
    }

    @Override
    @Transactional
    public List<Character> giveItemToOtherCharacter(int idGiver, String charNameTaker, int itemId) {
        checkIfExistsAndThrow(idGiver);
        checkIfExistsAndThrow(charNameTaker);

        Character character1 = getOne(idGiver);
        Character character2 = getOneByName(charNameTaker);
        List<Character> result = new ArrayList<>();
        result.add(character1);
        result.add(character2);
        ItemPrivate item;

        List<ItemPrivate> collection;
        if (hasItemInCollectionById(character1.getInventory(), itemId)) {
            collection = character1.getInventory();
        } else if (hasItemInCollectionById(character1.getBag(), itemId)) {
            collection = character1.getBag();
        } else if (hasItemInCollectionById(character1.getEquipment(), itemId)) {
            //NOT USED SO FAR
            collection = character1.getEquipment();
        } else {
            throw new EntityNotFoundException("Item not found");
        }

        item = getItemFromCollectionById(collection, itemId);
        character2 = useItemIfBetterOrStashIt(character2, item);
        logService.logMessage(character2.getUserCreator(),
                character2.getName() + ": " + "-----Item----- " + item.getName()
                        + " was given to character: " + character2.getName());
        collection.remove(item);
        if (item.getItemClass() == ItemClass.CRYSTAL) {
            character1.setCrystals(character1.getCrystals() - 1);
        }
        logService.logMessage(character1.getUserCreator(),
                character1.getName() + ": " + "-----Item----- " + item.getName()
                        + " You gave item to character " + character2.getName());

        character1 = updateEquipStats(character1);
        updateFinalStatsAndDB(character1);

        character2 = updateEquipStats(character2);
        updateFinalStatsAndDB(character2);
        return result;
    }

    @Override
    @Transactional
    public FightResultBean fight(Individual fighter1, Individual fighter2, Character f1, Character f2) {
//        refactor: the method was 300 lines before refactor:) now all methods = 240 lines
        FightResultBean fightResultBean = new FightResultBean();
        List<String> fightLog = new ArrayList<>();
        fightResultBean.setFightLog(fightLog);
        Character f1Coppy = saveState(fighter1);
        Character f2Coppy = saveState(fighter2);
        applyCurses(fighter1, fighter2, f1, f2);
        FightHelperDTO dto1 = createFighterHelperDTO(fighter1);
        FightHelperDTO dto2 = createFighterHelperDTO(fighter2);

        long logTime = System.currentTimeMillis();
        // ***** Stan Clock Generator ******
        double time = 0.1; //  1/10 seconds
        while (dto1.getLife() > 0 && dto2.getLife() > 0) {
            StringBuilder message = new StringBuilder();
            if (time == dto1.getTimeToHit()) {
                attack(dto1, dto2, fighter1, fighter2, fightLog, message);
                if (dto2.getLife() <= 0)
                    break;
            }
            if (time == dto2.getTimeToHit()) {
                attack(dto2, dto1, fighter2, fighter1, fightLog, message);
            }
            time += 0.1;
            time = Utils.round(time, 1); // fixes the bug with double + double!
        }

        restoreCharacter(fighter1, f1Coppy);
        restoreCharacter(fighter2, f2Coppy);
        System.out.println("hitLoop " + (System.currentTimeMillis() - logTime));

        logTime = System.currentTimeMillis();
        setWinner(fightResultBean, dto1, fighter1, fighter2);
        StringBuilder logWinner = new StringBuilder();

        //if both are characters - no reward
        if (fighter1.getType() != IndividualType.Character || fighter2.getType() != IndividualType.Character) {
            if (fightResultBean.getWinner().getType() == IndividualType.Character) {
                appendLogWinner(fightResultBean, logWinner);
                Character winner = assignWinner(f1, f2, fightResultBean);
                System.out.println("setWinner " + (System.currentTimeMillis() - logTime));
                rewardWinner(fighter2, winner, fightResultBean, logWinner);
            }
        }
        logMessages(fightResultBean, logWinner);
        return fightResultBean;
    }

    private void applyCurses(Individual fighter1, Individual fighter2, Character f1, Character f2) {
        String availability1 = getAvailabilityCurse(fighter1, f1);
        String availability2 = getAvailabilityCurse(fighter2, f2);
        skillsService.applyCurses(fighter2, fighter1, availability2);
        skillsService.applyCurses(fighter1, fighter2, availability1);
    }

    private Character assignWinner(Character f1, Character f2, FightResultBean fightResultBean) {
        Character winner;
        if (f1 != null && fightResultBean.getWinner().getName().equals(f1.getName())) {
            winner = f1;
        } else {
            winner = f2;
        }
        return winner;
    }

    private void appendLogWinner(FightResultBean fightResultBean, StringBuilder logWinner) {
        logWinner.append(fightResultBean.getWinner().getName()).append("(").append(fightResultBean.getWinner().getLevel())
                .append("): [WIN]  against  ").append(fightResultBean.getLooser().getName()).append(" (")
                .append(fightResultBean.getLooser().getLevel()).append(")");
    }

    private void logMessages(FightResultBean fightResultBean, StringBuilder logWinner) {
        long logTime = System.currentTimeMillis();
        if (logWinner.length() != 0) {
            //TODO  --2 x DB connection--
            logService.logMessage(fightResultBean.getWinner().getUserCreator(),
                    logWinner.toString());
        }
        if (fightResultBean.getLooser().getType() == IndividualType.Character) {
            String msg = fightResultBean.getLooser().getName() + "(" + fightResultBean.getLooser().getLevel() +
                    "): [[[[-LOSS-]]]] from " + fightResultBean.getWinner().getName() + " (" +
                    fightResultBean.getWinner().getLevel() + ")";
            logService.logMessage(fightResultBean.getLooser().getUserCreator(), msg);
        }

        //Log the fightLog in DB
        FightResultBeanLight fightResultBeanLight = modelFactory.createLightBeanFromFightBean(fightResultBean);
        //TODO  --DB connection--
        fightResultBeanLightService.logMessage(fightResultBeanLight);
        System.out.println("logBeans " + (System.currentTimeMillis() - logTime));
    }

    private void rewardWinner(Individual fighter2, Character winner,
                              FightResultBean fightResultBean, StringBuilder logWinner) {
        long logTime;
        int winnerLvl = winner.getLevel();
        if (winnerLvl < Constants.MAX_CHARACTER_LEVEL) {
            winner.setExperience
                    (winner.getExperience() + fightResultBean.getExperience());
        }
        winner.setGold(winner.getGold() + fightResultBean.getGold());

        if (Utils.randomChance(Constants.GENERAL_DROP_ITEM_PER_CENT)) {
            logTime = System.currentTimeMillis();
            winner = dropItem(fighter2, fightResultBean, logTime, logWinner, winner);
        }

        logTime = System.currentTimeMillis();
        //TODO  -- 1 DB connection-- to update exp and gold & to update final stats after updateEquipStats(winner) - it fucks up final stats! - could be changed
        updateFinalStatsAndDB(winner);
        System.out.println("updateFinalStatsAndDB " + (System.currentTimeMillis() - logTime));
        updateLevelIfNeeded(winner, logWinner, winnerLvl);
    }

    private void updateLevelIfNeeded(Character winner, StringBuilder logWinner, int winnerLvl) {
        if (winner.getLevel() < Constants.MAX_CHARACTER_LEVEL) {
            // no DB! connection for calculateLevel()
            int newLevel = calculateLevel(winner);
            boolean isWinnerLevelUp = newLevel > winnerLvl;
            if (isWinnerLevelUp) {
                winner.setLevel(newLevel);
                winner = updateStatsFromBaseStats(winner);
                logWinner.append(". ---Level UP--- ").append(newLevel).append("!");
                winner = updateEquipStats(winner);
                //TODO  --1 x DB connection on level up--
                updateFinalStatsAndDB(winner);
            }
        }
    }

    private Character dropItem(Individual fighter2, FightResultBean fightResultBean, long logTime, StringBuilder logWinner, Character winner) {
        //TODO -- 1 x DB conn if Item dropped is better than what winner have
        ItemPrivate itemDropped = itemsService.dropItemFromLooser(winner, fighter2.getDropChances());
        if (itemDropped != null) {
            fightResultBean.setItemDropped(itemDropped);
            logWinner.append(". ---Item---: ").append(itemDropped.getName()).append(", rise: ").append(itemDropped.getRise());
            //TODO 2 x DB conn because of addToInventoryAndRemoveIfNeeded()
            winner = useItemIfBetterOrStashIt(winner, itemDropped);
            System.out.println("dropItemFromCreep " + (System.currentTimeMillis() - logTime));

            logTime = System.currentTimeMillis();
            //TODO useItemIfBetterOrStashIt could return boolean equipmentChanged? if true then winner = updateEquipStats(winner);
            winner = updateEquipStats(winner);
            System.out.println("updateEquipStats " + (System.currentTimeMillis() - logTime));
        }
        return winner;
    }

    private void setWinner(FightResultBean fightResultBean, FightHelperDTO dto1, Individual fighter1, Individual fighter2) {
        if (dto1.getLife() <= 0) {
            fightResultBean.setWinner(fighter2);
            fightResultBean.setLooser(fighter1);
            fightResultBean.setExperience(fighter1.getExperience() / 10);
            fightResultBean.setGold(fighter1.getGold() / 10);
        } else {
            fightResultBean.setWinner(fighter1);
            fightResultBean.setLooser(fighter2);
            fightResultBean.setExperience(fighter2.getExperience() / 10);
            fightResultBean.setGold(fighter2.getGold() / 10);
        }
    }

    private void attack(FightHelperDTO dto1, FightHelperDTO dto2, Individual fighter1, Individual fighter2,
                        List<String> fightLog, StringBuilder message) {
        message.setLength(0);
        boolean ifCharBlocked = Utils.randomChance(fighter2.getFinalBlockChance());

        if (!ifCharBlocked) {
            applyShockSkill(dto1, dto2, fighter1, message);
            applyDynamiteSkill(dto1, fighter1, message);
        }
        boolean isOponentDead = hit(dto1, dto2, fighter1, fighter2, fightLog, message, ifCharBlocked);
        if (isOponentDead) return;
        applyMultyHit(dto1, dto2, fighter1, fighter2, fightLog, message);

        dto1.setTimeToHit(Utils.round(dto1.getTimeToHit() + fighter1.getFinalSpeed(), 1));
    }

    private boolean hit(FightHelperDTO dto1, FightHelperDTO dto2, Individual fighter1, Individual fighter2,
                        List<String> fightLog, StringBuilder message, boolean ifCharBlocked) {
        int damageDone = doHit(fighter1, fighter2, dto1.getLife(), dto2.getLife(), fightLog, message, dto1.getActiveSkill(), ifCharBlocked);
        dto2.setLife(dto2.getLife() - damageDone);
        if (dto2.getLife() <= 0) {
            StringBuilder msgWinner = new StringBuilder();
            if (fighter1.getType() == IndividualType.Boss) {
                fightLog.add(msgWinner.append(fighter1.getName()).append(" WINS!").toString());
            } else {
                fightLog.add(msgWinner.append(fighter1.getType()).append(" ").append(fighter1.getName()).append(" WINS!").toString());
            }
            return true;
        }
        return false;
    }

    private void applyDynamiteSkill(FightHelperDTO dto1, Individual fighter1, StringBuilder message) {
        //DYNAMITE SKILL 5% to start making 1 - x1.5 dmg
        if (dto1.getActiveSkill() != null && dto1.getActiveSkill().getSkillClass() == SkillClass.Dynamite &&
                !dto1.isDynamiteActivated() && dto1.getActiveSkill().getLevel() > 0) {
            if (Utils.randomChance(Constants.SKILL_DYNAMITE_CHANCE)) {
                fighter1.setFinalDamageMin((int) (fighter1.getFinalDamageMin() * Double.parseDouble(dto1.getActiveSkill().getValue())));
                fighter1.setFinalDamageMax((int) (fighter1.getFinalDamageMax() * Double.parseDouble(dto1.getActiveSkill().getValue())));
                dto1.setDynamiteActivated(true);
                message.append(" ## ").append(fighter1.getName()).append(" is deadly like a Dynamite!");
            }
        }
    }

    private void applyShockSkill(FightHelperDTO dto1, FightHelperDTO dto2, Individual fighter1, StringBuilder message) {
        //SHOCK SKILL 15% to shock enemy from 1.0 to 1.8 sec
        if (dto1.getActiveSkill() != null &&
                dto1.getActiveSkill().getSkillClass() == SkillClass.Shock && dto1.getActiveSkill().getLevel() > 0) {
            if (Utils.randomChance(Constants.SKILL_SHOCK_CHANCE)) {
                dto2.setTimeToHit(Utils.round(dto2.getTimeToHit() +
                        Double.parseDouble(dto1.getActiveSkill().getValue()), 1));
                message.append(" ## ").append(fighter1.getName()).append(" shocked the enemy!");
            }
        }
    }

    private void applyMultyHit(FightHelperDTO dto1, FightHelperDTO dto2, Individual fighter1, Individual fighter2,
                               List<String> fightLog, StringBuilder message) {
        //MULTI HIT SKILL 10% - 50% to hit twice
        boolean ifCharBlocked = Utils.randomChance(fighter2.getFinalBlockChance());
        ;
        if (dto1.getActiveSkill() != null && dto1.getActiveSkill().getSkillClass() == SkillClass.MultiHit &&
                dto1.getActiveSkill().getLevel() > 0) {
            if (Utils.randomChance(Double.parseDouble(dto1.getActiveSkill().getValue()))) {
                message.append(" ## ").append(fighter1.getName()).append(" Multy hit!");
                hit(dto1, dto2, fighter1, fighter2, fightLog, message, ifCharBlocked);
            }
        }
    }

    private void applyCriticalSkill(Individual f1, StringBuilder message, Skill activeSkill1, DmgBean dmgBean) {
        //CRITICAL SKILL (15%) - dmgMax-Min * (1 to 2)
        if (activeSkill1 != null && activeSkill1.getSkillClass() == SkillClass.Critical && activeSkill1.getLevel() > 0) {
            if (Utils.randomChance(Constants.SKILL_CRITICAL_CHANCE)) {
                if (f1.getMagicDamageBase() == 0) { //if not Wizard
                    dmgBean.setDmgMin((int) (dmgBean.getDmgMin() * Double.parseDouble(activeSkill1.getValue())));
                    dmgBean.setDmgMax((int) (dmgBean.getDmgMax() * Double.parseDouble(activeSkill1.getValue())));
                } else {
                    dmgBean.setDmgMagic((int) (dmgBean.getDmgMagic() * Double.parseDouble(activeSkill1.getValue())));
                }
                message.append(" ## Critical hit!");
            }
        }
    }

    private FightHelperDTO createFighterHelperDTO(Individual character) {
        FightHelperDTO dto = new FightHelperDTO();
        dto.setLife(character.getFinalLife());
        dto.setTimeToHit(Utils.round(character.getFinalSpeed(), 1));
        dto.setListActiveSkills(character.getSkills().stream()
                .filter(s -> s.getType() == SkillType.Active)
                .collect(Collectors.toList()));
        if (dto.getListActiveSkills().size() != 0) {
            dto.setActiveSkill(dto.getListActiveSkills().get(0));
        }
        dto.setDynamiteActivated(false);
        return dto;
    }

    @Override
//    @Transactional
    public List<String> getFightsInBattles() {
        long startTime_getFightsInBattles = System.currentTimeMillis();
        List<String> result = new ArrayList<>();
        //Each char is fighting once, If they fight twice the chars wont be updated with the rewards of the first fight -
        // need to getAll() again if they need to fight second time
        long logTime = System.currentTimeMillis();
        //TODO  --1 x DB connection--
        List<Character> allChars = getFighters();
        List<Character> duelChars = allChars.stream().filter(c -> c.getZone() == ZoneClass.Duel).collect(Collectors.toList());
        List<Character> arenaChars = allChars.stream().filter(c -> c.getZone() == ZoneClass.Arena).collect(Collectors.toList());
        List<Character> creepingChars = allChars.stream().filter(c -> c.getZone() != ZoneClass.Duel &&
                c.getZone() != ZoneClass.Arena).collect(Collectors.toList());

        // NO DB connection !!--
        List<Creep> allCreeps = StaticVar.AllCreeps;
        //If we use Tests, the StaticVar.AllCreeps is not loaded yet - so DB conn
        if (StaticVar.AllCreeps == null) {
            allCreeps = creepService.getAll();
        }
        List<Creep> creeps = allCreeps.stream().filter(c -> c.getType() == IndividualType.Creep).collect(Collectors.toList());
        List<Creep> bosses = allCreeps.stream().filter(c -> c.getType() == IndividualType.Boss).collect(Collectors.toList());
        List<Creep> gods = allCreeps.stream().filter(c -> c.getType() == IndividualType.God).collect(Collectors.toList());
        System.out.println("");
        System.out.println("getAllCharsAndCreeps " + (System.currentTimeMillis() - logTime));

        creepingCharsFight(result, creepingChars, creeps, bosses, gods);
        arenaCharsFight(result, arenaChars);
        duelCharsFight(result, allChars, duelChars);
        System.out.println("getFightsInBattles " + (System.currentTimeMillis() - startTime_getFightsInBattles));
        return result;
    }

    private void duelCharsFight(List<String> result, List<Character> allChars, List<Character> duelChars) {
        if (duelChars.size() > 0) {
            for (Character character : duelChars) {
                Character duelTarget = getOneFromAllByName(allChars, character.getDuel());
                if (duelTarget != null) {
                    fight(character, duelTarget, character, duelTarget);
                    result.add(character.getName() + " " + duelTarget.getName());
                }
            }
        }
    }

    private void arenaCharsFight(List<String> result, List<Character> arenaChars) {
        //TODO NOT TESTED!!! - need debugging
        if (arenaChars.size() > 1) {
            List<Character> similarChars;
            for (Character character : arenaChars) {
                similarChars = filterCharsByLvl(arenaChars, character.getLevel() - 5, character.getLevel() + 5);
                if (similarChars.size() <= 1) {
                    logService.logMessage(character.getUserCreator(),
                            "No similar characters in the Arena for: " + character.getName());
                    continue;
                }
                int index = Utils.getRandomNumberUsingNextInt(0, similarChars.size() - 1);
                while (similarChars.get(index).getId() == character.getId()) {
                    index = Utils.getRandomNumberUsingNextInt(0, similarChars.size() - 1);
                }
                Character character2 = similarChars.get(index);
                fight(character, character2, character, character2);
                result.add(character.getName() + " " + character2.getName());
            }
        }
    }

    private void creepingCharsFight(List<String> result, List<Character> creepingChars, List<Creep> creeps,
                                    List<Creep> bosses, List<Creep> gods) {
        List<Creep> creepsFinal = new ArrayList<>();
        for (Character char1 : creepingChars) {
            creepsFinal = switchCaseCreeps(creeps, bosses, gods, creepsFinal, char1);
            int index = Utils.getRandomNumberUsingNextInt(0, creepsFinal.size() - 1);
            Creep creep = creepsFinal.get(index);
            fight(char1, creep, char1, null);
            result.add(char1.getName() + " " + creep.getName());
        }
    }

    private List<Creep> switchCaseCreeps(List<Creep> creeps, List<Creep> bosses, List<Creep> gods,
                                         List<Creep> creepsFinal, Character char1) {
        int lvl = char1.getLevel();
        switch (char1.getZone()) {
            case LowCreeps:
                creepsFinal = filterCreepsByTypeAndLvl(creeps, IndividualType.Creep, lvl - 5, lvl);
                if (creepsFinal == null || creepsFinal.size() == 0) {
                    creepsFinal = filterCreepsByTypeAndLvl(creeps, IndividualType.Creep, lvl - 10, lvl);
                }
                break;
            case MidCreeps:
                creepsFinal = filterCreepsByTypeAndLvl(creeps, IndividualType.Creep, lvl - 2, lvl + 2);
                if (creepsFinal == null || creepsFinal.size() == 0) {
                    creepsFinal = filterCreepsByTypeAndLvl(creeps, IndividualType.Creep, lvl - 10, lvl);
                }
                break;
            case HighCreeps:
                creepsFinal = filterCreepsByTypeAndLvl(creeps, IndividualType.Creep, lvl, lvl + 5);
                if (creepsFinal == null || creepsFinal.size() == 0) {
                    creepsFinal = filterCreepsByTypeAndLvl(creeps, IndividualType.Creep, lvl - 5, lvl + 10);
                }
                break;
            case LowBosses:
                creepsFinal = filterCreepsByTypeAndLvl(bosses, IndividualType.Boss, lvl - 2, lvl + 2);
                if (creepsFinal == null || creepsFinal.size() == 0) {
                    creepsFinal = filterCreepsByTypeAndLvl(bosses, IndividualType.Boss, lvl - 5, lvl + 5);
                }
                break;
            case MidBosses:
                creepsFinal = filterCreepsByTypeAndLvl(bosses, IndividualType.Boss, lvl, lvl + 5);
                if (creepsFinal == null || creepsFinal.size() == 0) {
                    creepsFinal = filterCreepsByTypeAndLvl(bosses, IndividualType.Boss, lvl - 5, lvl + 5);
                }
                break;
            case HighBosses:
                creepsFinal = filterCreepsByTypeAndLvl(bosses, IndividualType.Boss, lvl, lvl + 10);
                if (creepsFinal == null || creepsFinal.size() == 0) {
                    creepsFinal = filterCreepsByTypeAndLvl(bosses, IndividualType.Boss, lvl - 5, lvl);
                }
                break;
            case Ares:
                creepsFinal = getCreepByName(gods, Gods.Ares);
                break;
            case Thanatos:
                creepsFinal = getCreepByName(gods, Gods.Thanatos);
                break;
            case Achlys:
                creepsFinal = getCreepByName(gods, Gods.Achlys);
                break;
            case Erebus:
                creepsFinal = getCreepByName(gods, Gods.Erebus);
                break;
            case Chaos:
                creepsFinal = getCreepByName(gods, Gods.Chaos);
                break;
            case Hades:
                creepsFinal = getCreepByName(gods, Gods.Hades);
                break;
            case Tartarus:
                creepsFinal = getCreepByName(gods, Gods.Tartarus);
                break;
        }
        //just in any case
        if (creepsFinal == null || creepsFinal.size() == 0) {
            creepsFinal = filterCreepsByTypeAndLvl(creeps, IndividualType.Creep, 0, lvl);
        }
        return creepsFinal;
    }

    @Override
    public boolean nameIsFree(String characterName) {
        return !characterRepository.findByName(characterName).isPresent();
    }

    @Override
    public int calculateLevel(Character character) {
        long exp = character.getExperience();
        List<Integer> allLevels = StaticVar.AllLevels;
        //If we use Tests, the StaticVar.AllLevels is not loaded yet - so ModelFactory.getAllLevels() - again no DB conn
        if (StaticVar.AllLevels == null) {
            allLevels = ModelFactory.getAllLevels();
        }
        int level = 1;
        for (int i = 0; i <= Constants.MAX_CHARACTER_LEVEL; i++) {
            if (exp < allLevels.get(i)) {
                level = i + 1;
                break;
            }
        }
        return level;
    }

    @Override
    public List<Character> getFighters() {
        List<Character> fighters = characterRepository.bringAllFighters();
        if (fighters == null || fighters.isEmpty()) {
            return new ArrayList<>();
        } else {
            return fighters;
        }
    }

    @Override
    public Character changeZone(Character character, ZoneClass newZone, String characterDuel) {
        if (newZone == ZoneClass.Duel) {
            if (!ifExistsByName(characterDuel)) {
                throw new IllegalArgumentException("Character not found");
            }
            character.setDuel(characterDuel);
        }
        character.setZone(newZone);
        updatesDB(character);
        return character;
    }

    @Override
    @Transactional
    public Character getCurrentCharacterOfUser(Principal principal) {
        User user = userService.getByName(principal.getName());
        return user.getCurrentCharacter();
    }

    @Override
    @Transactional
    public boolean isBestEquipped(Principal principal) {
        User user = userService.getPrincipalUser(principal);
        Character currentCharacter = user.getCurrentCharacter();
        if (currentCharacter == null) {
            throw new IllegalArgumentException("No current character");
        }
        List<ItemPrivate> equipment = currentCharacter.getEquipment();
        List<ItemPrivate> inventory = currentCharacter.getInventory();

        //check if the equipped item is best
        for (int i = 0; i < equipment.size(); i++) {
            ItemClass itemClass = equipment.get(i).getItemClass();
            int itemLevel = equipment.get(i).getLevel();
            double itemRise = equipment.get(i).getRise();
            List<ItemPrivate> itemsSameClassInInventory = inventory.stream()
                    .filter(item -> item.getItemClass().equals(itemClass))
                    .collect(Collectors.toList());
            for (ItemPrivate itemPrivate : itemsSameClassInInventory) {
                if ((itemPrivate.getLevel() > itemLevel &&
                        itemPrivate.getLevel() <= currentCharacter.getLevel()) ||
                        (itemPrivate.getLevel() == itemLevel && itemPrivate.getRise() > itemRise)) {
                    return false;
                }
            }
        }

        //check if there is missing item in the equipment compared to inventory
        for (ItemClass itemClass : ItemClass.values()) {
            if (equipment.stream().noneMatch(item -> item.getItemClass().equals(itemClass)) &&
                    inventory.stream().anyMatch(item -> item.getItemClass().equals(itemClass))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public ItemPrivate getTheWorstItemFromInvent(Character character, ItemPrivate item) {
        List<ItemPrivate> items = character.getInventory().stream()
                .filter(i -> i.getName().equals(item.getName())).collect(Collectors.toList());
        double minRise = 99.0;
        int indexMin = 99;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getRise() < minRise) {
                minRise = items.get(i).getRise();
                indexMin = i;
            }
        }
        return items.get(indexMin);
    }

    @Override
    public boolean hasItemClassEquipped(Character character, ItemClass itemClass) {
        List<ItemPrivate> equipment = character.getEquipment();
        return equipment.size() == 0 ? false : equipment.stream().anyMatch(item -> (item.getItemClass().equals(itemClass)));
    }

    @Override
    public ItemPrivate getItemFromCollectionByClass(List<ItemPrivate> list, ItemClass itemClass) {
        return list.stream()
                .filter(item -> (item.getItemClass().equals(itemClass)))
                .collect(Collectors
                        .toList()).get(0);
    }

    @Override
    public Character addSkillAndUpdateDB(Character character, SkillClass skillClass) {
        return updatesDB(skillsService.addSkill(character, character, skillClass));
    }

    @Override
    public Character removeSkillAndUpdateDB(Character character, SkillClass skillClass) {
        return updatesDB(skillsService.removeSkill(character, skillClass));
    }

    @Override
    @Transactional
    public Character skillLevelUpdateAndUpdateDB(Character character, SkillClass skillClass, int level) {
        if (!character.getSkills().stream().anyMatch(s -> s.getSkillClass() == skillClass)) {
            throw new IllegalArgumentException("You don't have this skill");
        }
        if (level < 0 || level > Constants.SKILL_MAX_LEVEL) {
            throw new IllegalArgumentException("Skill's level must be between 0 and 20");
        }
        Skill skill = character.getSkills().stream().filter(s -> s.getSkillClass() == skillClass).findFirst().get();

        if (calculateTotalSkillPointsUsed(character) + (level - skill.getLevel()) > character.getLevel()) {
            throw new IllegalArgumentException("No available skill points");
        }
        character = skillsService.skillLevelUpdate(character, skill, level);
        if (skill.getType() == SkillType.Passive) {
            character = updateFinalStatsAndDB(character);
            return updatesDB(character);
        } else {
            return character;
        }
    }

    @Override
    public int calculateTotalSkillPointsUsed(Character character) {
        int totalSkillPointsUsed = 0;
        for (Skill skill : character.getSkills()) {
            totalSkillPointsUsed += skill.getLevel();
        }
        return totalSkillPointsUsed;
    }

    @Override
    public int getMaxId() {
        return characterRepository.count() == 0 ? 0 : characterRepository.findFirstByOrderByIdDesc().getId();
    }

    @Override
    public void removeMarkedItemsToDeleteFromInventories(List<Integer> itemsIDMarkedForDelete) {
        List<Character> characters = getAll();

        for (Integer itemId : itemsIDMarkedForDelete) {
            boolean removed = false;
            for (Character character : characters) {
                List<ItemPrivate> inventory = character.getInventory();
                for (ItemPrivate item : inventory) {
                    if (item.getId() == itemId) {
                        removeItemFromInventory(character, item);
                        System.out.println("removeItemFromInventory: " + character.getName() + ", itemId: " + itemId);
                        removed = true;
                        break;
                    }
                }
                if (removed) {
                    break;
                }
            }
        }
    }

    @Override
    public boolean hasTheItemNTimesInventory(int N, Character character, String itemName) {
        return character.getInventory().stream()
                .filter(i -> i.getName().equals(itemName))
                .count() >= N;
    }

    @Override
    public boolean hasTheItemNTimesBag(int N, Character character, String itemName) {
        return character.getBag().stream()
                .filter(i -> i.getName().equals(itemName)).count() >= N;
    }

    private boolean hasItemInCollectionById(List<ItemPrivate> collection, int itemId) {
        return collection.stream().anyMatch(item -> (item.getId() == itemId));
    }

    private ItemPrivate getItemFromCollectionById(List<ItemPrivate> list, int itemId) {
        return list.stream()
                .filter(item -> (item.getId() == itemId))
                .collect(Collectors.toList()).get(0);
    }

    private int doHit(Individual f1, Individual f2, int lifeC1, int lifeC2, List<String> fightLog, StringBuilder
            message, Skill activeSkill1, boolean ifCharBlocked) {
        DmgBean dmgBean = new DmgBean(f1.getFinalDamageMin(), f1.getFinalDamageMax(), f1.getFinalMagicDamage());
        int armor2 = f2.getFinalArmor();

        if (ifCharBlocked) {
            if (message.length() != 0) {
                String msg = message.toString();
                message.setLength(0);
                message.append(" :------------------------------ ").append(msg);
            }
            fightLog.add(f2.getType() + ": (" + "life " + (lifeC2 - dmgBean.getDmgMagic()) + "): " + f2.getName() +
                    ": blocked!" + " Magic dmg: " + dmgBean.getDmgMagic() + message);
            return dmgBean.getDmgMagic(); //Magic damage cannot be blocked
        }
        applyCriticalSkill(f1, message, activeSkill1, dmgBean);

        int dmgDone = Utils.getRandomNumberUsingNextInt(dmgBean.getDmgMin(), dmgBean.getDmgMax()) - armor2;
        if (dmgDone < 0) {
            dmgDone = dmgBean.getDmgMagic(); //Magic damage cannot be decreased by armor
        } else {
            dmgDone += dmgBean.getDmgMagic();
        }
        int finalDmgDone = Math.max(dmgDone, 1);
        fightLog.add(f1.getType() + ": (" + "life " + lifeC1 + "): " + f1.getName() + ": hit with: " +
                finalDmgDone + ": " +
                f2.getName() + ": (" + "life " + (lifeC2 - finalDmgDone) + "): " + message);
        return finalDmgDone;
    }

    private List<Creep> filterCreepsByTypeAndLvl(List<Creep> allCreeps, IndividualType type, int lvlFrom,
                                                 int lvlTo) {
        return allCreeps.stream()
                .filter(c -> c.getType() == type)
                .filter(c -> c.getLevel() >= lvlFrom && c.getLevel() <= lvlTo)
                .collect(Collectors.toList());
    }

    private List<Character> filterCharsByLvl(List<Character> characters, int lvlFrom,
                                             int lvlTo) {
        return characters.stream()
                .filter(c -> c.getLevel() >= lvlFrom && c.getLevel() <= lvlTo)
                .collect(Collectors.toList());
    }

    private List<Creep> getCreepByName(List<Creep> creeps, Gods god) {
        return creeps.stream().filter(c -> c.getName().equals(god.name())).collect(Collectors.toList());
    }

    private Character saveState(Individual originalChar) {
        Character copyObject = new Character();
        copyObject.setFinalDamageMin(originalChar.getFinalDamageMin());
        copyObject.setFinalDamageMax(originalChar.getFinalDamageMax());
        copyObject.setFinalMagicDamage(originalChar.getFinalMagicDamage());
        copyObject.setFinalLife(originalChar.getFinalLife());
        copyObject.setFinalArmor(originalChar.getFinalArmor());
        copyObject.setFinalSpeed(originalChar.getFinalSpeed());
        copyObject.setFinalBlockChance(originalChar.getFinalBlockChance());
        return copyObject;
    }

    private Individual restoreCharacter(Individual originalChar, Individual savedState) {
        originalChar.setFinalDamageMin(savedState.getFinalDamageMin());
        originalChar.setFinalDamageMax(savedState.getFinalDamageMax());
        originalChar.setFinalMagicDamage(savedState.getFinalMagicDamage());
        originalChar.setFinalLife(savedState.getFinalLife());
        originalChar.setFinalArmor(savedState.getFinalArmor());
        originalChar.setFinalSpeed(savedState.getFinalSpeed());
        originalChar.setFinalBlockChance(savedState.getFinalBlockChance());
        return originalChar;
    }

    private Character getOneFromAllByName(List<Character> list, String name) {
        if (list.stream().anyMatch(c -> c.getName().equals(name))) {
            return list.stream().filter(c -> c.getName().equals(name)).findFirst().get();
        } else {
            return null;
        }
    }

    private void checkIfSomeoneIsDuelingThisCharAndRemoveIt(int id) {
        Character charForDelete = getOne(id);
        List<Character> allChars = getAll();
        for (Character character : allChars) {
            if (character.getZone() == ZoneClass.Duel && charForDelete.getName().equals(character.getDuel())) {
                character.setDuel(null);
                character.setZone(ZoneClass.MidCreeps);
                characterRepository.saveAndFlush(character);
            }
        }
    }

    private String getAvailabilityCurse(Individual fighter, Character f1) {
        if (fighter.getType() == IndividualType.Character) {
            return f1.getCharacterClass().name();
        } else {
            return fighter.getName();
        }
    }

    private void logMessageWhenEquipped(Character character, ItemPrivate item) {
        logService.logMessage(character.getUserCreator(),
                character.getName() + ": " + "-----Item----- " + item.getName() + " equipped!!");
    }

    private static boolean isJUnitTest() {
        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }
}
