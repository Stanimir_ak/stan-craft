package com.stan.craft.Services;

import com.stan.craft.Models.DropChances;

import java.security.Principal;
import java.util.List;

public interface DropChancesService {
    DropChances create(Principal principal, DropChances dropChances);

    DropChances getOne(int id);

    boolean ifExists(int dropChancesId);

    List<DropChances> getAll();

    DropChances update(Principal principal, DropChances dropChances);

    void delete(Principal principal, int id);
}
