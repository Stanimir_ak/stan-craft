package com.stan.craft.Services;

import com.stan.craft.Models.Character;
import com.stan.craft.Models.User;

import java.security.Principal;
import java.util.List;


public interface UserService {

    User getByName(String username);

    User getById(int id);

    User update(User user);

    User changeCurrentCharacter(User user, Character newCharacter);

    User getPrincipalUser(Principal principal);

    List<User> getAll();

    boolean isAdmin(Principal principal);

    boolean isUserTheCreatorOfChar(Principal principal, Character character);

    boolean isLoggedUser(Principal principal);

    void ifNotLoggedOrAdminThrowInvalid(Principal principal);

    void isAdminOrCharCreatorAndThrow(Principal principal, Character character);

    boolean ifExists(String username);

    List<User> findAll();
}
