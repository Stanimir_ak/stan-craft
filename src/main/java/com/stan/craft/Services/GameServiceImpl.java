package com.stan.craft.Services;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Constants.StaticVar;
import com.stan.craft.Models.Creep;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameServiceImpl implements GameService {
    private final LogService logService;
    private final CharacterService characterService;
    private final FightResultBeanLightService fightResultBeanLightService;
    private final CreepService creepService;
    private final ItemsService itemsService;
    private final ItemPrivateService itemPrivateService;

    @Autowired
    public GameServiceImpl(LogService logService, CharacterService characterService,
                           FightResultBeanLightService fightResultBeanLightService, CreepService creepService, ItemsService itemsService, ItemPrivateService itemPrivateService) {
        this.logService = logService;
        this.characterService = characterService;
        this.fightResultBeanLightService = fightResultBeanLightService;
        this.creepService = creepService;
        this.itemsService = itemsService;
        this.itemPrivateService = itemPrivateService;
    }

    @Override
//    @Transactional
    public void startTimeGenerator() {
//         ***** Stan Clock Generator ******
        if (Constants.ENABLE_FIGHTS) {
            int time1Day = 1;

            while (true) {
                try {
                    Thread.sleep(Constants.MILLISECONDS_BETWEEN_FIGHTS); //The timer! 1000 is 1 sec
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

                if (StaticVar.GameTime % 10 == 0) {
                    System.out.print(StaticVar.GameTime);
                    System.out.print(" ");
                }

                //EVERY DAY
                if (time1Day == Constants.SECONDS_PER_DAY) {
                    StaticVar.Days++;
                    time1Day = 0;
                    StaticVar.AllCreeps = getAllCreeps();
                    StaticVar.AllItems = getAllItems();
                    logService.deleteOldLog();
                    fightResultBeanLightService.deleteOldFightLog();
                    //TODO send chars not played for 5 days to Rest - how to check?
                }

                //EVERY ROUND (3 minutes?)
                if (StaticVar.GameTime == Constants.ROUND_LENGTH_SECONDS) {
                    StaticVar.GameTime = 0;
                    StaticVar.Fights++;
                    System.out.print("Fight! ");

                //THE FIGHT GENERATOR: CALLING THE CHAR SERVICE AND THE FIGHT() methods! :)
                    characterService.getFightsInBattles();
                    List<Integer> itemsIDMarkedForDelete = itemPrivateService.getItemsIDMarkedForDelete();

                    System.out.println("getItemsIDMarkedForDelete");
                    for (Integer itemId : itemsIDMarkedForDelete) {
                        System.out.println(itemId);
                    }
                    characterService.removeMarkedItemsToDeleteFromInventories(itemsIDMarkedForDelete);
                    itemPrivateService.deleteItemsMarkedForDelete();
                    System.out.println("deleteItemsMarkedForDelete");

                    System.out.print("Days: " + StaticVar.Days + "; ");
                    System.out.print("Fights: " + StaticVar.Fights + "; ");
                    System.out.println();
                }
                StaticVar.GameTime++;
                time1Day++;
            }
        }
    }

    @Override
    public void setMaxIndividualId() {
        StaticVar.IndividualId = Math.max(characterService.getMaxId(), creepService.getMaxId()) + 1;
    }

    @Override
    public void deleteOldFightLog() {
        fightResultBeanLightService.deleteOldFightLog();
    }

    @Override
    public void deleteOldLog() {
        logService.deleteOldLog();
    }

    @Override
    public List<Creep> getAllCreeps() {
        return creepService.getAll();
    }

    @Override
    public List<Integer> getAllLevels() {
        return ModelFactory.getAllLevels();
    }

    @Override
    public List<Item> getAllItems() {
        return itemsService.getAll();
    }
}
