package com.stan.craft.Services;

import com.stan.craft.Models.*;
import com.stan.craft.Models.Factories.ModelFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    private final CreepService creepService;
    private final SkillsService skillsService;
    private final UserService userService;
    private final ModelFactory modelFactory;

    @Autowired
    public AdminServiceImpl(CreepService creepService, SkillsService skillsService, UserService userService, ModelFactory modelFactory) {
        this.creepService = creepService;
        this.skillsService = skillsService;
        this.userService = userService;
        this.modelFactory = modelFactory;
    }

    @Override
    @Transactional
    public Creep addSkillToCreep(Creep creep, SkillClass skillClass, int level, Principal principal) {
        if (!userService.isAdmin(principal)) {
            throw new IllegalArgumentException("Admin authority required");
        }
        if (creep.getSkills().stream().anyMatch(s -> s.getSkillClass() == skillClass)) {
            throw new IllegalArgumentException("Creep already have this skill");
        }
        List<Skill> skills = creep.getSkills();
        int totalSkillPointsUsed = 0;
        for (Skill skill : skills) {
            totalSkillPointsUsed += skill.getLevel();
        }
        if (totalSkillPointsUsed + level > creep.getLevel()) {
            throw new IllegalArgumentException("No available skill points");
        }

        Skill skill = ModelFactory.skillsFactoryMethod(skillClass);
        if (skill.getType() == SkillType.Active &&
                creep.getSkills().stream().anyMatch(s -> s.getType() == SkillType.Active)) {
            throw new IllegalArgumentException("You can only use 1 active skill");
        }
        if (skill.getType() == SkillType.Passive) {
            throw new IllegalArgumentException("No point of adding Passive skill to creep - amend it in the DB");
        }
        skill.setLevel(level);
        skill.applySkill(creep); // just to trigger  setValue() of the Skill. We do not change creep because we do not write creep = skill.apply() - check it!!
        skill.setIndividual(creep);

        skillsService.create(skill);
        creep.getSkills().add(skill);
        creepService.update(creep, principal);
        return creep;
        //uprate creep after the addition
    }

    @Override
    @Transactional
    //Not used
    public Creep removeSkillFromCreep(Creep creep, SkillClass skillClass, Principal principal) {
        if (!userService.isAdmin(principal)) {
            throw new IllegalArgumentException("Admin authority required");
        }
        if (creep.getSkills().stream().noneMatch(s -> s.getSkillClass() == skillClass)) {
            throw new IllegalArgumentException("Skill not found");
        }
        Skill skill = null;
        for (int i = 0; i < creep.getSkills().size(); i++) {
            if (creep.getSkills().get(i).getSkillClass() == skillClass) {
                skill = creep.getSkills().get(i);
                break;
            }
        }
        creep.getSkills().remove(skill);
        assert skill != null;
        skillsService.delete(skill.getId());
        creepService.update(creep, principal);
        return creep;
    }

    @Override
    @Transactional
    public List<Creep> addSkillsToGods(Principal principal) {
        List<Creep> allGods = creepService.getAllGods();

        for (Creep god : allGods) {
            switch (Gods.valueOf(god.getName())) {
                case Ares:
                    addSkillToCreep(god, SkillClass.MultiHit, 10, principal);
                    break;
                case Thanatos:
                    addSkillToCreep(god, SkillClass.Critical, 10, principal);
                    break;
                case Achlys:
                    addSkillToCreep(god, SkillClass.Shock, 10, principal);
                    break;
                case Erebus:
                    addSkillToCreep(god, SkillClass.Dynamite, 10, principal);
                    break;
                case Chaos:
                    addSkillToCreep(god, SkillClass.Dynamite, 15, principal);
                    break;
                case Hades:
                    addSkillToCreep(god, SkillClass.MultiHit, 15, principal);
                    break;
                case Tartarus:
                    addSkillToCreep(god, SkillClass.Critical, 20, principal);
                    break;
            }
        }
        return allGods;
    }
}
