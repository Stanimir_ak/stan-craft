package com.stan.craft.Services;

import com.stan.craft.Models.Character;
import com.stan.craft.Models.DropChances;
import com.stan.craft.Models.Item;
import com.stan.craft.Models.ItemClass;
import com.stan.craft.Models.ItemPrivate;

import java.security.Principal;
import java.util.List;

public interface ItemsService {

    List<Item> getAll();

    List<Item> getItemsAccordingToLevelFromDB(int level);

    List<Item> getItemsAccordingToLevelFromRAM(int level);

    Item getOne(int itemId);

    Item getOneByLevelAndClass(int level, ItemClass itemClass);

    Item getOneByName(String name);

    Item create(Item item, Principal principal);

    Item update(Item item, Principal principal);

    void delete(int itemId, Principal principal);

    boolean ifExists(int itemId);

//    ItemPrivate dropItemFromCharacter(Character looser, Character winner);
//
//    ItemPrivate dropItemFromCreep(Creep creep, Character character);

    ItemPrivate dropItemFromLooser(Character winner, DropChances dropChancesLooser);

    ItemPrivate dropItemFromDropChances(Character winner, DropChances dropChances, boolean hasCrystalRingWithGold);

    ItemPrivate createItemPrivateFromBaseItem(Item item);
}
