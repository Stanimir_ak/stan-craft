package com.stan.craft.Services;

import com.stan.craft.Models.DropChances;
import com.stan.craft.Repositories.DropChancesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;

@Service
public class DropChancesServiceImpl implements DropChancesService {

    private final DropChancesRepository dropChancesRepository;
    private final UserService userService;

    @Autowired
    public DropChancesServiceImpl(DropChancesRepository dropChancesRepository, UserService userService) {
        this.dropChancesRepository = dropChancesRepository;
        this.userService = userService;
    }

    @Override
    public DropChances create(Principal principal, DropChances dropChances) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
        return dropChancesRepository.saveAndFlush(dropChances);
    }

    @Override
    public DropChances getOne(int id) {
        return dropChancesRepository.getOne(id);
    }

    @Override
    public boolean ifExists(int dropChancesId) {
        return dropChancesRepository.existsById(dropChancesId);
    }

    @Override
    public List<DropChances> getAll() {
        return dropChancesRepository.findAll();
    }

    @Override
    @Transactional
    public DropChances update(Principal principal, DropChances dropChances) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
        return dropChancesRepository.saveAndFlush(dropChances);
    }

    @Override
    @Transactional
    public void delete(Principal principal, int id) {
        userService.ifNotLoggedOrAdminThrowInvalid(principal);
         dropChancesRepository.deleteById(id);
    }
}
