package com.stan.craft.Services;

import com.stan.craft.Models.Beans.FightResultBeanLight;
import com.stan.craft.Models.Page;
import org.springframework.data.domain.Slice;

import java.security.Principal;
import java.util.List;

public interface FightResultBeanLightService {

    List<String> getOneFightLog(long fightLogId);
    List<String> getOneFightLog2(long fightLogId);

    Slice<FightResultBeanLight> getFightLogByCharacter(Principal principal, Page page);

    FightResultBeanLight logMessage(FightResultBeanLight fightResultBeanLight);

    void deleteOldFightLog();
}
