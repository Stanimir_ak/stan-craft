package com.stan.craft.Services;

import com.stan.craft.Exceptions.InvalidOperationException;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.User;
import com.stan.craft.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getByName(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getById(int id) {
        return userRepository.getOne(id);
    }

    @Override
    public User update(User user) {
        userRepository.saveAndFlush(user);
        return user;
    }

    @Override
    public User changeCurrentCharacter(User user, Character newCharacter) {
        user.setCurrentCharacter(newCharacter);
        return update(user);
    }

    @Override
    public User getPrincipalUser(Principal principal) {
        return getByName(principal.getName());
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public boolean isAdmin(Principal principal) {
        return getPrincipalUser(principal).getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
    }

    @Override
    public boolean isUserTheCreatorOfChar(Principal principal, Character character) {
        return getPrincipalUser(principal).getCharacters().stream()
                .anyMatch(c -> c.getName().equals(character.getName()));
    }

    @Override
    public boolean isLoggedUser(Principal principal) {
        return principal != null;
    }

    @Override
    public void ifNotLoggedOrAdminThrowInvalid(Principal principal) {
        if (!isLoggedUser(principal) || !isAdmin(principal)) {
            throw new InvalidOperationException("Not authorized");
        }
    }

    @Override
    public void isAdminOrCharCreatorAndThrow(Principal principal, Character character) {
        if (!(isAdmin(principal) || isUserTheCreatorOfChar(principal, character))) {
            throw new InvalidOperationException("Not authorized");
        }
    }

    @Override
    public boolean ifExists(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }


}
