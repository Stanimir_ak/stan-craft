package com.stan.craft.Services;

import com.stan.craft.Models.*;
import com.stan.craft.Models.Beans.FightResultBean;
import com.stan.craft.Models.Character;

import java.security.Principal;
import java.util.List;

public interface CharacterService {

    List<Character> getAll();

    List<Character> getAllSortedByExp();

    Character getOne(int id);

    Character getOneByName(String name);

    Character create(Principal principal, Character character);

    Character updateEquipStats(Character character);

    Character updateFinalStatsAndDB(Character character);

    Character userUpdatesDB(Character character, Principal principal);

    Character updatesDB(Character character);

    void delete(Principal principal, int id);

    boolean ifExistsByName(int id);

    void checkIfExistsAndThrow(int id);

    void checkIfExistsAndThrow(String charName);

    boolean ifExistsByName(String characterName);

    void buy(ItemPrivate item);

    Character useItemIfBetterOrStashIt(Character character, ItemPrivate itemNew);

    Character useItemFromOwnInventory(Character character, int id, int itemId);

    Character transformRing(Character character, int ringId, int crystalId);

    Character useItemFromOwnBag(Character character, int id, int itemId);

    void removeItemFromEquipment(Principal principal, int id, int itemId);

    void removeItemFromInventory(Character character, ItemPrivate item);

    List<Character> giveItemToOtherCharacter(int id, String name, int itemId);

//    Character calculateEquipStats(Character character);

    Character updateStatsFromBaseStats(Character character);

    FightResultBean fight(Individual fighter1, Individual fighter2, Character f1, Character f2);

    List<String> getFightsInBattles();

    boolean nameIsFree(String characterName);

    int calculateLevel(Character character);

    List<Character> getFighters();

    Character changeZone(Character character, ZoneClass newZone, String characterDuel);

    Character getCurrentCharacterOfUser(Principal principal);

    boolean isBestEquipped(Principal principal);

    ItemPrivate getTheWorstItemFromInvent(Character character, ItemPrivate item);

    boolean hasItemClassEquipped(Character character, ItemClass itemClass);

    ItemPrivate getItemFromCollectionByClass(List<ItemPrivate> list, ItemClass itemClass);

    Character addSkillAndUpdateDB(Character character, SkillClass skillClass);

    Character removeSkillAndUpdateDB(Character character, SkillClass skillClass);

    Character skillLevelUpdateAndUpdateDB(Character character, SkillClass skillClass, int level);

    boolean hasTheItemNTimesInventory(int N, Character character, String itemName);

    boolean hasTheItemNTimesBag(int N, Character character, String itemName);

//    void skillLevelUpdateAndApplyNotPassive(Character character, SkillClass skillClass, int level);

    int calculateTotalSkillPointsUsed(Character character);

    int getMaxId();

    void removeMarkedItemsToDeleteFromInventories(List<Integer> itemsIDMarkedForDelete);
}
