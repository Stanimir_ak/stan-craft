package com.stan.craft.Services;

import com.stan.craft.Models.Creep;
import com.stan.craft.Models.SkillClass;

import java.security.Principal;
import java.util.List;

public interface AdminService {
    Creep addSkillToCreep(Creep creep, SkillClass skillClass, int level, Principal principal);

    Creep removeSkillFromCreep(Creep creep, SkillClass skillClass, Principal principal);

    List<Creep> addSkillsToGods(Principal principal);
}
