package com.stan.craft.Services;

import com.stan.craft.Models.Character;
import com.stan.craft.Models.Creep;
import com.stan.craft.Models.Item;

import java.security.Principal;
import java.util.List;

public interface CreepService {

    Creep getOne(int id);

    List<Creep> getAll();

    List<Creep> getCreepsByLevel(int levelFrom, int levelTo);

    List<Creep> getBossesByLevel(int levelFrom, int levelTo);

    int getGamesNumberOfCreeps();

    List<Creep> getAllNormalCreeps();

    List<Creep> getAllBosses();

    List<Creep> getAllGods();

    Creep create(Creep creep, Principal principal);

    Creep update(Creep creep, Principal principal);

    void delete(int id, Principal principal);

    List<Item> dropItems(Creep creep, Character character);

    int getMaxId();
}
