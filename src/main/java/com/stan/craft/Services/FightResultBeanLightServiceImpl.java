package com.stan.craft.Services;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Beans.FightResultBeanLight;
import com.stan.craft.Models.Page;
import com.stan.craft.Models.User;
import com.stan.craft.Repositories.FightResultBeanLightDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FightResultBeanLightServiceImpl implements FightResultBeanLightService {

    private final FightResultBeanLightDao fightResultBeanLightDao;
    private final UserService userService;

    @Autowired
    public FightResultBeanLightServiceImpl(FightResultBeanLightDao fightResultBeanLightDao, UserService userService) {
        this.fightResultBeanLightDao = fightResultBeanLightDao;
        this.userService = userService;
    }

    @Override
    @Transactional
    public List<String> getOneFightLog(long fightLogId) {
        String log = fightResultBeanLightDao.getOne(fightLogId).getFightLog();
        String[] array = log.split(Constants.LOG_SEPARATOR);
        if (array.length <= Constants.LOGS_PER_PAGE + 1) {
            //Deprecated
            //IT IS ALWAYS <= 31, the logic is in ModelFactory - concatenateStringsWithLimit()
            return Arrays.asList(array);
        } else {
            String[] array2 = new String[array.length + 1];
            for (int i = 0; i < Constants.FIRST_LINES_FIGHT_LOG; i++) {
                array2[i] = array[i];
            }
            array2[Constants.FIRST_LINES_FIGHT_LOG] = ".......skipping some moves....................";

            for (int i = Constants.FIRST_LINES_FIGHT_LOG + 1; i < array2.length; i++) {
                array2[i] = array[i - 1];
            }
            return Arrays.asList(array2);
        }
    }

    @Override
    public List<String> getOneFightLog2(long fightLogId) {
        //new method for creating moves
        //on first move I set who is the player 1 and who player 2 - left & right
        //on the rest moves I just set the sword direction and the dmg - the life should go together with the name & type of both players

        FightResultBeanLight fightResultBeanLight = fightResultBeanLightDao.getOne(fightLogId);
        String log = fightResultBeanLight.getFightLog();
        String[] array = log.split(Constants.LOG_SEPARATOR);
        List<String> list = new ArrayList<>();

        String lifeF1 = "";
        String lifeF2 = "";
        boolean block;
        int action;
        String damage;

        for (int i = 0; i <= array.length; i++) {
            String fighter1 = fightResultBeanLight.getWinner();
            String fighter2 = fightResultBeanLight.getLooser();
            String message = "";
            String[] tempArr = array[i].split(":");
            if (tempArr.length == 1) {
                //fixing problem with diff length of creeps/bosses/gods
                String lastRowWithWinner = tempArr[0].replace(" ","-");
                list.add(lastRowWithWinner);
                if (tempArr[0].contains("WIN")) {
                    break;
                } else {
                    continue;
                }
            }
            String actor = tempArr[2];
            if (tempArr[tempArr.length - 1].contains("##")) {
                message = tempArr[tempArr.length - 1];
            }

            if (tempArr[3].contains("blocked")) {
                block = true;
                damage = tempArr[4];
                //fighter1 is the actor
                if (actor.contains(fighter1)) {
                    lifeF1 = tempArr[1];
                    action = 2;
                } else {
                    lifeF2 = tempArr[1];
                    action = 1;
                }
                //fixing problem with diff length of creeps/bosses/gods
                fighter1 = fighter1.replace(" ","-");
                fighter2 = fighter2.replace(" ","-");

                list.add(paintWithStrings(fighter1, fighter2, lifeF1, lifeF2, damage, action, block, i + 1, message));
            } else {
                block = false;
                damage = tempArr[4];
                //fighter1 is the actor
                if (actor.contains(fighter1)) {
                    lifeF1 = tempArr[1];
                    lifeF2 = tempArr[6];
                    action = 1;
                } else {
                    action = 2;
                    lifeF1 = tempArr[6];
                    lifeF2 = tempArr[1];
                }
                //fixing problem with diff length of creeps/bosses/gods
                fighter1 = fighter1.replace(" ","-");
                fighter2 = fighter2.replace(" ","-");

                list.add(paintWithStrings(fighter1, fighter2, lifeF1, lifeF2, damage, action, block, i + 1, message));
            }
        }
        return list;
    }

    private String paintWithStrings(String f1, String f2, String lifeF1, String lifeF2, String dmg, int action, boolean block,
                                    int index, String message) {
        String dogs = " " +
                "                                                                                  <br />" +
                "                                 __                                               <br />" +
                "                             ___/O'\'_                                            <br />" +
                "                            '\'_____ '\'                                          <br />" +
                "                                 /    '\'                                         <br />" +
                "                         __     / /'\' '\'                                        <br />" +
                "                     ___/O'\'__/ /__'\' '\'_/                                     <br />" +
                "                    '\'_____'_____   '\'  |                                       <br />" +
                "                           ||      '\''\' |                                       <br />" +
                "                         __||      __|  |_|                                       ";


        String painting = "" +
                index + ":....................................................." + message + "<br />" +
                "..................................................................................<br />" +
                ".............................." + isBlocking(block) + ".....................................<br />" +
                "....................................." + dmg + ".......................................<br />" +
                "............................." + swordDirection(action) + "............................<br />" +
                "..................................................................................<br />" +
                f1 + lifeF1 + " ....................................." + f2 + lifeF2;

        return painting;
    }

    private String swordDirection(int action) {
        String swordRight = " ||=======>> ";
        String swordLeft = " <<=======|| ";
        if (action == 1) {
            return swordRight;
        } else {
            return swordLeft;
        }
    }

    private String isBlocking(boolean block) {
        if (block) {
            return " BLOCK!";
        } else {
            return "";
        }
    }

    @Override
    public Slice<FightResultBeanLight> getFightLogByCharacter(Principal principal, Page page) {
//        page.setIndex(page.getIndex() - 1);
        User user = userService.getPrincipalUser(principal);
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.FIGHT_LOGS_PER_PAGE, Sort.by("id").descending());
        return fightResultBeanLightDao.getFightLogByCharacter(pageable, user.getCurrentCharacter().getName());
    }

    @Override
    @Transactional
    public FightResultBeanLight logMessage(FightResultBeanLight fightResultBeanLight) {
        return fightResultBeanLightDao.saveAndFlush(fightResultBeanLight);
    }

    @Override
    @Transactional
    public void deleteOldFightLog() {
        fightResultBeanLightDao.deleteOldFightLog(Constants.MAX_FIGHT_LOG_COUNT);
    }
}
