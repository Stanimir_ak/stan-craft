package com.stan.craft.Services;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Beans.LogBean;
import com.stan.craft.Models.Page;
import com.stan.craft.Models.User;
import com.stan.craft.Repositories.LogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    private final LogDao logDao;
    private final UserService userService;

    @Autowired
    public LogServiceImpl(LogDao logDao, UserService userService) {
        this.logDao = logDao;
        this.userService = userService;
    }

    @Override
    public Slice<LogBean> getLogByUsername(Principal principal, Page page) {
//        page.setIndex(page.getIndex() - 1);
        User user = userService.getPrincipalUser(principal);
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.LOGS_PER_PAGE, Sort.by("id").descending());
        return logDao.getLogByUsername(pageable, user.getUsername());
    }

    @Override
    @Transactional
    public LogBean logMessage(String username, String message) {
        LogBean logBeanNew = new LogBean();
        logBeanNew.setUsername(username);
        logBeanNew.setMessage(message);
        return logDao.saveAndFlush(logBeanNew);
    }

    @Override
    @Transactional
    public void chat(String sender, String receiver, String message) {
        if (userService.ifExists(sender) && userService.ifExists(receiver) && !message.isEmpty()) {
            saveChat(sender, receiver, "<<" + sender + ">> " + message);
            logMessage(sender, message + "(to " + receiver + ")");
        }
        //else - > we can add here exception for message for the user or empty message
    }

    @Override
    @Transactional
    public LogBean saveChat(String sender, String receiver, String message) {
        LogBean logBeanNew = new LogBean();
        logBeanNew.setSender(sender);
        logBeanNew.setUsername(receiver);
        logBeanNew.setMessage(message);
        return logDao.saveAndFlush(logBeanNew);
    }

    @Override
    @Transactional
    public void deleteOldLog() {
        logDao.deleteOldLog(Constants.MAX_MAIN_LOG_COUNT);
    }

    @Override
    @Transactional
    public void notifyAllUsers(String message) {
        List<User> allUsers = userService.getAll();
        for (User allUser : allUsers) {
            logMessage(allUser.getUsername(), message);
        }
    }
}
