package com.stan.craft.Services;

import com.stan.craft.Models.ItemPrivate;

import java.util.List;

public interface ItemPrivateService {

    List<ItemPrivate> getAll();

    ItemPrivate getOne(int itemId);

    ItemPrivate create(ItemPrivate item);

    ItemPrivate update(ItemPrivate item);

    void delete(int itemId);

    void forceDelete(int id);

    void markForDelete(ItemPrivate item);

    void deleteItemsMarkedForDelete();

    List<Integer> getItemsIDMarkedForDelete();

    boolean ifExists(int itemId);
}
