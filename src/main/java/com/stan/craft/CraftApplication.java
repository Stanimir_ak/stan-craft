package com.stan.craft;

import com.stan.craft.Constants.StaticVar;
import com.stan.craft.Services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class CraftApplication {
    private static GameService gameService;


    @Autowired
    public CraftApplication(GameService gameService) {
        CraftApplication.gameService = gameService;
    }

    public static void main(String[] args) {
        SpringApplication.run(CraftApplication.class, args);
        //set next individual id
//        gameService.setMaxIndividualId();
        //load all creeps in RAM
        StaticVar.AllCreeps = gameService.getAllCreeps();
        //load all levels in RAM
        StaticVar.AllLevels = gameService.getAllLevels();
        //load all Items in RAM
        StaticVar.AllItems = gameService.getAllItems();
        gameService.deleteOldLog();
        gameService.deleteOldFightLog();
        gameService.startTimeGenerator();
    }
}
