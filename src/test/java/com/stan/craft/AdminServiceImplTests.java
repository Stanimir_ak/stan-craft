package com.stan.craft;

import com.stan.craft.Models.*;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Services.AdminServiceImpl;
import com.stan.craft.Services.CreepService;
import com.stan.craft.Services.SkillsService;
import com.stan.craft.Services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceImplTests {
    @InjectMocks
    AdminServiceImpl mockAdminService;

    @Mock
    CreepService creepService;
    @Mock
    SkillsService skillsService;
    @Mock
    UserService userService;
    @Mock
    ModelFactory modelFactory;

    @Test
    public void addSkillsToGodsTest() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Thanatos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Thanatos.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Achlys = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Achlys.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Erebus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Erebus.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Chaos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Chaos.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Hades = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Hades.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Tartarus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Tartarus.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);


        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);
        allGods.add(Thanatos);
        allGods.add(Achlys);
        allGods.add(Erebus);
        allGods.add(Chaos);
        allGods.add(Hades);
        allGods.add(Tartarus);

        Mockito.when(creepService.getAllGods()).thenReturn(allGods);
        Mockito.when(userService.isAdmin(principal)).thenReturn(true);

        //act
        List<Creep> resultGods = mockAdminService.addSkillsToGods(principal);

        //assert
        //Ares
        assertEquals(1, resultGods.get(0).getSkills().size());
        assertEquals(SkillClass.MultiHit, resultGods.get(0).getSkills().get(0).getSkillClass());
        assertEquals(10, resultGods.get(0).getSkills().get(0).getLevel());

        //Tartarus
        assertEquals(1, resultGods.get(6).getSkills().size());
        assertEquals(SkillClass.Critical, resultGods.get(6).getSkills().get(0).getSkillClass());
        assertEquals(20, resultGods.get(6).getSkills().get(0).getLevel());
    }

    @Test
    public void addSkillsToGods_ShouldThrowIfNotAdmin() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);

        Mockito.when(creepService.getAllGods()).thenReturn(allGods);
        Mockito.when(userService.isAdmin(principal)).thenReturn(false);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockAdminService.addSkillsToGods(principal));
    }

    @Test
    public void addSkillsToGods_ShouldThrowIfExists() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Ares.getSkills().add(ModelFactory.skillsFactoryMethod(SkillClass.MultiHit));
        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);

        Mockito.when(creepService.getAllGods()).thenReturn(allGods);
        Mockito.when(userService.isAdmin(principal)).thenReturn(true);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockAdminService.addSkillsToGods(principal));
    }

    @Test
    public void addSkillsToGods_ShouldThrowIfNoPoints() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Skill armless = ModelFactory.skillsFactoryMethod(SkillClass.Armless);
        armless.setLevel(5);
        armless.applySkill(new Character());
        Ares.getSkills().add(armless);
        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);

        Mockito.when(creepService.getAllGods()).thenReturn(allGods);
        Mockito.when(userService.isAdmin(principal)).thenReturn(true);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockAdminService.addSkillsToGods(principal));
    }

    @Test
    public void addSkillsToGods_ShouldThrowIfAlredyHaveIt() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 50, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        critical.setLevel(1);
        critical.applySkill(new Character());
        Ares.getSkills().add(critical);
        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);

        Mockito.when(creepService.getAllGods()).thenReturn(allGods);
        Mockito.when(userService.isAdmin(principal)).thenReturn(true);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockAdminService.addSkillsToGods(principal));
    }

    @Test
    public void removeSkillFromCreep_Throw_IfNotAdmin() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 50, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        critical.setLevel(1);
        critical.applySkill(new Character());
        Ares.getSkills().add(critical);
        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);

        Mockito.when(userService.isAdmin(principal)).thenReturn(false);


        //assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockAdminService.removeSkillFromCreep(Ares, SkillClass.Power, principal));
    }

    @Test
    public void removeSkillFromCreep_Throw_IfNotFound() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 50, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        critical.setLevel(1);
        critical.applySkill(new Character());
        Ares.getSkills().add(critical);
        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);

        Mockito.when(userService.isAdmin(principal)).thenReturn(true);


        //assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockAdminService.removeSkillFromCreep(Ares, SkillClass.Power, principal));
    }

    @Test
    public void removeSkillFromCreep_ShouldRemove() {
        //arrenge
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 50, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        critical.setLevel(1);
        critical.applySkill(new Character());
        Ares.getSkills().add(critical);
        List<Creep> allGods = new ArrayList<>();
        allGods.add(Ares);

        Mockito.when(userService.isAdmin(principal)).thenReturn(true);


        //act
        Ares =  mockAdminService.removeSkillFromCreep(Ares, SkillClass.Critical, principal);

        //assert
        assertEquals(0, Ares.getSkills().size());
    }
}
