package com.stan.craft;

import com.stan.craft.Utils.Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class UtilsTests {

    @Test
    public void getRandomNumberUsingNextIntShouldBe98PerCentCorrect() {
        int sum = 0;
        int numberOfTests = 10000;
        int maxNumber = 100;

        for (int i = 0; i < numberOfTests; i++) {
            sum += Utils.getRandomNumberUsingNextInt(1, maxNumber);
        }

        int min = (int) ((maxNumber / 2) * 0.97);
        int max = (int) ((maxNumber / 2) * 1.04);

        System.out.println("getRandomNumberUsingNextIntShouldBe98PerCentCorrect: ");
        System.out.println("min: " + min);
        System.out.println("Avr result: " + sum / numberOfTests);
        System.out.println("max: " + max);

        assertTrue(sum / numberOfTests >= min);
        assertTrue(sum / numberOfTests <= max);
    }

    @Test
    public void getRandomNumberUsingNextDoubleShouldBe98PerCentCorrect() {
        double sum = 0.0;
        int numberOfTests = 10000;
        double maxNumber = 1.0;

        for (int i = 0; i < numberOfTests; i++) {
            sum += Utils.getRandomNumberUsingNextDouble(0.0, maxNumber);
        }

        double min = Utils.round((maxNumber / 2) * 0.97, 2);
        double max = Utils.round((maxNumber / 2) * 1.04, 2);

        System.out.println("getRandomNumberUsingNextDoubleShouldBe98PerCentCorrect: ");
        System.out.println("min: " + min);
        System.out.println("Avr result: " + sum / numberOfTests);
        System.out.println("max: " + max);

        assertTrue(Utils.round(sum / numberOfTests, 2) >= min);
        assertTrue(Utils.round(sum / numberOfTests, 2) <= max);
    }

    @Test
    public void randomChanceShouldBe98PerCentCorrect() {
        int count = 0;
        int numberOfTests = 10000;
        double chancePerCent = 0.20;

        for (int i = 0; i < numberOfTests; i++) {
            if (Utils.randomChance(chancePerCent)) {
                count++;
            }
        }

        double min = Utils.round((chancePerCent) * 0.97, 2);
        double max = Utils.round((chancePerCent) * 1.04, 2);

        System.out.println("randomChanceShouldBe98PerCentCorrect: ");
        System.out.println("min: " + min);
        System.out.println("Avr result: " + Utils.round(count / (double) numberOfTests, 2));
        System.out.println("max: " + max);

        assertTrue(Utils.round(count / (double) numberOfTests, 2) >= min);
        assertTrue(Utils.round(count / (double) numberOfTests, 2) <= max);
    }
}
