package com.stan.craft;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Beans.FightResultBean;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.*;
import com.stan.craft.Models.DTO.CharacterDTO;
import com.stan.craft.Models.DTO.CreepDTO;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Repositories.CharacterRepository;
import com.stan.craft.Repositories.CreepRepository;
import com.stan.craft.Services.*;
import com.stan.craft.Utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class CharacterServiceImplTests {
    @InjectMocks
    CharacterServiceImpl mockCharacterServiceImpl;

    @Mock
    CharacterRepository characterRepository;
    @Mock
    LogService logService;
    @Mock
    ItemPrivateService itemPrivateService;
    @Mock
    SkillsService skillsService;
    @Mock
    UserService userService;
    @Mock
    ModelFactory modelFactory;
    @Mock
    ItemsService itemsService;
    @Mock
    FightResultBeanLightService fightResultBeanLightService;
    @Mock
    CreepRepository creepRepository;
    @Mock
    CreepService creepService;

    @Test
    public void betterItemShouldBeUsedIfItemSwordNotUniqueLvlOk() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword2Name", 10, 20, 0.1, 1.5, 10);

        character.getEquipment().add(sword1);
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword2);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword2);
    }

    @Test
    public void betterItemShouldBeUsedIfItemSwordNotUniqueLvlOkNoEquipment() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);

        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword1);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword1);
    }

    @Test
    public void betterItemShouldNotUsedIfNoBagItemRing() {
        //arrange
        Character character = Factory.createCharWithOUTBag("Pol", CharacterClass.Knight, 15);
        ItemPrivate ring = Factory.createItemPrivate(0, 1, ItemClass.RING, "RingName", 0, 0, 0.0, 0.0, 1);

        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, ring);

        //assert
        assertFalse(character.getEquipment().stream().anyMatch(i -> i.getItemClass() == ItemClass.BAG));
        assertEquals(0, character.getBag().size());
    }

    @Test
    public void betterItemShouldUseIfHasBagItemRing() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 5, ZoneClass.LowCreeps);
        ItemPrivate ring = Factory.createItemPrivate(0, 1, ItemClass.RING, "RingName", 0, 0, 0.0, 0.0, 1);

        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, ring);

        //assert
        assertEquals(character.getEquipment().stream().anyMatch(i -> i.getItemClass() == ItemClass.BAG), true);
        assertEquals(1, character.getBag().size());
    }

    @Test
    public void betterItemShouldNotUseIfHasBagItemRingHas3Rings() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 5, ZoneClass.LowCreeps);
        ItemPrivate ring = Factory.createItemPrivate(0, 1, ItemClass.RING, "RingName", 0, 0, 0.0, 0.0, 1);
        ItemPrivate ring2 = Factory.createItemPrivate(0, 2, ItemClass.RING, "RingName", 0, 0, 0.0, 0.0, 1);
        ItemPrivate ring3 = Factory.createItemPrivate(0, 3, ItemClass.RING, "RingName", 0, 0, 0.0, 0.0, 1);
        ItemPrivate ring4 = Factory.createItemPrivate(0, 4, ItemClass.RING, "RingName", 0, 0, 0.0, 0.0, 1);

        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, ring);
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, ring2);
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, ring3);
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, ring4);

        //assert
        assertEquals(character.getEquipment().stream().anyMatch(i -> i.getItemClass() == ItemClass.BAG), true);
        assertEquals(3, character.getBag().size());
    }

    @Test
    public void betterItemShouldUseIfHasBagItemCrystal() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal = Factory.createItemPrivate(0, 1, ItemClass.CRYSTAL, "CrystalName", 0, 0, 0.0, 0.0, 1);

        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, crystal);

        //assert
        assertEquals(character.getEquipment().stream().anyMatch(i -> i.getItemClass() == ItemClass.BAG), true);
        assertEquals(1, character.getBag().size());
    }

    @Test
    public void betterItemShouldNotUseIfHasBagItemCrystalBagFull() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 1, ZoneClass.LowCreeps);
        ItemPrivate crystal = Factory.createItemPrivate(0, 1, ItemClass.CRYSTAL, "CrystalName", 0, 0, 0.0, 0.0, 1);
        ItemPrivate crystal2 = Factory.createItemPrivate(0, 2, ItemClass.CRYSTAL, "CrystalName2", 0, 0, 0.0, 0.0, 1);

        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, crystal);
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, crystal2);

        //assert
        assertEquals(character.getEquipment().stream().anyMatch(i -> i.getItemClass() == ItemClass.BAG), true);
        assertEquals(1, character.getBag().size());
    }

    @Test
    public void betterItemShouldPutInventoryIfLvlItemBigger() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 1, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 20);

        assertEquals(0, character.getInventory().size());
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword1);

        //assert
        assertEquals(character.getEquipment().stream().anyMatch(i -> i.getItemClass() == ItemClass.BAG), true);
        assertEquals(1, character.getInventory().size());
    }

    @Test
    public void betterItemShouldPutInventoryIfLvlItemBiggerAndUnique() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 15, 1, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(1, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 20);

        assertEquals(0, character.getInventory().size());
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword1);

        //assert
        assertEquals(character.getEquipment().stream().anyMatch(i -> i.getItemClass() == ItemClass.BAG), true);
        assertEquals(1, character.getInventory().size());
    }

    @Test
    public void betterItemShouldBeUsedIfBothUniqueItemSwordLvlOkNewBiggerLvl() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 20, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(1, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword2 = Factory.createItemPrivate(1, 2, ItemClass.WEAPON, "Sword2Name", 10, 20, 0.1, 1.1, 15);

        character.getEquipment().add(sword1);
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword2);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword2);
        assertEquals(character.getInventory().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get().getId(), sword1.getId());
    }

    @Test
    public void betterItemShouldBeUsedIfBothUniqueItemSwordLvlOkNewSameOrLowerLvl() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 20, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(1, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword2 = Factory.createItemPrivate(1, 2, ItemClass.WEAPON, "Sword2Name", 10, 20, 0.1, 1.1, 10);

        character.getEquipment().add(sword1);
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword2);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword1);
        assertEquals(character.getInventory().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get().getId(), sword2.getId());
    }

    @Test
    public void betterItemShouldBeUsedIfNewUniqueItemSwordLvlOk() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 20, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword2 = Factory.createItemPrivate(1, 2, ItemClass.WEAPON, "Sword2Name", 10, 20, 0.1, 1.1, 10);

        character.getEquipment().add(sword1);
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword2);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword2);
        assertEquals(character.getInventory().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get().getId(), sword1.getId());
    }

    @Test
    public void betterItemShouldBeUsedIfUsedUniqueNewNotItemSwordLvlOk() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 20, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(1, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword2Name", 10, 20, 0.1, 1.5, 10);

        character.getEquipment().add(sword1);
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword2);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword1);
        assertEquals(character.getInventory().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get().getId(), sword2.getId());
    }

    @Test
    public void betterItemShouldBeUsedIfUsedNotUniqueButBiggerLvlNewUniqueItemSwordLvlOk() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 20, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(1, 2, ItemClass.WEAPON, "Sword2Name", 10, 20, 0.1, 1.5, 10);

        character.getEquipment().add(sword1);
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword2);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword1);
        assertEquals(character.getInventory().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get().getId(), sword2.getId());
    }

    @Test
    public void betterItemShouldBeUsedIfUsedBiggerLvlItemSwordLvlOkHas2InInvent() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 20, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.5, 10);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword3 = Factory.createItemPrivate(0, 3, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.2, 10);
        ItemPrivate sword4 = Factory.createItemPrivate(0, 4, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.3, 10);

        character.getEquipment().add(sword1);
        character.getInventory().add(sword2);
        character.getInventory().add(sword3);
        //act
        character = mockCharacterServiceImpl.useItemIfBetterOrStashIt(character, sword4);

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(), sword1);
        //remove the worst item
        List<ItemPrivate> inventory = character.getInventory();
        assertEquals(2, character.getInventory().stream().filter(i -> i.getName().equals("Sword1Name")).count());
        assertEquals(0, inventory.stream().filter(i -> i.getId() == sword2.getId()).count());
    }

    @Test
    public void useItemFromOwnInventoryItemsDifferent() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword3 = Factory.createItemPrivate(0, 3, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.2, 10);
        ItemPrivate sword4 = Factory.createItemPrivate(0, 4, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.3, 10);

        character.getEquipment().add(sword1);
        character.getInventory().add(sword2);
        character.getInventory().add(sword3);
        character.getInventory().add(sword4);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        //skills to not work here
        int finalDamageMax = character.getFinalDamageMax();

//        Mockito.when(characterRepository.saveAndFlush(skillsService.applySkills(character, character))).thenReturn(character);
        Mockito.when(skillsService.applySkills(character, character)).thenReturn(character);

        //act
        character = mockCharacterServiceImpl.useItemFromOwnInventory(character, character.getId(), sword4.getId());

        int finalDamageMaxEnd = character.getFinalDamageMax();

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(),
                sword4);
        //remove the worst item
        List<ItemPrivate> inventory = character.getInventory();
        assertEquals(3, inventory.size());
        assertEquals(2, character.getInventory().stream().filter(i -> i.getName().equals("Sword1Name")).count());
        assertEquals(0, inventory.stream().filter(i -> i.getId() == sword4.getId()).count());
        assertEquals(sword4.getDamageMax() - sword1.getDamageMax(), finalDamageMaxEnd - finalDamageMax);
    }

    @Test
    public void useItemFromOwnInventoryNoEquipped() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.1, 10);
        ItemPrivate sword3 = Factory.createItemPrivate(0, 3, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.2, 10);
        ItemPrivate sword4 = Factory.createItemPrivate(0, 4, ItemClass.WEAPON, "Sword1Name", 10, 20, 0.1, 1.3, 10);

        character.getInventory().add(sword2);
        character.getInventory().add(sword3);
        character.getInventory().add(sword4);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //skills to not work here
        int finalDamageMax = character.getFinalDamageMax();

//        Mockito.when(characterRepository.saveAndFlush(skillsService.applySkills(character, character))).thenReturn(character);
        Mockito.when(skillsService.applySkills(character, character)).thenReturn(character);

        //act
        character = mockCharacterServiceImpl.useItemFromOwnInventory(character, character.getId(), sword4.getId());

        int finalDamageMaxEnd = character.getFinalDamageMax();

        //assert
        assertEquals(character.getEquipment().stream().filter(i -> i.getItemClass() == ItemClass.WEAPON).findFirst().get(),
                sword4);
        //remove the worst item
        List<ItemPrivate> inventory = character.getInventory();
        assertEquals(2, inventory.size());
        assertEquals(2, character.getInventory().stream().filter(i -> i.getName().equals("Sword1Name")).count());
        assertEquals(0, inventory.stream().filter(i -> i.getId() == sword4.getId()).count());
        assertEquals(sword4.getDamageMax(), finalDamageMaxEnd - finalDamageMax);
    }

    @Test
    public void useItemFromOwnInventory_Should_ThrowIfItemNotFound() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //act & assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockCharacterServiceImpl.useItemFromOwnInventory(character, character.getId(), 5));
    }

    @Test
    public void useItemFromOwnInventory_Should_ThrowIfItemBiggerLevel() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 19, 5, ZoneClass.LowCreeps);
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        character.getInventory().add(sword1);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.useItemFromOwnInventory(character, character.getId(), 1));
    }

    @Test
    public void useItemFromOwnInventory_Should_ThrowIfItemIsBagButSmaller() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 19, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal1 = Factory.createCrystal(1, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate crystal2 = Factory.createCrystal(2, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate crystal3 = Factory.createCrystal(3, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate bag = Factory.createItemPrivate(0, 1, ItemClass.BAG, "Bagg", 0, 0, 0.0, 0.0, 10);
        bag.setCapacity(2);
        character.getInventory().add(bag);
        //the collection bag, not the item
        character.getBag().add(crystal1);
        character.getBag().add(crystal2);
        character.getBag().add(crystal3);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.useItemFromOwnInventory(character, character.getId(), 1));
    }

    @Test
    public void transformRing_Should_ThrowIfItemNotFound() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal1 = Factory.createCrystal(1, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate emptyRing = Factory.createEmptyRing(2, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);

        //act & assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockCharacterServiceImpl.transformRing(character, emptyRing.getId(), crystal1.getId()));
    }

    @Test
    public void transformRing_Should_ThrowIfOneItemFoundOtherNot() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal1 = Factory.createCrystal(1, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate emptyRing = Factory.createEmptyRing(2, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);

        Mockito.when(itemPrivateService.ifExists(emptyRing.getId())).thenReturn(true);

        //act & assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockCharacterServiceImpl.transformRing(character, emptyRing.getId(), crystal1.getId()));
    }

    @Test
    public void transformRing_Should_ThrowIfRingIDIsNotEmptyRing() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal1 = Factory.createCrystal(1, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate emptyRing = Factory.createEmptyRing(2, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystal3 = Factory.createCrystal(3, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);

        Mockito.when(itemPrivateService.ifExists(crystal1.getId())).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(crystal3.getId())).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(crystal1.getId())).thenReturn(crystal1);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.transformRing(character, crystal1.getId(), crystal3.getId()));
    }

    @Test
    public void transformRing_Should_ThrowIfRingNotInBag() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal1 = Factory.createCrystal(1, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate emptyRing = Factory.createEmptyRing(2, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);

        Mockito.when(itemPrivateService.ifExists(crystal1.getId())).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(emptyRing.getId())).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        character.getBag().add(crystal1);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.transformRing(character, emptyRing.getId(), crystal1.getId()));
    }

    @Test
    public void transformRing_Should_ThrowIfCrystalNotInBag() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal1 = Factory.createCrystal(1, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate emptyRing = Factory.createEmptyRing(2, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);

        Mockito.when(itemPrivateService.ifExists(crystal1.getId())).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(emptyRing.getId())).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        character.getBag().add(emptyRing);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.transformRing(character, emptyRing.getId(), crystal1.getId()));
    }

    @Test
    public void transformRingShouldTransformRUBYIfElementsInBag() {
        //arrange
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystal = Factory.createCrystal(2, ItemClass.CRYSTAL, "Ruby", Constants.CRYSTAL_RUBY);
        ItemPrivate crystalRing = Factory.createEmptyRing(3, ItemClass.RING, "Crystal Ring", Constants.CRYSTAL_RING);
        String descCrystalRing = crystalRing.getDescription();

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.getBag().add(emptyRing);
        character.getBag().add(crystal);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        int charLifeBeforeRing = character.getLife();

        Mockito.when(itemPrivateService.ifExists(1)).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(2)).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(crystal.getId())).thenReturn(crystal);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        Mockito.when(itemsService.createItemPrivateFromBaseItem(itemsService.getOneByName("Crystal Ring"))).thenReturn(crystalRing);

        //act
        character = mockCharacterServiceImpl.transformRing(character, 1, 2);
        ItemPrivate realRing = character.getBag().get(0);
        character.getEquipment().add(realRing);
        character.getBag().remove(realRing);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(0, character.getBag().size());
        assertEquals(crystal.getName(), realRing.getCrystal());
        assertEquals(descCrystalRing + crystal.getDescription(), realRing.getDescription());
        assertEquals((int) (charLifeBeforeRing * 1.2), character.getEquipLife());
        assertEquals(character.getEquipLife(), character.getFinalLife());
    }

    @Test
    public void transformRingShouldTransformAmberIfElementsInBag() {
        //arrange
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystal = Factory.createCrystal(2, ItemClass.CRYSTAL, "Amber", Constants.CRYSTAL_AMBER);
        ItemPrivate crystalRing = Factory.createEmptyRing(3, ItemClass.RING, "Crystal Ring", Constants.CRYSTAL_RING);
        String descCrystalRing = crystalRing.getDescription();

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.getBag().add(emptyRing);
        character.getBag().add(crystal);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        double charSpeedBeforeRing = character.getSpeed();

        Mockito.when(itemPrivateService.ifExists(1)).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(2)).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(crystal.getId())).thenReturn(crystal);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        Mockito.when(itemsService.createItemPrivateFromBaseItem(itemsService.getOneByName("Crystal Ring"))).thenReturn(crystalRing);

        //act
        character = mockCharacterServiceImpl.transformRing(character, 1, 2);
        ItemPrivate realRing = character.getBag().get(0);
        character.getEquipment().add(realRing);
        character.getBag().remove(realRing);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(0, character.getBag().size());
        assertEquals(crystal.getName(), realRing.getCrystal());
        assertEquals(descCrystalRing + crystal.getDescription(), realRing.getDescription());
        assertEquals(Utils.round(charSpeedBeforeRing * 0.8, 1), character.getEquipSpeed(), 0.0001);
    }

    @Test
    public void transformRingShouldTransformAmethystIfElementsInBag() {
        //arrange
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystal = Factory.createCrystal(2, ItemClass.CRYSTAL, "Amethyst", Constants.CRYSTAL_AMETHYST);
        ItemPrivate crystalRing = Factory.createEmptyRing(3, ItemClass.RING, "Crystal Ring", Constants.CRYSTAL_RING);
        String descCrystalRing = crystalRing.getDescription();

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.getBag().add(emptyRing);
        character.getBag().add(crystal);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        int charArmorBeforeRing = character.getArmor();

        Mockito.when(itemPrivateService.ifExists(1)).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(2)).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(crystal.getId())).thenReturn(crystal);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        Mockito.when(itemsService.createItemPrivateFromBaseItem(itemsService.getOneByName("Crystal Ring"))).thenReturn(crystalRing);

        //act
        character = mockCharacterServiceImpl.transformRing(character, 1, 2);
        ItemPrivate realRing = character.getBag().get(0);
        character.getEquipment().add(realRing);
        character.getBag().remove(realRing);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(0, character.getBag().size());
        assertEquals(crystal.getName(), realRing.getCrystal());
        assertEquals(descCrystalRing + crystal.getDescription(), realRing.getDescription());
        assertEquals((int) (charArmorBeforeRing * 1.2), character.getEquipArmor());
    }

    @Test
    public void transformRingShouldTransformSapphireIfElementsInBag() {
        //arrange
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystal = Factory.createCrystal(2, ItemClass.CRYSTAL, "Sapphire", Constants.CRYSTAL_SAPPHIRE);
        ItemPrivate crystalRing = Factory.createEmptyRing(3, ItemClass.RING, "Crystal Ring", Constants.CRYSTAL_RING);
        String descCrystalRing = crystalRing.getDescription();

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.getBag().add(emptyRing);
        character.getBag().add(crystal);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        int charDmgMaxBeforeRing = character.getDamageMax();

        Mockito.when(itemPrivateService.ifExists(1)).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(2)).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(crystal.getId())).thenReturn(crystal);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        Mockito.when(itemsService.createItemPrivateFromBaseItem(itemsService.getOneByName("Crystal Ring"))).thenReturn(crystalRing);

        //act
        character = mockCharacterServiceImpl.transformRing(character, 1, 2);
        ItemPrivate realRing = character.getBag().get(0);
        character.getEquipment().add(realRing);
        character.getBag().remove(realRing);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(0, character.getBag().size());
        assertEquals(crystal.getName(), realRing.getCrystal());
        assertEquals(descCrystalRing + crystal.getDescription(), realRing.getDescription());
        assertEquals((int) (charDmgMaxBeforeRing * 1.2), character.getEquipDamageMax());
    }

    @Test
    public void transformRingShouldTransformDiamondIfElementsInBag() {
        //arrange
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystal = Factory.createCrystal(2, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        ItemPrivate crystalRing = Factory.createEmptyRing(3, ItemClass.RING, "Crystal Ring", Constants.CRYSTAL_RING);
        String descCrystalRing = crystalRing.getDescription();

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.setMagicDamageBase(10);
        character.getBag().add(emptyRing);
        character.getBag().add(crystal);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        double charMagicDmgBeforeRing = character.getMagicDamage();

        Mockito.when(itemPrivateService.ifExists(1)).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(2)).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(crystal.getId())).thenReturn(crystal);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        Mockito.when(itemsService.createItemPrivateFromBaseItem(itemsService.getOneByName("Crystal Ring"))).thenReturn(crystalRing);

        //act
        character = mockCharacterServiceImpl.transformRing(character, 1, 2);
        ItemPrivate realRing = character.getBag().get(0);
        character.getEquipment().add(realRing);
        character.getBag().remove(realRing);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(0, character.getBag().size());
        assertEquals(crystal.getName(), realRing.getCrystal());
        assertEquals(descCrystalRing + crystal.getDescription(), realRing.getDescription());
        assertEquals((int) (charMagicDmgBeforeRing * 1.2), character.getEquipMagicDamage());
    }

    @Test
    public void transformRingShouldTransformEmeraldIfElementsInBag() {
        //arrange
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystal = Factory.createCrystal(2, ItemClass.CRYSTAL, "Emerald", Constants.CRYSTAL_EMERALD);
        ItemPrivate crystalRing = Factory.createEmptyRing(3, ItemClass.RING, "Crystal Ring", Constants.CRYSTAL_RING);
        String descCrystalRing = crystalRing.getDescription();

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.getBag().add(emptyRing);
        character.getBag().add(crystal);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        double charBlockBeforeRing = character.getBlockChance();

        Mockito.when(itemPrivateService.ifExists(1)).thenReturn(true);
        Mockito.when(itemPrivateService.ifExists(2)).thenReturn(true);
        Mockito.when(itemPrivateService.getOne(crystal.getId())).thenReturn(crystal);
        Mockito.when(itemPrivateService.getOne(emptyRing.getId())).thenReturn(emptyRing);
        Mockito.when(itemsService.createItemPrivateFromBaseItem(itemsService.getOneByName("Crystal Ring"))).thenReturn(crystalRing);

        //act
        character = mockCharacterServiceImpl.transformRing(character, 1, 2);
        ItemPrivate realRing = character.getBag().get(0);
        character.getEquipment().add(realRing);
        character.getBag().remove(realRing);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(0, character.getBag().size());
        assertEquals(crystal.getName(), realRing.getCrystal());
        assertEquals(descCrystalRing + crystal.getDescription(), realRing.getDescription());
        assertEquals(Utils.round(charBlockBeforeRing + 0.1, 2), character.getEquipBlockChance(), 0.0001);
    }

    @Test
    public void useItemFromOwnBag_Should_ThrowIfItemNotFound() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        ItemPrivate crystalRing = Factory.createCrystalRingWithCrystal(1, "Ruby", Constants.CRYSTAL_RUBY);

        //act & assert
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockCharacterServiceImpl.useItemFromOwnBag(character, character.getId(), crystalRing.getId()));
    }

    @Test
    public void useItemFromOwnBag_Should_ThrowIfItemLevelBigger() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 8, 5, ZoneClass.LowCreeps);
        //empty ring is always level 10
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        ItemPrivate crystalRing = Factory.createCrystalRingWithCrystal(2, "Ruby", Constants.CRYSTAL_RUBY);
        character.getBag().add(crystalRing);
        character.getBag().add(emptyRing);

        //act & assert
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.useItemFromOwnBag(character, character.getId(), emptyRing.getId()));
    }

    @Test
    public void useItemFromOwnBag_Should_ThrowIfItemIsCrystal() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 8, 5, ZoneClass.LowCreeps);
        ItemPrivate crystal1 = Factory.createCrystal(1, ItemClass.CRYSTAL, "Diamond", Constants.CRYSTAL_DIAMOND);
        character.getBag().add(crystal1);

        //act & assert
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.useItemFromOwnBag(character, character.getId(), crystal1.getId()));
    }

    @Test
    public void useItemFromOwnBagShouldUseIfRingInBag() {
        //arrange
        ItemPrivate crystalRing = Factory.createCrystalRingWithCrystal(1, "Ruby", Constants.CRYSTAL_RUBY);

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.getBag().add(crystalRing);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        double charLifeBeforeRing = character.getFinalLife();

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //act
        character = mockCharacterServiceImpl.useItemFromOwnBag(character, character.getId(), crystalRing.getId());

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(0, character.getBag().size());
        assertEquals((int) (charLifeBeforeRing * 1.2), character.getEquipLife());
    }

    @Test
    public void useItemFromOwnBagShouldUseIfWeExchangeRings() {
        //arrange
        ItemPrivate crystalRingWithRuby = Factory.createCrystalRingWithCrystal(1, "Ruby", Constants.CRYSTAL_RUBY);
        ItemPrivate crystalRingWithAmethyst = Factory.createCrystalRingWithCrystal(2, "Amethyst", Constants.CRYSTAL_AMETHYST);

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        character.getBag().add(crystalRingWithRuby);
        character.getEquipment().add(crystalRingWithAmethyst);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);
        double charLifeBeforeChange = character.getFinalLife();
        int charArmorBeforeChange = character.getFinalArmor();

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //act
        character = mockCharacterServiceImpl.useItemFromOwnBag(character, character.getId(), crystalRingWithRuby.getId());

        //assert
        assertEquals(2, character.getEquipment().size());
        assertEquals(1, character.getBag().size());
        assertEquals((int) (charLifeBeforeChange * 1.2), character.getEquipLife());
        assertEquals((int) (charArmorBeforeChange / 1.2), character.getEquipArmor());
    }

    @Test
    public void giveItemToOtherCharacterShoudGiveFromInventory() {
        //arrenge
//        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING, 10);
        //thw sword thaw we will give is worse than sword2 becasue the give method uses useItemIfBetterOrStashIt()
        //so I dont want the given to go into the equipment
        ItemPrivate sword1 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 3, ItemClass.WEAPON, "Sword1XXX", 14, 28, 0.1, 1.5, 22);
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Titan, 25, 5, ZoneClass.LowCreeps);

        character.getInventory().add(sword1);
        character2.getEquipment().add(sword2);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
//        Mockito.when(characterRepository.findByName(character2.getName()).isPresent()).thenReturn(true);
        Mockito.when(characterRepository.getOne(character.getId())).thenReturn(character);
        Mockito.when(characterRepository.getOneByName(character2.getName())).thenReturn(character2);

        //act
        List<Character> result = mockCharacterServiceImpl.giveItemToOtherCharacter(character.getId(), character2.getName(), sword1.getId());
        character = result.get(0);
        character2 = result.get(1);

        // assert
        assertEquals(0, character.getInventory().size());
        assertEquals(1, character2.getInventory().size());
        ItemPrivate givenSword = character2.getInventory().get(0);
        assertEquals(sword1, givenSword);
    }

    @Test
    public void giveItemToOtherCharacterShoudGiveFromBag() {
        //arrenge
        ItemPrivate emptyRing = Factory.createEmptyRing(1, ItemClass.RING, "Empty Ring", Constants.EMPTY_RING);
        //thw sword thaw we will give is worse than sword2 becasue the give method uses useItemIfBetterOrStashIt()
        //so I dont want the given to go into the equipment
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Titan, 25, 5, ZoneClass.LowCreeps);

        character.getBag().add(emptyRing);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
//        Mockito.when(characterRepository.findByName(character2.getName()).isPresent()).thenReturn(true);
//        Mockito.when(mockCharacterServiceImpl.ifExistsByName(character.getName())).thenReturn(true);
//        doReturn(true).when(characterRepository).findByName(character2.getName()).isPresent();
        //doReturn(fooBar).when(bar).getFoo()
        Mockito.when(characterRepository.getOne(character.getId())).thenReturn(character);
        Mockito.when(characterRepository.getOneByName(character2.getName())).thenReturn(character2);

        //act
        List<Character> result = mockCharacterServiceImpl.giveItemToOtherCharacter(character.getId(), character2.getName(), emptyRing.getId());
        character = result.get(0);
        character2 = result.get(1);

        // assert
        assertEquals(0, character.getBag().size());
        assertEquals(1, character2.getBag().size());
        ItemPrivate givenRing = character2.getBag().get(0);
        assertEquals(emptyRing, givenRing);
    }

    @Test
    public void TestFightCHARSAndActiveSkillsCritDyna() {
        //arrange
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword2XXX", 14, 28, 0.1, 1.5, 22);
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Titan, 25, 5, ZoneClass.LowCreeps);

        character.getEquipment().add(sword1);
        character2.getEquipment().add(sword2);

        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        critical.setId(1);
        Skill dynamite = ModelFactory.skillsFactoryMethod(SkillClass.Dynamite);
        dynamite.setId(2);
        character.getSkills().add(critical);
        character2.getSkills().add(dynamite);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        character2 = mockCharacterServiceImpl.updateStatsFromBaseStats(character2);
        character2 = mockCharacterServiceImpl.updateEquipStats(character2);

        //Apply curses
        String availability1;
        String availability2;
        if (character.getType() == IndividualType.Character) {
            availability1 = character.getCharacterClass().name();
        } else {
            availability1 = character.getName();
        }

        if (character2.getType() == IndividualType.Character) {
            availability2 = character2.getCharacterClass().name();
        } else {
            availability2 = character2.getName();
        }

        Mockito.when(skillsService.applyCurses(character, character2, availability1)).thenReturn(character2);
        Mockito.when(skillsService.applyCurses(character2, character, availability2)).thenReturn(character);

        //act
        FightResultBean fightResultBean = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean1 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean2 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean3 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean4 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean5 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean6 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean7 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean8 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        //assert
        assertEquals(character, character);
    }

    @Test
    public void TestFightCHARSAndActiveSkillsMultyShock() {
        //arrange
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword2XXX", 14, 28, 0.1, 1.5, 22);
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Titan, 25, 5, ZoneClass.LowCreeps);

        character.getEquipment().add(sword1);
        character2.getEquipment().add(sword2);

        Skill multyHit = ModelFactory.skillsFactoryMethod(SkillClass.MultiHit);
        multyHit.setId(1);
        Skill shock = ModelFactory.skillsFactoryMethod(SkillClass.Shock);
        shock.setId(2);
        character.getSkills().add(shock);
        character2.getSkills().add(multyHit);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        character2 = mockCharacterServiceImpl.updateStatsFromBaseStats(character2);
        character2 = mockCharacterServiceImpl.updateEquipStats(character2);

        //Apply curses
        String availability1;
        String availability2;
        if (character.getType() == IndividualType.Character) {
            availability1 = character.getCharacterClass().name();
        } else {
            availability1 = character.getName();
        }

        if (character2.getType() == IndividualType.Character) {
            availability2 = character2.getCharacterClass().name();
        } else {
            availability2 = character2.getName();
        }

        Mockito.when(skillsService.applyCurses(character, character2, availability1)).thenReturn(character2);
        Mockito.when(skillsService.applyCurses(character2, character, availability2)).thenReturn(character);

        //act
        FightResultBean fightResultBean = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean1 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean2 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean3 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean4 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean5 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean6 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean7 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean8 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        //assert
        assertEquals(character, character);
    }

    @Test
    public void TestFightCHARSAndActiveSkillsShockMulty() {
        //arrange
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword2XXX", 14, 28, 0.1, 1.5, 22);
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Titan, 25, 5, ZoneClass.LowCreeps);

        character.getEquipment().add(sword1);
        character2.getEquipment().add(sword2);

        Skill multyHit = ModelFactory.skillsFactoryMethod(SkillClass.MultiHit);
        multyHit.setId(1);
        Skill shock = ModelFactory.skillsFactoryMethod(SkillClass.Shock);
        shock.setId(2);
        character.getSkills().add(multyHit);
        character2.getSkills().add(shock);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        character2 = mockCharacterServiceImpl.updateStatsFromBaseStats(character2);
        character2 = mockCharacterServiceImpl.updateEquipStats(character2);

        //Apply curses
        String availability1;
        String availability2;
        if (character.getType() == IndividualType.Character) {
            availability1 = character.getCharacterClass().name();
        } else {
            availability1 = character.getName();
        }

        if (character2.getType() == IndividualType.Character) {
            availability2 = character2.getCharacterClass().name();
        } else {
            availability2 = character2.getName();
        }

        Mockito.when(skillsService.applyCurses(character, character2, availability1)).thenReturn(character2);
        Mockito.when(skillsService.applyCurses(character2, character, availability2)).thenReturn(character);

        //act
        FightResultBean fightResultBean = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean1 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean2 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean3 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean4 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean5 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean6 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean7 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean8 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        //assert
        assertEquals(character, character);
    }

    @Test
    public void TestFightCHARSAndActiveSkillsDynaCrit() {
        //arrange
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword2XXX", 14, 28, 0.1, 1.5, 22);
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 25, 5, ZoneClass.LowCreeps);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Titan, 25, 5, ZoneClass.LowCreeps);

        character.getEquipment().add(sword1);
        character2.getEquipment().add(sword2);

        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        critical.setId(1);
        Skill dynamite = ModelFactory.skillsFactoryMethod(SkillClass.Dynamite);
        dynamite.setId(2);
        character.getSkills().add(dynamite);
        character2.getSkills().add(critical);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        character2 = mockCharacterServiceImpl.updateStatsFromBaseStats(character2);
        character2 = mockCharacterServiceImpl.updateEquipStats(character2);

        //Apply curses
        String availability1;
        String availability2;
        if (character.getType() == IndividualType.Character) {
            availability1 = character.getCharacterClass().name();
        } else {
            availability1 = character.getName();
        }

        if (character2.getType() == IndividualType.Character) {
            availability2 = character2.getCharacterClass().name();
        } else {
            availability2 = character2.getName();
        }

        Mockito.when(skillsService.applyCurses(character, character2, availability1)).thenReturn(character2);
        Mockito.when(skillsService.applyCurses(character2, character, availability2)).thenReturn(character);

        //act
        FightResultBean fightResultBean = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean1 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean2 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean3 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean4 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean5 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean6 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean7 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        FightResultBean fightResultBean8 = mockCharacterServiceImpl.fight(character, character2, character, character2);
        //assert
        assertEquals(character, character);
    }

    @Test
    public void TestFightWithCreep() {
        //arrange
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 40, 5, ZoneClass.LowCreeps);
        Individual creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Name", 20, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        character.getEquipment().add(sword1);

        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        critical.setId(1);
        character.getSkills().add(critical);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = creep.getName();

        Mockito.when(skillsService.applyCurses(creep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, creep, availability1)).thenReturn(creep);
//        List<Integer> allLevels = ModelFactory.getAllLevels();
//        int level = mockCharacterServiceImpl.ca
//        Mockito.when(characterRepository.calculateLevel(character)).thenReturn(ModelFactory.getAllLevels());

        //act
        FightResultBean fightResultBean = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean1 = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean2 = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean3 = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean4 = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean5 = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean6 = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean7 = mockCharacterServiceImpl.fight(character, creep, character, null);
        FightResultBean fightResultBean8 = mockCharacterServiceImpl.fight(character, creep, character, null);
        //assert
        assertEquals(character, character);
    }

    @Test
    public void TestGetFightsInBattlesLowCreeps() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.LowCreeps);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 1, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 1, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 6, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 6, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Ares);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = creep.getName();

        Mockito.when(skillsService.applyCurses(creep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, creep, availability1)).thenReturn(creep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + creep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesMidCreeps() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.MidCreeps);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 9, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 1, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 10, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 7, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 7, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Ares);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = creep.getName();

        Mockito.when(skillsService.applyCurses(creep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, creep, availability1)).thenReturn(creep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + creep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesHighCreeps() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.HighCreeps);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 12, 3, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 1, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Ares);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = creep.getName();

        Mockito.when(skillsService.applyCurses(creep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, creep, availability1)).thenReturn(creep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + creep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesLowBosses() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.LowBosses);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Ares);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = boss.getName();

        Mockito.when(skillsService.applyCurses(boss, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, boss, availability1)).thenReturn(boss);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + boss.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesMidBosses() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.MidBosses);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep boss2 = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 1, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep boss3 = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 15, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(boss2);
        allCreeps.add(boss3);
        allCreeps.add(Ares);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = boss.getName();

        Mockito.when(skillsService.applyCurses(boss, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, boss, availability1)).thenReturn(boss);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + boss.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesHighBosses() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.HighBosses);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 4, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep boss2 = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 1, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep boss3 = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 20, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(boss2);
        allCreeps.add(boss3);
        allCreeps.add(Ares);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = boss.getName();

        Mockito.when(skillsService.applyCurses(boss, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, boss, availability1)).thenReturn(boss);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + boss.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesAres() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Ares);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Thanatos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Thanatos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Achlys = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Achlys.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Ares);
        allCreeps.add(Thanatos);
        allCreeps.add(Achlys);

        //THE TARGET!
        Creep targetCreep = Ares;

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = targetCreep.getName();

        Mockito.when(skillsService.applyCurses(targetCreep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, targetCreep, availability1)).thenReturn(targetCreep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + targetCreep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesThanatos() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Thanatos);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Thanatos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Thanatos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Achlys = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Achlys.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Ares);
        allCreeps.add(Thanatos);
        allCreeps.add(Achlys);

        //THE TARGET!
        Creep targetCreep = Thanatos;

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = targetCreep.getName();

        Mockito.when(skillsService.applyCurses(targetCreep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, targetCreep, availability1)).thenReturn(targetCreep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + targetCreep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesAchlys() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Achlys);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Ares = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Ares.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Thanatos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Thanatos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Achlys = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Achlys.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Ares);
        allCreeps.add(Thanatos);
        allCreeps.add(Achlys);

        //THE TARGET!
        Creep targetCreep = Achlys;

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = targetCreep.getName();

        Mockito.when(skillsService.applyCurses(targetCreep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, targetCreep, availability1)).thenReturn(targetCreep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + targetCreep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesErebus() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Erebus);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Erebus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Erebus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Chaos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Chaos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Hades = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Hades.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Tartarus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Tartarus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Erebus);
        allCreeps.add(Chaos);
        allCreeps.add(Hades);
        allCreeps.add(Tartarus);

        //THE TARGET!
        Creep targetCreep = Erebus;

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = targetCreep.getName();

        Mockito.when(skillsService.applyCurses(targetCreep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, targetCreep, availability1)).thenReturn(targetCreep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + targetCreep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesChaos() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Chaos);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Erebus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Erebus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Chaos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Chaos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Hades = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Hades.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Tartarus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Tartarus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Erebus);
        allCreeps.add(Chaos);
        allCreeps.add(Hades);
        allCreeps.add(Tartarus);

        //THE TARGET!
        Creep targetCreep = Chaos;

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = targetCreep.getName();

        Mockito.when(skillsService.applyCurses(targetCreep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, targetCreep, availability1)).thenReturn(targetCreep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + targetCreep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesHades() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Hades);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 5, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Erebus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Erebus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Chaos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Chaos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Hades = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Hades.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Tartarus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Tartarus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Erebus);
        allCreeps.add(Chaos);
        allCreeps.add(Hades);
        allCreeps.add(Tartarus);

        //THE TARGET!
        Creep targetCreep = Hades;

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = targetCreep.getName();

        Mockito.when(skillsService.applyCurses(targetCreep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, targetCreep, availability1)).thenReturn(targetCreep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + targetCreep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesTartarus() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Tartarus);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 6, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Erebus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Erebus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Chaos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Chaos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Hades = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Hades.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Tartarus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Tartarus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Erebus);
        allCreeps.add(Chaos);
        allCreeps.add(Hades);
        allCreeps.add(Tartarus);

        //THE TARGET!
        Creep targetCreep = Tartarus;

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = targetCreep.getName();

        Mockito.when(skillsService.applyCurses(targetCreep, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, targetCreep, availability1)).thenReturn(targetCreep);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        assertEquals(character.getName() + " " + targetCreep.getName(), result.get(0));
    }

    @Test
    public void TestGetFightsInBattlesArena() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Erebus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Erebus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Chaos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Chaos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Hades = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Hades.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Tartarus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Tartarus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);
        allChars.add(character2);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Erebus);
        allCreeps.add(Chaos);
        allCreeps.add(Hades);
        allCreeps.add(Tartarus);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        character2 = mockCharacterServiceImpl.updateStatsFromBaseStats(character2);
        character2 = mockCharacterServiceImpl.updateEquipStats(character2);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = character2.getCharacterClass().name();

        Mockito.when(skillsService.applyCurses(character2, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, character2, availability1)).thenReturn(character2);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(2, result.size());
        boolean twoFights1 = (result.get(0) + result.get(1)).contains(character.getName() + " " + character2.getName());
        boolean twoFights2 = (result.get(0) + result.get(1)).contains(character2.getName() + " " + character.getName());

        assertTrue(twoFights1);
        assertTrue(twoFights2);
    }

    @Test
    public void TestGetFightsInBattlesDuel() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character character2 = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        character.setDuel(character2.getName());

        Creep creep = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep", 6, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep2 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep2", 7, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep creep3 = Factory.createCreep(IndividualType.Creep, CreepClass.Monster, "Name Creep3", 8, 2, 0.1,
                0, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        Creep boss = Factory.createCreep(IndividualType.Boss, CreepClass.Monster, "Name Boss", 16, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Erebus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Erebus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Chaos = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Chaos.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Hades = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Hades.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);
        Creep Tartarus = Factory.createCreep(IndividualType.God, CreepClass.Monster, Gods.Tartarus.name(), 8, 2, 0.1,
                1, 40, 160, 0, 80, 700, 1.0, 10000, 2000);

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);
        allChars.add(character2);

        List<Creep> allCreeps = new ArrayList<>();
        allCreeps.add(creep);
        allCreeps.add(creep2);
        allCreeps.add(creep3);
        allCreeps.add(boss);
        allCreeps.add(Erebus);
        allCreeps.add(Chaos);
        allCreeps.add(Hades);
        allCreeps.add(Tartarus);

        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        character = mockCharacterServiceImpl.updateStatsFromBaseStats(character);
        character = mockCharacterServiceImpl.updateEquipStats(character);

        Mockito.when(characterRepository.existsById(character2.getId())).thenReturn(true);
        character2 = mockCharacterServiceImpl.updateStatsFromBaseStats(character2);
        character2 = mockCharacterServiceImpl.updateEquipStats(character2);

        //Apply curses
        String availability1 = character.getCharacterClass().name();
        String availability2 = character2.getCharacterClass().name();

        Mockito.when(skillsService.applyCurses(character2, character, availability2)).thenReturn(character);
        Mockito.when(skillsService.applyCurses(character, character2, availability1)).thenReturn(character2);
        Mockito.when(characterRepository.bringAllFighters()).thenReturn(allChars);
        Mockito.when(creepService.getAll()).thenReturn(allCreeps);

        //act
        List<String> result = mockCharacterServiceImpl.getFightsInBattles();

        //assert
        assertEquals(1, result.size());
        boolean twoFights1 = result.get(0).contains(character.getName() + " " + character2.getName()) ||
                result.get(0).contains(character2.getName() + " " + character.getName());
        assertTrue(twoFights1);
    }

    @Test
    public void nameIsFree_Should_CallRepository() {
        //arrange


        //act
        mockCharacterServiceImpl.nameIsFree("ffff");
        //assert
        Mockito.verify(characterRepository, Mockito.times(1)).findByName("ffff");
    }

    @Test
    public void getAll_Should_CallRepository() {
        //arrange


        //act
        mockCharacterServiceImpl.getAll();
        //assert
        Mockito.verify(characterRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAllSortedByExp_Should_CallRepository() {
        //arrange


        //act
        mockCharacterServiceImpl.getAllSortedByExp();
        //assert
        Mockito.verify(characterRepository, Mockito.times(1)).findAll(Sort.by(Sort.Direction.DESC, "experience"));
    }

    @Test
    public void create_Should_CallRepository() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        Mockito.when(userService.getByName(principal.getName())).thenReturn(stan);
        //act
        mockCharacterServiceImpl.create(principal, character);
        //assert
        Mockito.verify(characterRepository, Mockito.times(1)).saveAndFlush(character);
    }

    @Test
    public void create_Should_throwPrincipalNull() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Principal principal = null;
        User stan = Factory.createUserStan();

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.create(principal, character));
    }

    @Test
    public void userUpdatesDB_Should_CallRepository() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        character.setUserCreator(stan.getUsername());
//        Mockito.when(userService.getByName(principal.getName())).thenReturn(stan);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        //act
        mockCharacterServiceImpl.userUpdatesDB(character, principal);
        //assert
        Mockito.verify(characterRepository, Mockito.times(1)).saveAndFlush(character);
    }

    @Test
    public void updatesDB_Should_CallRepository() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        //act
        mockCharacterServiceImpl.updatesDB(character);
        //assert
        Mockito.verify(characterRepository, Mockito.times(1)).saveAndFlush(character);
    }

    //Act, Assert
//        Assert.assertThrows(EntityNotFoundException.class,
//            () -> mockPostService.findSliceWithPosts(startIndex, pageSize, sortParam, user,"tedi"));

    @Test
    public void delete_Should_ThrowIfCharIsCurrentChar() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.setCurrentCharacter(character);
        Mockito.when(userService.getPrincipalUser(principal)).thenReturn(stan);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //act
        mockCharacterServiceImpl.updatesDB(character);
        //assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.delete(principal, character.getId()));
    }

    @Test
    public void delete_Should_ThrowIfNotYourCharacter() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character character2 = Factory.createCharWithBag(2, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.setCurrentCharacter(character2);
        Mockito.when(userService.getPrincipalUser(principal)).thenReturn(stan);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);

        //act
        mockCharacterServiceImpl.updatesDB(character);
        //assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockCharacterServiceImpl.delete(principal, character.getId()));
    }

    @Test
    public void delete_Should_CallRepositoryAndRemoveCharIfOtherCharIsDuelingHim() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character character2 = Factory.createCharWithBag(2, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.getCharacters().add(character);
        character.setUserCreator(principal.getName());
        stan.setCurrentCharacter(character2);
        character2.setDuel(character.getName());

        List<Character> allChars = new ArrayList<>();
        allChars.add(character);
        allChars.add(character2);
        Mockito.when(userService.getPrincipalUser(principal)).thenReturn(stan);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Mockito.when(characterRepository.getOne(character.getId())).thenReturn(character);
        Mockito.when(characterRepository.findAll()).thenReturn(allChars);

        //act
        mockCharacterServiceImpl.delete(principal, character.getId());
        //assert
        Mockito.verify(characterRepository, Mockito.times(1)).deleteById(character.getId());
    }

    @Test
    public void checkIfExistsAndThrow_Should_ThrowIfCharacterDoesNotExist() {
        //arrange

        Mockito.when(characterRepository.existsById(1)).thenReturn(false);

        //act & assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockCharacterServiceImpl.checkIfExistsAndThrow(1));
    }

    ////JUnit cannot work with jpa repo .findByName(characterName).isPresent() we I will bypass this check and return true
    //Cancel the test
//    @Test
//    public void checkIfExistsAndThrow_Should_ThrowIfCharacterDoesNotExistByName() {
//        //arrange
//
//
//        //act & assert
//        Assert.assertThrows(EntityNotFoundException.class,
//                () -> mockCharacterServiceImpl.checkIfExistsAndThrow("Pal"));
//    }

    @Test
    public void createCharDromDTOModelFactoryKnight() {
        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setCharacterClass(CharacterClass.Knight);
        characterDTO.setName(characterDTO.getCharacterClass().name());

        Character character = ModelFactory.createCharacterFromDTO(characterDTO);

        assertEquals(CharacterClass.Knight, character.getCharacterClass());
        assertEquals(4, character.getArmorBase());
        assertEquals(25, character.getLifeBase());
        assertEquals(25, character.getEquipLife());
        assertEquals(25, character.getFinalLife());
    }

    @Test
    public void createCharDromDTOModelFactoryTitan() {
        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setCharacterClass(CharacterClass.Titan);
        characterDTO.setName(characterDTO.getCharacterClass().name());

        Character character = ModelFactory.createCharacterFromDTO(characterDTO);

        assertEquals(CharacterClass.Titan, character.getCharacterClass());
        assertEquals(1, character.getArmorBase());
        assertEquals(37, character.getLifeBase());
        assertEquals(37, character.getEquipLife());
        assertEquals(37, character.getFinalLife());
    }

    @Test
    public void createCharDromDTOModelFactoryShaman() {
        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setCharacterClass(CharacterClass.Shaman);
        characterDTO.setName(characterDTO.getCharacterClass().name());

        Character character = ModelFactory.createCharacterFromDTO(characterDTO);

        assertEquals(CharacterClass.Shaman, character.getCharacterClass());
        assertEquals(1, character.getArmorBase());
        assertEquals(50, character.getLifeBase());
        assertEquals(50, character.getEquipLife());
        assertEquals(50, character.getFinalLife());
    }

    @Test
    public void createCharDromDTOModelFactorySlayer() {
        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setCharacterClass(CharacterClass.Slayer);
        characterDTO.setName(characterDTO.getCharacterClass().name());

        Character character = ModelFactory.createCharacterFromDTO(characterDTO);

        assertEquals(CharacterClass.Slayer, character.getCharacterClass());
        assertEquals(2, character.getArmorBase());
        assertEquals(25, character.getLifeBase());
        assertEquals(25, character.getEquipLife());
        assertEquals(25, character.getFinalLife());
    }

    @Test
    public void createCharDromDTOModelFactoryWizard() {
        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setCharacterClass(CharacterClass.Wizard);
        characterDTO.setName(characterDTO.getCharacterClass().name());

        Character character = ModelFactory.createCharacterFromDTO(characterDTO);

        assertEquals(CharacterClass.Wizard, character.getCharacterClass());
        assertEquals(2, character.getArmorBase());
        assertEquals(24, character.getLifeBase());
        assertEquals(24, character.getEquipLife());
        assertEquals(24, character.getFinalLife());
    }

    @Test
    public void calculateLevel_Tets() {
        Character character = Factory.createCharWithOUTBag("Pol", CharacterClass.Knight, 1);
        character.setExperience(118005);
        int newLevel = mockCharacterServiceImpl.calculateLevel(character);
        assertEquals(15, newLevel);

        character.setExperience(3000005);
        newLevel = mockCharacterServiceImpl.calculateLevel(character);
        assertEquals(37, newLevel);

        character.setExperience(51400000);
        newLevel = mockCharacterServiceImpl.calculateLevel(character);
        assertEquals(80, newLevel);

        character.setExperience(111600001);
        newLevel = mockCharacterServiceImpl.calculateLevel(character);
        assertEquals(100, newLevel);
    }

    @Test
    public void createCreepFromDTO() {
        CreepDTO creepDTO = new CreepDTO();
        creepDTO.setArmor(5);
        creepDTO.setCreepClass(CreepClass.Cobra);
        creepDTO.setLevel(5);
        creepDTO.setDamageMax(199);

        Creep creep = ModelFactory.createCreepFromDTO(creepDTO);
        assertEquals(5, creep.getFinalArmor());
        assertEquals(CreepClass.Cobra.name(), creep.getCreepClass().name());
        assertEquals(5, creep.getLevel());
        assertEquals(199, creep.getFinalDamageMax());
    }

//    @Test
//    public void createItemPrivateFromBaseItem() {
//        Item item = new Item();
//        item.setRise(1.5);
//        item.setDamageMin(50);
//        item.setDamageMax(100);
//        item.setMagicDamage(40);
//        item.setArmor(100);
//        item.setLife(300);
//
//        item.setLevel(20);
//
//        item.setItemClass(ItemClass.WEAPON);
//        item.setName("StanItem");
//        item.setIsUnique(1);
//        item.setAvailableInShop(0);
//        item.setPrice(0);
//        item.setBlockChance(0.0);
//        item.setSpeed(0.5);
//        item.setCapacity(0);
//        item.setDescription("desc");
//
//        ItemPrivate itemPrivate = ModelFactory.createItemPrivateFromBaseItem(item);
//        double riseFinal = itemPrivate.getRise();
//
//        assertEquals((int) (item.getDamageMin() * riseFinal), itemPrivate.getDamageMin());
//        assertEquals((int) (item.getDamageMax() * riseFinal), itemPrivate.getDamageMax());
//        assertEquals((int) (item.getMagicDamage() * riseFinal), itemPrivate.getMagicDamage());
//        assertEquals((int) (item.getArmor() * riseFinal), itemPrivate.getArmor());
//        assertEquals((int) (item.getLife() * riseFinal), itemPrivate.getLife());
//
//        assertEquals(item.getLevel(), itemPrivate.getLevel());
//        assertEquals(item.getItemClass(), itemPrivate.getItemClass());
//        assertEquals(item.getName(), itemPrivate.getName());
//        assertEquals(item.getIsUnique(), itemPrivate.getIsUnique());
//        assertEquals(item.getAvailableInShop(), itemPrivate.getAvailableInShop());
//        assertEquals(item.getPrice(), itemPrivate.getPrice());
//        assertEquals(item.getBlockChance(), itemPrivate.getBlockChance(),0.0001);
//        assertEquals(item.getSpeed(), itemPrivate.getSpeed(),0.0001);
//        assertEquals(item.getCapacity(), itemPrivate.getCapacity());
//        assertEquals(item.getDescription(), itemPrivate.getDescription());
//
//        assertEquals("none", itemPrivate.getCrystal());
//    }

    @Test
    public void changeZoneTest() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        ZoneClass newZone = ZoneClass.Ares;
        //act
        character = mockCharacterServiceImpl.changeZone(character, newZone, "");

        //assert
        assertEquals(newZone, character.getZone());
    }

    ////JUnit cannot work with jpa repo .findByName(characterName).isPresent() we I will bypass this check and return true
    //Cancel the test
//    @Test
//    public void changeZoneShould_ThrowWhenDuelCharNotFound() {
//        //arrange
//        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
//        ZoneClass newZone = ZoneClass.Duel;
//        //act
////        character = mockCharacterServiceImpl.changeZone(character, newZone, "Tim");
//
//        //assert
//        Assert.assertThrows(IllegalArgumentException.class,
//                () -> mockCharacterServiceImpl.changeZone(character, newZone, "Tim"));
//    }

    @Test
    public void changeZoneTestDuel() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        Character character2 = Factory.createCharWithBag(2, "Tim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        ZoneClass newZone = ZoneClass.Duel;
//        Mockito.when(characterRepository.findByName(character2.getName()).isPresent()).thenReturn(true);
        //act
        character = mockCharacterServiceImpl.changeZone(character, newZone, character2.getName());

        //assert
        assertEquals(newZone, character.getZone());
        assertEquals(character2.getName(), character.getDuel());
    }

    @Test
    public void getCurrentCharacterOfUserTest() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        User user = Factory.createUserStan();
        Principal principal = () -> "stan";
        user.setCurrentCharacter(character);

        Mockito.when(userService.getByName(principal.getName())).thenReturn(user);

        //act
        Character resultChar = mockCharacterServiceImpl.getCurrentCharacterOfUser(principal);

        //assert
        assertEquals(character, resultChar);
    }

    @Test
    public void isBestEquippedTest() {
        //arrange
        ItemPrivate sword1 = Factory.createItemPrivate(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 20);
        ItemPrivate sword2 = Factory.createItemPrivate(0, 2, ItemClass.WEAPON, "Sword2XXX", 14, 28, 0.1, 1.4, 22);
        ItemPrivate sword3 = Factory.createItemPrivate(0, 3, ItemClass.WEAPON, "Sword2XXX", 14, 28, 0.1, 1.5, 22);

        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.Arena);
        User user = Factory.createUserStan();
        Principal principal = () -> "stan";
        user.setCurrentCharacter(character);

        character.getEquipment().add(sword1);
        character.getInventory().add(sword2);
        character.getInventory().add(sword3);

        Mockito.when(userService.getPrincipalUser(principal)).thenReturn(user);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Mockito.when(skillsService.applySkills(character, character)).thenReturn(character);
        Mockito.when(characterRepository.saveAndFlush(skillsService.applySkills(character, character))).thenReturn(character);

        //act & assert
        boolean isBestEquipped = mockCharacterServiceImpl.isBestEquipped(principal);
        assertFalse(isBestEquipped);

        character = mockCharacterServiceImpl.useItemFromOwnInventory(character, character.getId(), 2);
        isBestEquipped = mockCharacterServiceImpl.isBestEquipped(principal);
        assertFalse(isBestEquipped);

        character = mockCharacterServiceImpl.useItemFromOwnInventory(character, character.getId(), 3);
        isBestEquipped = mockCharacterServiceImpl.isBestEquipped(principal);
        assertTrue(isBestEquipped);
    }

    @Test
    public void skillLevelUpdateAndUpdateDBTEST_ThrowIfSkillNotFound() {
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.Arena);
        Skill skill = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        int dmgStart = character.getFinalDamageMax();

        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.skillLevelUpdateAndUpdateDB(character, skill.getSkillClass(), 10));
    }

    @Test
    public void skillLevelUpdateAndUpdateDBTEST_ThrowIfLevelBigger() {
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.Arena);
        Skill skill = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        character.getSkills().add(skill);
        int dmgStart = character.getFinalDamageMax();

        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.skillLevelUpdateAndUpdateDB(character, skill.getSkillClass(), 25));
    }

    @Test
    public void skillLevelUpdateAndUpdateDBTEST_ThrowIfCharDoesNotHavePoints() {
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 5, 5, ZoneClass.Arena);
        Skill skill = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        character.getSkills().add(skill);
        int dmgStart = character.getFinalDamageMax();

        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockCharacterServiceImpl.skillLevelUpdateAndUpdateDB(character, skill.getSkillClass(), 10));
    }

    @Test
    public void skillLevelUpdateAndUpdateDBTEST() {
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 10, 5, ZoneClass.Arena);
        Skill skill = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        character.getSkills().add(skill);
        int dmgStart = character.getFinalDamageMax();
        int level = 10;

        Mockito.when(skillsService.skillLevelUpdate(character, skill, level)).thenReturn(character);
//        Mockito.when(characterRepository.saveAndFlush(skillsService.applySkills(character, character))).thenReturn(character);
        Mockito.when(characterRepository.saveAndFlush(character)).thenReturn(character);
        Mockito.when(characterRepository.existsById(character.getId())).thenReturn(true);
        Mockito.when(skillsService.applySkills(character, character)).thenReturn(character);

        //act
        character = mockCharacterServiceImpl.skillLevelUpdateAndUpdateDB(character, skill.getSkillClass(), level);

        //we cannot test here if skillsService.applySkills() and skillsService.skillLevelUpdate() work
        //this will be done in the Tests of SkillsService
        assertEquals(1, character.getSkills().get(0).getLevel());
    }


}
