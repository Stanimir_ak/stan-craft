package com.stan.craft;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.*;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Repositories.SkillRepository;
import com.stan.craft.Services.CharacterService;
import com.stan.craft.Services.SkillsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SkillsServiceImplTests {
    @InjectMocks
    SkillsServiceImpl mockSkillService;

    @Mock
    CharacterService characterService;
    @Mock
    SkillRepository skillRepository;

    @Test
    public void create_Should_CallRepository() {
        //arrange
        Skill skill = ModelFactory.skillsFactoryMethod(SkillClass.Defence);

        //act
        mockSkillService.create(skill);

        //assert
        Mockito.verify(skillRepository, Mockito.times(1)).saveAndFlush(skill);
    }

    @Test
    public void getOne_Should_CallRepository() {
        //arrange

        //act
        mockSkillService.getOne(1);

        //assert
        Mockito.verify(skillRepository, Mockito.times(1)).getOne(1);
    }

    @Test
    public void getAll_Should_CallRepository() {
        //arrange

        //act
        mockSkillService.getAll();

        //assert
        Mockito.verify(skillRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void update_Should_CallRepository() {
        //arrange
        Skill skill = ModelFactory.skillsFactoryMethod(SkillClass.Defence);

        //act
        mockSkillService.update(skill);

        //assert
        Mockito.verify(skillRepository, Mockito.times(1)).saveAndFlush(skill);
    }

    @Test
    public void delete_Should_CallRepository() {
        //arrange
        Skill skill = ModelFactory.skillsFactoryMethod(SkillClass.Defence);

        //act
        mockSkillService.delete(1);

        //assert
        Mockito.verify(skillRepository, Mockito.times(1)).deleteById(1);
    }

    @Test
    public void ModelFactorySkillMethodTest() {
        //arrange
        Skill skill1 = ModelFactory.skillsFactoryMethod(SkillClass.Swift);
        Skill skill2 = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        Skill skill3 = ModelFactory.skillsFactoryMethod(SkillClass.Defence);
        Skill skill4 = ModelFactory.skillsFactoryMethod(SkillClass.Toughness);
        Skill skill5 = ModelFactory.skillsFactoryMethod(SkillClass.GodShield);
        Skill skill6 = ModelFactory.skillsFactoryMethod(SkillClass.Spirit);

        Skill skill7 = ModelFactory.skillsFactoryMethod(SkillClass.Retard);
        Skill skill8 = ModelFactory.skillsFactoryMethod(SkillClass.Weak);
        Skill skill9 = ModelFactory.skillsFactoryMethod(SkillClass.Vulnerable);
        Skill skill10 = ModelFactory.skillsFactoryMethod(SkillClass.Invalid);
        Skill skill11 = ModelFactory.skillsFactoryMethod(SkillClass.Armless);

        Skill skill12 = ModelFactory.skillsFactoryMethod(SkillClass.Shock);
        Skill skill13 = ModelFactory.skillsFactoryMethod(SkillClass.Critical);
        Skill skill14 = ModelFactory.skillsFactoryMethod(SkillClass.MultiHit);
        Skill skill15 = ModelFactory.skillsFactoryMethod(SkillClass.Dynamite);

        //act


        //assert
        assertEquals(SkillClass.Swift, skill1.getSkillClass());
        assertEquals(SkillClass.Power, skill2.getSkillClass());
        assertEquals(SkillClass.Defence, skill3.getSkillClass());
        assertEquals(SkillClass.Toughness, skill4.getSkillClass());
        assertEquals(SkillClass.GodShield, skill5.getSkillClass());
        assertEquals(SkillClass.Spirit, skill6.getSkillClass());

        assertEquals(SkillClass.Retard, skill7.getSkillClass());
        assertEquals(SkillClass.Weak, skill8.getSkillClass());
        assertEquals(SkillClass.Vulnerable, skill9.getSkillClass());
        assertEquals(SkillClass.Invalid, skill10.getSkillClass());
        assertEquals(SkillClass.Armless, skill11.getSkillClass());

        assertEquals(SkillClass.Shock, skill12.getSkillClass());
        assertEquals(SkillClass.Critical, skill13.getSkillClass());
        assertEquals(SkillClass.MultiHit, skill14.getSkillClass());
        assertEquals(SkillClass.Dynamite, skill15.getSkillClass());
    }

    @Test
    public void applySkills_ShouldApply() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);
        Skill power = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        Skill defence = ModelFactory.skillsFactoryMethod(SkillClass.Defence);
        Skill swift = ModelFactory.skillsFactoryMethod(SkillClass.Swift);
        Skill toughness = ModelFactory.skillsFactoryMethod(SkillClass.Toughness);
        Skill godShield = ModelFactory.skillsFactoryMethod(SkillClass.GodShield);
        Skill spirit = ModelFactory.skillsFactoryMethod(SkillClass.Spirit);
        int dmgStart = character.getFinalDamageMax();
        int armorStart = character.getFinalArmor();

        character.getSkills().add(power);
        character.getSkills().add(defence);
        character.getSkills().add(swift);
        character.getSkills().add(toughness);
        character.getSkills().add(godShield);
        character.getSkills().add(spirit);
        power.setLevel(10);
        defence.setLevel(10);
        swift.setLevel(10);
        toughness.setLevel(10);
        godShield.setLevel(10);
        spirit.setLevel(10);
        //act

        character = mockSkillService.applySkills(character, character);

        //assert
        assertEquals((int) (1.0 + power.getLevel() / 10) * dmgStart, character.getFinalDamageMax());
        assertEquals((int) (1.0 + defence.getLevel() / 10) * armorStart, character.getFinalDamageMax());
    }

    @Test
    public void applyCurses_ShouldApply() {
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);
        Character character2 = Factory.createCharWithBag(1, "Jim", CharacterClass.Titan, 50, 5, ZoneClass.LowCreeps);
        Individual individual = character;
        Individual individual2 = character2;

        Skill weak = ModelFactory.skillsFactoryMethod(SkillClass.Weak);
        Skill vulnerable = ModelFactory.skillsFactoryMethod(SkillClass.Vulnerable);
        Skill retard = ModelFactory.skillsFactoryMethod(SkillClass.Retard);
        Skill invalid = ModelFactory.skillsFactoryMethod(SkillClass.Invalid);
        Skill armless = ModelFactory.skillsFactoryMethod(SkillClass.Armless);
        character.getSkills().add(weak);
        character.getSkills().add(vulnerable);
        character.getSkills().add(retard);
        character.getSkills().add(invalid);
        character.getSkills().add(armless);
        weak.setLevel(10);
        vulnerable.setLevel(10);
        retard.setLevel(10);
        invalid.setLevel(10);
        armless.setLevel(10);

        int dmgStart = character2.getFinalDamageMax();
        int armorStart = character2.getFinalArmor();

        //act
        individual2 = mockSkillService.applyCurses(character, character2, character.getCharacterClass().name());

        //assert
        assertEquals((int) (dmgStart * (1 - (Constants.SKILL_WEAK_DECREASE_DMG_PER_LVL * weak.getLevel()))), individual2.getFinalDamageMax());
        assertEquals((int) (armorStart *
                (1 - (Constants.SKILL_VULNERABLE_DECREASE_ARMOR_PER_LVL * vulnerable.getLevel()))), individual2.getFinalDamageMax());
    }

    @Test
    public void addSkill_ShouldThrowIfYouHaveIt() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);
        Skill power = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        Skill defence = ModelFactory.skillsFactoryMethod(SkillClass.Defence);

        character.getSkills().add(power);
        character.getSkills().add(defence);
        power.setLevel(10);
        defence.setLevel(10);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockSkillService.addSkill(character, character, SkillClass.Power));

    }

    @Test
    public void addSkill_ShouldThrowIfNoPoints() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 20, 5, ZoneClass.LowCreeps);
        Skill power = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        Skill defence = ModelFactory.skillsFactoryMethod(SkillClass.Defence);

        character.getSkills().add(power);
        character.getSkills().add(defence);
        power.setLevel(10);
        defence.setLevel(10);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockSkillService.addSkill(character, character, SkillClass.Weak));

    }

    @Test
    public void addSkill_ShouldThrowIfActiveSkillAddedAlready() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);
        Skill power = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        Skill defence = ModelFactory.skillsFactoryMethod(SkillClass.Defence);
        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);

        character.getSkills().add(power);
        character.getSkills().add(defence);
        character.getSkills().add(critical);
        power.setLevel(10);
        defence.setLevel(10);
        critical.setLevel(10);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockSkillService.addSkill(character, character, SkillClass.Dynamite));

    }

    @Test
    public void addSkill_ShouldAdd() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);
        Skill power = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        Skill defence = ModelFactory.skillsFactoryMethod(SkillClass.Defence);

        character.getSkills().add(power);
        character.getSkills().add(defence);
        power.setLevel(10);
        defence.setLevel(10);

        //act
        character = mockSkillService.addSkill(character, character, SkillClass.Toughness);

        //assert
        List<SkillClass> skillClasses = new ArrayList<>();
        for (Skill skill : character.getSkills()) {
            skillClasses.add(skill.getSkillClass());
        }
        boolean containCritical = skillClasses.contains(SkillClass.Toughness);

        assertEquals(3, character.getSkills().size());
        assertTrue(containCritical);
    }

    @Test
    public void removeSkill_ShouldThrowIfYouDontHaveIt() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockSkillService.removeSkill(character, SkillClass.Power));

    }

    @Test
    public void removeSkill_ShouldRemove() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);
        Skill power = ModelFactory.skillsFactoryMethod(SkillClass.Power);
        Skill defence = ModelFactory.skillsFactoryMethod(SkillClass.Defence);

        character.getSkills().add(power);
        character.getSkills().add(defence);
        power.setLevel(10);
        defence.setLevel(10);

        //act
        character = mockSkillService.removeSkill(character, SkillClass.Defence);

        //assert
        List<SkillClass> skillsClasses = new ArrayList<>();
        for (Skill skill : character.getSkills()) {
            skillsClasses.add(skill.getSkillClass());
        }
        boolean containCritical = skillsClasses.contains(SkillClass.Defence);

        assertEquals(1, character.getSkills().size());
        assertFalse(containCritical);
    }

    @Test
    public void skillLevelUpdate_ShouldThrowIfYouDontHaveIt() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);

        //act & assert
        Assert.assertThrows(IllegalArgumentException.class,
                () -> mockSkillService.skillLevelUpdate(character, null, 10));

    }

    @Test
    public void skillLevelUpdate_ShouldUpdate() {
        //arrange
        Character character = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 50, 5, ZoneClass.LowCreeps);
        Skill critical = ModelFactory.skillsFactoryMethod(SkillClass.Critical);

        character.getSkills().add(critical);
        critical.setLevel(10);
        int targetLevel = 15;

        //act
        character = mockSkillService.skillLevelUpdate(character, critical, targetLevel);

        //assert
        assertEquals(1, character.getSkills().size());
        assertEquals(critical, character.getSkills().get(0));
        assertEquals(targetLevel, character.getSkills().get(0).getLevel());
    }

    @Test
    public void availableSkillsForCharClass_Test_Knight() {
        //arrange
        CharacterClass characterClass = CharacterClass.Knight;

        //act
        String result = mockSkillService.availableSkillsForCharClass(characterClass);

        //assert
        assertEquals(Constants.KNIGHT_SKILLS_AVAILABILITY, result);
    }

    @Test
    public void availableSkillsForCharClass_Test_Titan() {
        //arrange
        CharacterClass characterClass = CharacterClass.Titan;

        //act
        String result = mockSkillService.availableSkillsForCharClass(characterClass);

        //assert
        assertEquals(Constants.TITAN_SKILLS_AVAILABILITY, result);
    }

    @Test
    public void availableSkillsForCharClass_Test_Shaman() {
        //arrange
        CharacterClass characterClass = CharacterClass.Shaman;

        //act
        String result = mockSkillService.availableSkillsForCharClass(characterClass);

        //assert
        assertEquals(Constants.SHAMAN_SKILLS_AVAILABILITY, result);
    }

    @Test
    public void availableSkillsForCharClass_Test_Wizard() {
        //arrange
        CharacterClass characterClass = CharacterClass.Wizard;

        //act
        String result = mockSkillService.availableSkillsForCharClass(characterClass);

        //assert
        assertEquals(Constants.WIZARD_SKILLS_AVAILABILITY, result);
    }

    @Test
    public void availableSkillsForCharClass_Test_Slayer() {
        //arrange
        CharacterClass characterClass = CharacterClass.Slayer;

        //act
        String result = mockSkillService.availableSkillsForCharClass(characterClass);

        //assert
        assertEquals(Constants.SLAYER_SKILLS_AVAILABILITY, result);
    }
}


























