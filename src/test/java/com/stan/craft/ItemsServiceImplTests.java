package com.stan.craft;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Exceptions.EntityNotFoundException;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.*;
import com.stan.craft.Models.Factories.ModelFactory;
import com.stan.craft.Repositories.ItemsRepository;
import com.stan.craft.Services.ItemPrivateService;
import com.stan.craft.Services.ItemsServiceImpl;
import com.stan.craft.Services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class ItemsServiceImplTests {
    @InjectMocks
    ItemsServiceImpl mockItemsService;
    @Mock
    UserService userService;
    @Mock
    ItemsRepository itemsRepository;
    @Mock
    ModelFactory modelFactory;
    @Mock
    ItemPrivateService itemPrivateService;

    @Test
    public void create_Should_CallRepository() {
        //arrange
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Item item = new Item();

        //act
        mockItemsService.create(item, principal);

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).saveAndFlush(item);
    }

    @Test
    public void getAll_Should_CallRepository() {
        //arrange

        //act
        mockItemsService.getAll();

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getItemsAccordingToLevel_Should_CallRepository() {
        //arrange

        //act
        mockItemsService.getItemsAccordingToLevelFromDB(1);

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).getItemsAccordingToLevel(1);
    }

    @Test
    public void getOne_ShouldThrowIfNotFound() {
        //arrange

        //act & assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockItemsService.getOne(1));
    }

    @Test
    public void getOne_Should_CallRepository() {
        //arrange
        Mockito.when(itemsRepository.existsById(1)).thenReturn(true);
        //act
        mockItemsService.getOne(1);

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).getOne(1);
    }

    @Test
    public void getOneByLevelAndClass_Should_CallRepository() {
        //arrange
        ItemClass itemClass = ItemClass.WEAPON;

        //act
        mockItemsService.getOneByLevelAndClass(1, itemClass);

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).getOneByLevelAndClass(1, itemClass);
    }

    @Test
    public void getOneByName_Should_CallRepository() {
        //arrange

        //act
        mockItemsService.getOneByName("item");

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).getOneByName("item");
    }

    @Test
    public void update_Should_CallRepository() {
        //arrange
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Item item = new Item();
        item.setId(1);

        Mockito.when(itemsRepository.existsById(item.getId())).thenReturn(true);

        //act
        mockItemsService.update(item, principal);

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).saveAndFlush(item);
    }

    @Test
    public void update_ShouldThrowIfNotFound() {
        //arrange
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Item item = new Item();

        //act & assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockItemsService.update(item, principal));
    }

    @Test
    public void delete_Should_CallRepository() {
        //arrange
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Item item = new Item();
        item.setId(1);

        Mockito.when(itemsRepository.existsById(item.getId())).thenReturn(true);

        //act
        mockItemsService.delete(item.getId(), principal);

        //assert
        Mockito.verify(itemsRepository, Mockito.times(1)).deleteById(item.getId());
    }

    @Test
    public void delete_ShouldThrowIfNotFound() {
        //arrange
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));
        Item item = new Item();

        //act & assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockItemsService.delete(item.getId(), principal));
    }

    @Test
    public void dropItemFromLooserTest1() {
        Character winner = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character looser = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        Item sword1 = Factory.createItem(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 1);

        Map<Integer, Integer> itemFromDropChances = new HashMap<>();
        DropChances dRLooser = looser.getDropChances();
        itemFromDropChances.put(1, dRLooser.getChanceLevel1());
        itemFromDropChances.put(10, dRLooser.getChanceLevel10());
        itemFromDropChances.put(20, dRLooser.getChanceLevel20());
        itemFromDropChances.put(30, dRLooser.getChanceLevel30());
        itemFromDropChances.put(40, dRLooser.getChanceLevel40());
        itemFromDropChances.put(50, dRLooser.getChanceLevel50());
        itemFromDropChances.put(60, dRLooser.getChanceLevel60());
        itemFromDropChances.put(70, dRLooser.getChanceLevel70());
        itemFromDropChances.put(80, dRLooser.getChanceLevel80());
        itemFromDropChances.put(0, dRLooser.getChanceCrystal());

        List<Item> l1 = new ArrayList<>();
        l1.add(sword1);
        Mockito.when(itemsRepository.getItemsAccordingToLevel(anyInt())).thenReturn(l1);

        //act & assert
        ItemPrivate droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        boolean result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);
    }

    @Test
    public void dropItemFromLooserTest2() {
        Character winner = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character looser = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        looser.setDropChances(ModelFactory.createDropChancesFromId(2));
        Item sword1 = Factory.createItem(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 1);

        Map<Integer, Integer> itemFromDropChances = new HashMap<>();
        DropChances dRLooser = looser.getDropChances();
        itemFromDropChances.put(1, dRLooser.getChanceLevel1());
        itemFromDropChances.put(10, dRLooser.getChanceLevel10());
        itemFromDropChances.put(20, dRLooser.getChanceLevel20());
        itemFromDropChances.put(30, dRLooser.getChanceLevel30());
        itemFromDropChances.put(40, dRLooser.getChanceLevel40());
        itemFromDropChances.put(50, dRLooser.getChanceLevel50());
        itemFromDropChances.put(60, dRLooser.getChanceLevel60());
        itemFromDropChances.put(70, dRLooser.getChanceLevel70());
        itemFromDropChances.put(80, dRLooser.getChanceLevel80());
        itemFromDropChances.put(0, dRLooser.getChanceCrystal());

        List<Item> l1 = new ArrayList<>();
        l1.add(sword1);
        Mockito.when(itemsRepository.getItemsAccordingToLevel(anyInt())).thenReturn(l1);

        //act & assert
        ItemPrivate droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        boolean result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);
    }

    @Test
    public void dropItemFromLooserTest3() {
        Character winner = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character looser = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        looser.setDropChances(ModelFactory.createDropChancesFromId(3));
        Item sword1 = Factory.createItem(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 1);

        Map<Integer, Integer> itemFromDropChances = new HashMap<>();
        DropChances dRLooser = looser.getDropChances();
        itemFromDropChances.put(1, dRLooser.getChanceLevel1());
        itemFromDropChances.put(10, dRLooser.getChanceLevel10());
        itemFromDropChances.put(20, dRLooser.getChanceLevel20());
        itemFromDropChances.put(30, dRLooser.getChanceLevel30());
        itemFromDropChances.put(40, dRLooser.getChanceLevel40());
        itemFromDropChances.put(50, dRLooser.getChanceLevel50());
        itemFromDropChances.put(60, dRLooser.getChanceLevel60());
        itemFromDropChances.put(70, dRLooser.getChanceLevel70());
        itemFromDropChances.put(80, dRLooser.getChanceLevel80());
        itemFromDropChances.put(0, dRLooser.getChanceCrystal());

        List<Item> l1 = new ArrayList<>();
        l1.add(sword1);
        Mockito.when(itemsRepository.getItemsAccordingToLevel(anyInt())).thenReturn(l1);

        //act & assert
        ItemPrivate droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        boolean result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);
    }

    @Test
    public void dropItemFromLooserTest4() {
        Character winner = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character looser = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        looser.setDropChances(ModelFactory.createDropChancesFromId(4));
        Item sword1 = Factory.createItem(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 1);

        Map<Integer, Integer> itemFromDropChances = new HashMap<>();
        DropChances dRLooser = looser.getDropChances();
        itemFromDropChances.put(1, dRLooser.getChanceLevel1());
        itemFromDropChances.put(10, dRLooser.getChanceLevel10());
        itemFromDropChances.put(20, dRLooser.getChanceLevel20());
        itemFromDropChances.put(30, dRLooser.getChanceLevel30());
        itemFromDropChances.put(40, dRLooser.getChanceLevel40());
        itemFromDropChances.put(50, dRLooser.getChanceLevel50());
        itemFromDropChances.put(60, dRLooser.getChanceLevel60());
        itemFromDropChances.put(70, dRLooser.getChanceLevel70());
        itemFromDropChances.put(80, dRLooser.getChanceLevel80());
        itemFromDropChances.put(0, dRLooser.getChanceCrystal());

        List<Item> l1 = new ArrayList<>();
        l1.add(sword1);
        Mockito.when(itemsRepository.getItemsAccordingToLevel(anyInt())).thenReturn(l1);

        //act & assert
        ItemPrivate droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        boolean result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);
    }

    @Test
    public void dropItemFromLooserTest5() {
        Character winner = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character looser = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        looser.setDropChances(ModelFactory.createDropChancesFromId(5));
        Item sword1 = Factory.createItem(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 1);

        Map<Integer, Integer> itemFromDropChances = new HashMap<>();
        DropChances dRLooser = looser.getDropChances();
        itemFromDropChances.put(1, dRLooser.getChanceLevel1());
        itemFromDropChances.put(10, dRLooser.getChanceLevel10());
        itemFromDropChances.put(20, dRLooser.getChanceLevel20());
        itemFromDropChances.put(30, dRLooser.getChanceLevel30());
        itemFromDropChances.put(40, dRLooser.getChanceLevel40());
        itemFromDropChances.put(50, dRLooser.getChanceLevel50());
        itemFromDropChances.put(60, dRLooser.getChanceLevel60());
        itemFromDropChances.put(70, dRLooser.getChanceLevel70());
        itemFromDropChances.put(80, dRLooser.getChanceLevel80());
        itemFromDropChances.put(0, dRLooser.getChanceCrystal());

        List<Item> l1 = new ArrayList<>();
        l1.add(sword1);
        Mockito.when(itemsRepository.getItemsAccordingToLevel(anyInt())).thenReturn(l1);

        //act & assert
        ItemPrivate droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        boolean result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);
    }

    @Test
    public void dropItemFromLooserTest6() {
        Character winner = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        Character looser = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        looser.setDropChances(ModelFactory.createDropChancesFromId(6));
        Item sword1 = Factory.createItem(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 1);

        Map<Integer, Integer> itemFromDropChances = new HashMap<>();
        DropChances dRLooser = looser.getDropChances();
        itemFromDropChances.put(1, dRLooser.getChanceLevel1());
        itemFromDropChances.put(10, dRLooser.getChanceLevel10());
        itemFromDropChances.put(20, dRLooser.getChanceLevel20());
        itemFromDropChances.put(30, dRLooser.getChanceLevel30());
        itemFromDropChances.put(40, dRLooser.getChanceLevel40());
        itemFromDropChances.put(50, dRLooser.getChanceLevel50());
        itemFromDropChances.put(60, dRLooser.getChanceLevel60());
        itemFromDropChances.put(70, dRLooser.getChanceLevel70());
        itemFromDropChances.put(80, dRLooser.getChanceLevel80());
        itemFromDropChances.put(0, dRLooser.getChanceCrystal());

        List<Item> l1 = new ArrayList<>();
        l1.add(sword1);
        Mockito.when(itemsRepository.getItemsAccordingToLevel(anyInt())).thenReturn(l1);

        //act & assert
        ItemPrivate droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        boolean result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0;
        assertTrue(result);
    }

    @Test
    public void dropItemFromLooserTest1WithCrystalGold() {
        Character winner = Factory.createCharWithBag(1, "Pol", CharacterClass.Knight, 7, 5, ZoneClass.Duel);
        ItemPrivate crystalRingWithGold = Factory.createCrystalRingWithCrystal(2, "Gold", Constants.CRYSTAL_GOLD);
        winner.getEquipment().add(crystalRingWithGold);
        Character looser = Factory.createCharWithBag(2, "Jim", CharacterClass.Knight, 7, 5, ZoneClass.Arena);
        Item sword1 = Factory.createItem(0, 1, ItemClass.WEAPON, "Sword1XXX", 12, 24, 0.1, 1.5, 1);

        Map<Integer, Integer> itemFromDropChances = new HashMap<>();
        DropChances dRLooser = looser.getDropChances();
        itemFromDropChances.put(1, dRLooser.getChanceLevel1());
        itemFromDropChances.put(10, dRLooser.getChanceLevel10());
        itemFromDropChances.put(20, dRLooser.getChanceLevel20());
        itemFromDropChances.put(30, dRLooser.getChanceLevel30());
        itemFromDropChances.put(40, dRLooser.getChanceLevel40());
        itemFromDropChances.put(50, dRLooser.getChanceLevel50());
        itemFromDropChances.put(60, dRLooser.getChanceLevel60());
        itemFromDropChances.put(70, dRLooser.getChanceLevel70());
        itemFromDropChances.put(80, dRLooser.getChanceLevel80());
        itemFromDropChances.put(0, dRLooser.getChanceCrystal());

        List<Item> l1 = new ArrayList<>();
        l1.add(sword1);
        Mockito.when(itemsRepository.getItemsAccordingToLevel(anyInt())).thenReturn(l1);

        //act & assert
        ItemPrivate droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        boolean result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);

        droppedItem = mockItemsService.dropItemFromLooser(winner, dRLooser);
        result = itemFromDropChances.get(droppedItem.getDropItemLevel()) > 0 || (droppedItem.getDropItemLevel() == 30 || droppedItem.getDropItemLevel() == 40);
        assertTrue(result);
    }
}
