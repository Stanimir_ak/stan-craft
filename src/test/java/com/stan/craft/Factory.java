package com.stan.craft;

import com.stan.craft.Constants.Constants;
import com.stan.craft.Models.Character;
import com.stan.craft.Models.*;
import com.stan.craft.Models.Factories.ModelFactory;

public class Factory {
    public static ItemPrivate createItemPrivate(int isUnique, int id, ItemClass itemClass, String name, int dmgMinBase, int dmgMaxBase, double speed, double rise, int lvl) {
        ItemPrivate item = new ItemPrivate();
        item.setIsUnique(isUnique);
        item.setId(id);
        item.setItemClass(itemClass);
        item.setName(name);
        item.setDamageMin((int) (dmgMinBase * rise));
        item.setDamageMax((int) (dmgMaxBase * rise));
        item.setSpeed(speed);
        item.setRise(rise);
        item.setLevel(lvl);
        return item;
    }

    public static Item createItem(int isUnique, int id, ItemClass itemClass, String name, int dmgMinBase, int dmgMaxBase, double speed, double rise, int lvl) {
        Item item = new Item();
        item.setIsUnique(isUnique);
        item.setId(id);
        item.setItemClass(itemClass);
        item.setName(name);
        item.setDamageMin((int) (dmgMinBase * rise));
        item.setDamageMax((int) (dmgMaxBase * rise));
        item.setSpeed(speed);
        item.setRise(rise);
        item.setLevel(lvl);
        return item;
    }

    public static ItemPrivate createCrystalRingWithCrystal(int id, String crystal, String crystalDescFromConstants) {
        ItemPrivate crystalRing = createEmptyRing(id, ItemClass.RING, "Crystal Ring", Constants.CRYSTAL_RING);
        crystalRing.setCrystal(crystal);
        //crystal ring is always level 1
        crystalRing.setLevel(1);
        crystalRing.setDescription(crystalRing.getDescription() + crystalDescFromConstants);
        return crystalRing;
    }

    public static ItemPrivate createEmptyRing(int id, ItemClass itemClass, String name, String description) {
        ItemPrivate ring = new ItemPrivate();
        ring.setId(id);
        ring.setItemClass(itemClass);
        ring.setDescription(description);
        ring.setName(name);
        //empty ring is always level 10
        ring.setLevel(10);
        return ring;
    }

    public static ItemPrivate createCrystal(int id, ItemClass itemClass, String name, String description) {
        ItemPrivate ring = new ItemPrivate();
        ring.setId(id);
        ring.setItemClass(itemClass);
        ring.setDescription(description);
        ring.setName(name);
        //crystal is always level 0
        ring.setLevel(0);
        return ring;
    }

    public static Character createCharWithBag(int id, String name, CharacterClass class1, int lvl, int bagCapacity, ZoneClass zone) {
        Character character = new Character();
        character.setType(IndividualType.Character);
        character.setName(name);
        character.setCharacterClass(class1);
        character.setId(id);
        character.setLevel(lvl);
        ItemPrivate bag = new ItemPrivate();
        bag.setItemClass(ItemClass.BAG);
        bag.setLevel(1);
        bag.setName("justBag");
        bag.setCapacity(bagCapacity);
        character.getEquipment().add(bag);
        character.setArmorBase(1);
        character.setLifeBase(30);
        character.setDamageMinBase(2);
        character.setDamageMaxBase(4);
        character.setMagicDamageBase(0);
        character.setBlockChance(0.10);
        character.setSpeed(1.0);
        character.setZone(zone);
        character.setDropChances(ModelFactory.createDropChancesFromId(1));
        return character;
    }

    public static Character createCharWithOUTBag(String name, CharacterClass class1, int lvl) {
        Character character = new Character();
        character.setLevel(lvl);
        return character;
    }

    public static User createUserStan() {
        User stan = new User();
        stan.setUsername("stan");
        return stan;
    }

    public static Creep createCreep(IndividualType type, CreepClass creepClass, String name, int lvl, int dropChancesId,
                                    double block, int isBoss, int dmgMin, int dmgMax, int dmgMagic, int armor, int life,
                                    double speed, int exp, int gold) {
        Creep creep = new Creep();
        creep.setType(type);
        creep.setCreepClass(creepClass);
        creep.setName(name);
        creep.setLevel(lvl);
        creep.setDropChances(ModelFactory.createDropChancesFromId(dropChancesId));
//        creep.setPicture(creepDTO.getPicture());
        creep.setFinalBlockChance(block);
        creep.setIsBoss(isBoss);

        creep.setFinalDamageMin(dmgMin);
        creep.setFinalDamageMax(dmgMax);
        creep.setFinalMagicDamage(dmgMagic);
        creep.setFinalArmor(armor);
        creep.setFinalLife(life);
        creep.setFinalSpeed(speed);
        creep.setExperience(exp);
        creep.setGold(gold);
        return creep;
    }
}
